jQuery(document).ready(function($) {

    jQuery(".tb-mega-sub-menu .panel-default .panel-heading").each(function(index) {
        jQuery(this).click(function() {
            jQuery(".tb-mega-sub-menu .panel-body2").hide();
        });
    });


    jQuery(".tb-mega-sub-menu .panel-default .collapse").each(function(index) {
        if (jQuery(this).hasClass("in") == true) {
            jQuery(this).removeClass("in");
        }
    });


	 jQuery(".tb-mega-sub-menu .hidden-xs .col").each(function(index) {
       
            jQuery(this).attr("data-tabs", "tabs-" + index);
            jQuery(this).hover(function() {
				
                var tab_id = jQuery(this).attr("data-tabs");
                if (jQuery("#" + tab_id + ":visible").length == 1) {
                    jQuery(".tb-mega-sub-menu .panel-body2").hide();
                } else {
                    jQuery(".tb-mega-sub-menu .panel-body2").hide();
                    jQuery("#" + tab_id).show();
                    jQuery("#" + tab_id).addClass("current");
                }

                return false;
            },function() {});
        
    });
	
	jQuery(".tb-mega-sub-menu .hidden-lg .product_family_title  a").each(function(index) {
       
            jQuery(this).attr("data-tabs", "tabs-" + index);
            jQuery(this).hover(function() {
				
                var tab_id = jQuery(this).attr("data-tabs");
                if (jQuery("#" + tab_id + ":visible").length == 1) {
                    jQuery(".tb-mega-sub-menu .panel-body2").hide();
                } else {
                    jQuery(".tb-mega-sub-menu .panel-body2").hide();
                    jQuery("#" + tab_id).show();
                    jQuery("#" + tab_id).addClass("current");
                }

                return false;
            },function() {});
        
    });
	
    jQuery(".tb-mega-sub-menu .panel-body2").each(function(index) {
        jQuery(this).attr("id", "tabs-" + index);
    });


});
