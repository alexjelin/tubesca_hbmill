<?php

/**
 * @file
 * The primary PHP file for this theme.
 */
$theme_path = drupal_get_path('theme', 'tubesca_comabi');
include $theme_path . "/includes/preprocess_block.inc";
include $theme_path . "/includes/preprocess_pane.inc";
include $theme_path . "/includes/preprocess_page.inc";
include $theme_path . "/includes/preprocess_node.inc";
include $theme_path . "/includes/preprocess_entity.inc";
include $theme_path . "/includes/preprocess_field.inc";
include $theme_path . "/includes/preprocess_print.inc";
include $theme_path . "/includes/alter.inc";

function tubesca_comabi_status_messages($variables) {
	global $tc_translations;
  $display = $variables['display'];
  $output = '';
 
  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );
  foreach (drupal_get_messages($display) as $type => $messages) {
    // ! important : adding html needed for the modal.
    $output = '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-body">';
 
    $output .= "<div class=\"_messages $type\">\n";
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }
    $output .= "</div>\n";
 
    $output .= '</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">'.$tc_translations['string-area-397'].'</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->';
  }
  return $output;
}

function tubesca_comabi_webform_display_hidden($variables) {
    $element = $variables['element'];

    return $element['#format'] == 'html' ? $element['#markup'] : $element['#markup'];
}

/*
function tubesca_comabi_file_managed_file($variables) {
  $element = $variables['element'];
  $output = '';
  $output .= '<div class="image-widget form-managed-file clearfix">';

  if (isset($element['preview'])) {
    $output .= '<div class="image-preview">';
    $output .= drupal_render($element['preview']);
    $output .= '</div>';
  }

  $output .= '<div class="image-widget-data"><div class="bold file-button">Choose File</div>
  <div class="file-name file-button margin_10_top_bot">No File Selected</div>';
  if ($element['fid']['#value'] != 0) {
    $element['filename']['#markup'] .= ' <span class="file-size">(' . format_size($element['#file']->filesize) . ')</span> ';
  }
  $output .= drupal_render_children($element);
  $output .= '</div>';
  $output .= '</div>';
 
  return $output;
}

function tubesca_comabi_file_widget($variables) {
  $element = $variables['element'];
  $output = '';
  $output .= '<div class="image-widget form-managed-file clearfix">';

  if (isset($element['preview'])) {
    $output .= '<div class="image-preview">';
    $output .= drupal_render($element['preview']);
    $output .= '</div>';
  }

  $output .= '<div class="image-widget-data"><div class="bold file-button">Choose File</div>
  <div class="file-name file-button margin_10_top_bot">No File Selected</div>';
  if ($element['fid']['#value'] != 0) {
    $element['filename']['#markup'] .= ' <span class="file-size">(' . format_size($element['#file']->filesize) . ')</span> ';
  }
  $output .= drupal_render_children($element);
  $output .= '</div>';
  $output .= '</div>';
 
  return $output;
}*/