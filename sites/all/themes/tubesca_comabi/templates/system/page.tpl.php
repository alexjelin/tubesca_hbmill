<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K43MKTL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php 

if(isset($hreflang_results)){
	
	$counter=0;
	foreach ($hreflang_results as $key=>$hreflang) {
			 
		$element = array(
		  '#tag' => 'link', // The #tag is the html tag - 
		  '#attributes' => array( // Set up an array of attributes inside the tag
			'href' => $hreflang[0], 
			'rel' => 'alternate',
			'hreflang' => $hreflang[1],
		),
	  ); 
	  
	  drupal_add_html_head($element, 'hreflang'.$counter);
	  $counter++;
	  
	}

}

?> 

<?php print render($page['content']); ?>