<html>
<head>
<style>
// Wishlist PDF >> CSS classes

.table-wishlist .views-field-title {
    float: none;
}

.table-wishlist  img {
    width:110px;
    height:150px;
    text-align:left;
    vertical-align:top;
		border: 1px solid #eaebed;
}

.table-wishlist .inner_cells{
  text-align:left !important;
}

.table-wishlist .inner_numenclatures{
  width:85%;
  margin-top:5px;
}

#num_table{
    border-spacing: 6px;
}

#num_table tr td{
  border-bottom: 0.15px solid #cccccc !important;
  width:50% !important;
}

.table-wishlist .table > tbody > tr td  {
    border-bottom: 0.7px solid #cccccc;
    padding:20px 5px 20px 5px;
    text-align:left;
    vertical-align:top;
    font-size:10px;
    font-family:'Roboto', sans-serif;
}


#title_wishlist {
    text-align: left;
    margin-top:20px;
    font-size:16px;
    font-family:'Roboto', sans-serif;
}

// Wishlist PDF <<<<<<<<< CSS classes -----------------------------------


//  Compare PDF >>> CSS classes

.picture_compare_col ul li {
    height: 130px !important;
}
.picture_compare_col ul li img {
    margin: 0 auto;
    float: none;
}
.compare_pdf ul > li {
    margin-right: 0px;
}
.compare_pdf table {
    width: 700px;   

}

.compare_pdf .picture_compare_col img{
  width:60px;
  height:100px;
  margin: 20px 0px 20px 0px;
}

.compare_pdf table tr {
    background: white;
}
.compare_pdf table tr td {
    border: 1px solid #cccccc !important;
    width: 25%;
    padding: 5px 5px 5px 10px;
    font-family:'Roboto', sans-serif;
}

//  Compare PDF >>> CSS classes ---------------------------------------------------------------


//  PDF CSS - all content >>> CSS classes
.print-content {
    margin: 40px 0 0 0;
}
.print-content table {
    border-spacing: 0px;
}
#header_pdf td {
    width: 50%;
    line-spacing: 7px;
}
#header_pdf td:nth-child(2) {
    padding-left: 175px;
}
#header_pdf td:nth-child(2) div:nth-child(1) {
    color: black;
    font-size: 18px;
    width: 350px !important;
}
#header_pdf td:nth-child(2) div:nth-child(2) {
    color: grey;
    font-size: 16px;
    padding-left: 85px;
    padding-bottom: 10px;
}
#header_pdf td:nth-child(2) img {
    padding-left: 295px;
}
#logo {
    width: 235px;
}
#page {
    page-break-after: always;
}
.footer_text {
    font-size: 12px;
    line-height: 12px;
    font-family:'Roboto', sans-serif !important;
}
#footer {
    position: fixed;
    bottom: 65;
}

//  PDF CSS - all content >>> CSS classes -------------------------------------


.view-id-wishlist .flag-wishlist {
	color: #000;
	font-size: 22px;
	position: absolute;
	right: 0px;
	top: 4px;
}
.view-id-wishlist .flag-wishlist a {
	color: #cccccc;
}
.wishlist_buttons .remove_all {
	border: 0px;
	background-color: #2d2e33;
	float: left;
	padding-right: 25px;
	padding-top:28px;
	padding-bottom:28px;
	letter-spacing:2.4px;
}
.wishlist_buttons .remove_all:hover {
	background-color: #0a090e;
}
.wishlist_buttons .remove_all i {
	font-size: 22px;
	margin-left: 10px;
}
.wishlist_buttons .remove_all:hover, .wishlist_buttons .remove_all:active { 
	color: #fff;
}
.wishlist_buttons .buttons-wrapper {
	float:right;
}
.wishlist_buttons .remove_all {
	border-radius: 2px;
}
.wishlist_buttons .wishbtn {
	margin-right: 25px;
	padding-top:28px;
	padding-bottom:28px;
	letter-spacing:2.4px;
}
.wishlist_buttons .wishbtn {
	border: 0px;
	background-color: #1b66ea;
	float: left;
	padding-right: 25px;
}
.wishlist_buttons .wishbtn:hover {
	background-color: #083fa3;
}

.wishlist_buttons .wishbtn i {
	font-size: 22px;
	margin-left: 10px;
}
.wishlist_buttons .wishbtn:hover, .wishlist_buttons .wishbtn:active {
	color: #fff;
}
.wishlist_buttons .wishbtn {
	border-radius: 2px;
}
.view-id-wishlist .views-field-title-1 {
	clear: right;
    margin-bottom: 10px;
    margin-top: 5px;
}

.header-bottom {
	position: relative;
}
#ajax_replace_auto {
	background: #fff none repeat scroll 0 0;
	left: 0;
	position: absolute;
	top: 0;
	width: 100%;
	z-index: 9999;
}
#ajax_replace_auto .row {
	background: #fff;
	padding: 10px;
	padding-bottom: 20px;
	border: 1px solid #d6d6d6;
}
.search_div_img img {
	display: block;
	float: left;
	margin-bottom: 15px;
	margin-right: 15px;
	max-width: 100px;
}
.sep_menu {
	background: #d6d6d6 none repeat scroll 0 0;
	display: block;
	height: 85%;
	left: 50%;
	position: absolute;
	transform: translateX(-50%);
	width: 1px;
}
/* HOME MENU */
.full_menu .tb-megamenu-submenu {
	position: absolute;
	left: 0px;
	width: 100%;
}
li.simple_menu {
	position: relative !important;
}
.tb-megamenu-main-menu.tb-megamenu .nav > li {
	position: static;
  background-color: #fff;
}
.tb-megamenu-main-menu .tb-mega-sub-menu .panel-heading a, .tb-megamenu-main-menu .tb-mega-sub-menu .panel-heading a:hover {
	margin-left: 0px;
	white-space: normal;
	position: relative;
	text-align: left;
}
.tb-megamenu-main-menu .tb-mega-sub-menu .panel-heading a span span {
	position: absolute;
	left: 0px;
	padding-left: 30px;
	top: 50%;
	-ms-transform: translateY(-50%);
	-webkit-transform: translateY(-50%);
	transform: translateY(-50%);
	font-size: 15px;
	text-transform: none !important;
}
.tb-megamenu-main-menu .tb-mega-sub-menu a, .tb-megamenu-main-menu .tb-mega-sub-menu a:hover {
	white-space: normal;
}
.tb-megamenu-main-menu .tb-mega-sub-menu > .panel > .panel-collapse > .panel-body {
	padding-left: 15px;
	margin-left: 0px;
}
.tb-megamenu-main-menu .product-tabs-vertical > .panel > .panel-collapse {
	height: auto;
	padding-left: 255px;
}
.tb-megamenu-main-menu .view-product-family-tab .row {
	margin-left: -15px;
	margin-bottom: 20px;
}
.tb-megamenu-main-menu .product-tabs-vertical > .panel > .panel-collapse {
	background-color: #fff;
	border-radius: 5px;
	/*-webkit-box-shadow: 1px 2px 5px 0px rgba(153,153,153,1);
  -moz-box-shadow: 1px 2px 5px 0px rgba(153,153,153,1);
  box-shadow: 1px 2px 5px 0px rgba(153,153,153,1);*/
}
.tb-megamenu-main-menu .product-tabs-vertical{
	background-color: #fff;
}
.tb-megamenu-main-menu #tb-megamenu-column-1 .product-tabs-vertical > .panel > .panel-collapse{
	background: url('../images/mega_sub1.png') top left repeat-y #fff;
}
.tb-megamenu-main-menu #tb-megamenu-column-2 .product-tabs-vertical > .panel > .panel-collapse{
	background: url('../images/mega_sub2.png') top left repeat-y #fff;
}
.tb-megamenu-main-menu #tb-megamenu-column-3 .product-tabs-vertical > .panel > .panel-collapse{
	background: url('../images/mega_sub3.png') top left repeat-y #fff;
}
.mobile_menu {
	display: none;
}
.pc_menu {
	display: block;
}
.bean-video {
	position: relative;
}
.pane-footer-menus {
	padding-left: 25px;
}

.open-always{
	display: block !important;
}

.tb-megamenu-main-menu.tb-megamenu .mega-dropdown-menu{
	padding: 0px;
	border: 0px;
	box-shadow: none;
}

.tb-megamenu-main-menu.tb-megamenu .span12.mega-col-nav .mega-inner {
	padding: 0px;
	border: 0px;
}
.tb-megamenu .view-sub-menu-taxonomy .row{
	margin: 0px -15px;
}

.tb-megamenu-main-menu.tb-megamenu .dropdown-menu
@media (max-width: 1200px) {
.bean-services .services-description {
	max-width: 80%;
	margin: auto;
}
.bean-services .service-blocks a.service-block {
	float: unset;
	display: inline-block;
}
.service-blocks, .services-description {
	text-align: center;
}
.bean-slider2 nav.slick__arrow .slick__arrow__inner, .bean-top-slider nav.slick__arrow .slick__arrow__inner {
	width: 100%;
}
}
 @media only screen and (min-width:640px) and (max-width: 992px) {
.header-left .header-left-second .pane-page-logo {
	float: left;
	overflow: hidden;
	text-align: center;
	width: 100%;
	padding: 0px;
}
.header-left .header-left-second .pane-search-form, .header-left .header-left-second .bean-search-block {
	float: right;
	margin-bottom: 17px;
	padding: 2px 30px 0 0;
	position: relative;
	width: 320px;
}
}
@media (max-width: 640px) {
		
.bean-social-networks .field-type-field-collection .field-item {
	display: inline-block;
	float: unset;
}
.bean-social-networks .field-type-field-collection {
	max-width: 100%;
}
.bean-social-networks .field-name-field-title {
	font-size: 27px;
	width: 100%;
}
.bean-services .service-blocks .service-block-icon img {
	position: absolute;
	left: 0;
	top:15px;
}
.bean-services .services-button {
	left: 0px;
	right: 0px;
	text-align: center;
}
.bean-help-choose .field-type-link-field a, .bean-find-solution .field-type-link-field a, .bean-services .services-button a, .bean-video .field-name-field-link a, .pane-news .views-field-nid a, .bean-rss .field-name-field-link a, .bean-top-slider .slide__link a, .bean-slider2 .slide__link a {
	font-size: 14px;
}
.bean-services .service-blocks a.service-block .service-block-description p {
	padding: 0 15px 0 46px;
	font-size: 14px;
}
.bean-services .service-blocks a.service-block {
	position: relative;
}
.bean-services .service-blocks a.service-block .service-picto {
	position: absolute;
	right: 36px;
	top: 40px;
}
}
@media (max-width: 768px) {
.header-left {
	height: 200px;
}
}
@media (max-width: 992px) {
.mobile_menu {
	display: block;
}
.pc_menu {
	display: none;
}
.bean-slider2 nav.slick__arrow .slick__arrow__inner, .bean-top-slider nav.slick__arrow .slick__arrow__inner {
	width: 100%;
}
.tb-megamenu-main-menu .tb-megamenu-item .tb-megamenu-submenu {
	height: auto !important;
}
.tb-megamenu-main-menu.tb-megamenu .nav-collapse .dropdown-menu {
	margin: 0;
}
.tb-megamenu-main-menu.tb-megamenu .span12.mega-col-nav .mega-inner {
	padding: 0px;
}
.tb-megamenu-main-menu .panel-group {
	margin-bottom: 0px;
}
.tb-megamenu-main-menu .product-tabs-vertical > .panel > .panel-collapse {
	padding-left: 0px;
}
.tb-megamenu-main-menu .tb-mega-sub-menu > .panel > .panel-collapse > .panel-body {
	width: 100%;
}
.tb-megamenu-main-menu .tb-mega-sub-menu .panel-body2 {
	width: 100%;
}
.tb-megamenu-main-menu .view-product-family-tab .row {
	margin-bottom: 0px;
}
.tb-megamenu-main-menu .tb-mega-sub-menu .panel-heading a span span {
	position: relative;
}
}
.view-id-compare .compare_remove_button {
	position: absolute;
	right: 0;
	top: -128px;
}
.view-id-compare .compare_remove_button a {
	color: #d6d6d6;
	font-size: 20px;
	position:absolute;
	top:0px;
}
/* compare table */
.view-id-compare .compare_data {
	margin-top: 15px;
	margin-bottom: 30px;
}
.view-id-compare .compare_data ul li {
	width: 295px;
	color: #4b4f58;
    display: table-cell;
    font-family: "Rajdhani",sans-serif;
    font-size: 16px;
    font-weight: 500;
    height: 80px;
    padding: 10px 39px;
    text-align: left;
    vertical-align: middle;
	float: none;
	border-right: 10px solid #f2f2f2;
	border-bottom: 1px solid #d6d6d6;
	border-left: 1px solid #d6d6d6;
	border-top: 1px solid #d6d6d6;
	border-collapse: separate;
}
.view-id-compare .compare_data ul.row-last li {
	border-bottom: 1px solid #d7d7d7;
}
.view-id-compare .compare_data ul.row-odd > li {
	background: #fff;
}
.view-id-compare .compare_data ul.row-even > li {
	background: #e8e8e8;
    
}
.view-id-compare .compare_data ul > li {
	text-align: center;
	color: #828ca2;
}
.view-id-compare .compare_data ul > li:first-child {
	color: #4b4f58;
    display: table-cell;
    font-family: "Rajdhani",sans-serif;
    font-size: 20px;
    font-weight: bold;
    height: 80px;
    padding: 10px 39px;
    text-align: left;
    vertical-align: middle;
	float: none;
    border-right: 10px solid #f2f2f2;
	border-bottom: 1px solid #d6d6d6;
	border-left: 1px solid #d6d6d6;
	border-top: 1px solid #d6d6d6;
	border-collapse: separate;
}
.disable_ckeckbox {
	display: none;
}
.check_with_label input {
	display: none;
}
.check_with_label label.checked {
	color: red;
}
#filters > div {
	margin-bottom: 30px;
	padding-bottom: 30px;
	position: relative;
}
#filters > div:after {
	background: #d6d6d6 none repeat scroll 0 0;
	bottom: 0;
	content: "";
	display: block;
	height: 4px;
	position: absolute;
	width: 30%;
}
#filters .ui-slider {
	height: 6px;
	border-color: #4b4f58;
	border-radius: 0;
	border-left: 0;
	border-right: 0;
}
#filters .ui-slider:before {
	border-left: 1px solid #4b4f58;
	content: "";
	display: block;
	height: 14px;
	left: 0px;
	position: absolute;
	top: -5px;
	width: 1px;
}
#filters .ui-slider:after {
	border-left: 1px solid #4b4f58;
	content: "";
	display: block;
	height: 14px;
	right: 0px;
	position: absolute;
	top: -5px;
	width: 1px;
}
#filters .ui-slider-horizontal .ui-slider-handle {
	margin-left: 0px;
	top: -14px;
}
#filters .ui-slider .ui-slider-handle {
	background: #4b4f58 none repeat scroll 0 0;
	border: 0 none;
	border-radius: 0;
	cursor: default;
	height: 34px;
	position: absolute;
	width: 12px;
	z-index: 2;
}
#filters .ui-slider .valuebox {
	color: #4b4f58;
	left: -19px;
	position: absolute;
	text-align: center;
	top: 31px;
	width: 50px;
}
#filters .simple_slider .ui-slider {
	margin-bottom: 40px;
}
#filters .filter_title {
	margin-bottom: 30px;
}
#reset {
	font-family: 'Rajdhani', sans-serif;
	color: #1b66ea;
	display: block;
	font-weight: bold;
	letter-spacing: 2.4px;
	text-transform: uppercase;
	width: 100%;
	font-size: 16px;
}
#reset span {
	float: right;
	font-size: 20px;
	margin-top: -5px;
}
#filters .selectBox-dropdown {
	background: none;
	border-radius: 3px;
	width: 100%;
}
#filters .selectBox-dropdown .selectBox-label {
	display: block;
	font-family: "Roboto", sans-serif;
	font-size: 16px;
	font-weight: normal;
	padding-bottom: 15px;
	padding-left: 20px;
	padding-top: 16px;
	letter-spacing: 0px;
	color: #272e39;
	text-transform:lowercase;
}
#filters .selectBox-dropdown .selectBox-label:first-letter {
	text-transform:capitalize;
}
#filters .selectBox-dropdown .selectBox-arrow {
	background: rgba(0, 0, 0, 0) url("../images/arrows.png") no-repeat scroll 50% center;
	margin-right: 20px;
	border-left: 0px;
}
#filters h3 {
	text-transform: uppercase;
}
#views-exposed-form-facet-search-block .selectBox-dropdown .selectBox-arrow {
	background: rgba(0, 0, 0, 0) url("../images/arrows.png") no-repeat scroll 50% center;
}
/* maliste */
.maliste_comapre_amount {
	text-align: center;
}
.maliste_comapre_title:after, .maliste_comapre_amount:after {
	border-bottom: 1px solid #d6d6d6;
	bottom: 0;
	content: "";
	display: block;
	height: 1px;
	position: absolute;
	width: 100%;
}
.maliste_comapre_title.odd:after {
	left: 0;
}
.maliste_comapre_amount.odd:after {
	left: -15px;
}
.maliste_comapre_title.even:after {
	right: -15px;
}
.maliste_comapre_amount.even:after {
	right: 0;
}
.maliste_row {
	display: table;
	width: 100%;
	margin: 0px;
}
.maliste_row [class*="col-"] {
	float: none;
	display: table-cell;
	vertical-align: top;
	padding-top: 40px;
	padding-bottom: 40px;
}
.inner_wrapper {
	padding-left: 18px;
}
.maliste_row > .maliste_comapre_title:first-child .inner_wrapper {
	padding-left: 3px;
}
.view-id-wishlist .views-field-nothing-1 .maliste_row:last-child .maliste_comapre_title:after, .view-id-wishlist .views-field-nothing-1 .maliste_row:last-child .maliste_comapre_amount:after {
	border-bottom: 0px solid #d6d6d6;
}
.fulllist .headinfo {
	padding-bottom: 0px;
}
.views-field.views-field-nothing-1::before {
	background: #d6d6d6 none repeat scroll 0 0;
	content: "";
	display: block;
	left: 0;
	position: absolute;
	top: 0;
	width: 100%;
	height: 1px;
}
.close_whislist_info {
	color: #1d1d1b;
	font-size: 20px;
	position: absolute;
	right: 60px;
	top: 25px;
	cursor: pointer;
}
.fulllist .headinfo .collapsed .fa-chevron-down {
	display: none;
}
.fulllist .headinfo .collapsed .fa-chevron-right {
	display: inline-block;
}
.fulllist .headinfo .fa-chevron-down {
	display: inline-block;
}
.fulllist .headinfo .fa-chevron-right {
	display: none;
}
.fulllist .views-field-nothing-2 a {
	display: block;
}
.fulllist .views-field-nothing-2 a:first-child {
	color: #fff;
	background: #1b66ea none repeat scroll 0 0;
}
.fulllist .views-field-nothing-2 a {
	color: #1b66ea;
	text-transform: uppercase;
	border: 3px solid #1b66ea;
	border-radius: 28px;
	cursor: pointer;
	display: inline-block;
	font-size: 16px;
	margin-right: 20px;
	padding: 15px 30px;
	margin-top: 45px;
	margin-bottom: 60px;
	position: relative;
	font-family: 'Rajdhani', sans-serif;
	font-weight:bold;

}
.fulllist .views-field-nothing-2 a:hover {
	background-color:#083fa3;
	border:3px solid #083fa3;
	color:#fff;
	
}
.fulllist .views-field-nothing-2 a:last-child {
	padding-right: 60px;
}
.fulllist .views-field-nothing-2 i {
	margin-left: 25px;
	position: absolute;
	top: 18px;
	right: 25px;
}
.fulllist .views-row:first-child {
	padding-top: 0px;
}
.fulllist .wishlist_buttons {
	padding-top: 50px;
	padding-bottom: 50px;
}
#q1, #q1 .number2 {
	background-color: #193a71;
}
#q2, #q2 .number2 {
	background-color: #1a3054;
}
#q3, #q3 .number2 {
	background-color: #182740;
}
#q4, #q4 .number2 {
	background-color: #0d192e;
}
#q5, #q5 .number2 {
	background-color: #061020;
}
#q6, #q6 .number2 {
	background-color: #193a71;
}
#q7, #q7 .number2 {
	background-color: #1a3054;
}
div.question {
	position: relative;
	padding-left: 80px;
	padding-right: 80px;
	padding-top: 50px;
	padding-bottom: 50px;
	text-align: center;
}
div.question h2 {
	color: #fff;
}
div.question img {
	cursor: pointer;
	margin-bottom: 10px;
}
div.question span {
	font-family: 'Rajdhani', sans-serif;
	color: #fff;
	font-size: 16px;
	letter-spacing: 2.4px;
}
div.question .answers input {
	display: none;
}
div.question .answers {
	display: block;
	float: left;
	margin-right: 25px;
}
div.question > .number2 {
	height: 100px;
	left: 0;
	position: absolute;
	top: 50%;
	transform: translateY(-50%);
	width: 50px;
}
div.question > .number {
	font-family: 'Rajdhani', sans-serif;
	font-size: 24px;
	font-weight: bold;
	background-color: #1b66ea;
	border-radius: 100%;
	color: #fff;
	display: block;
	height: 100px;
	left: -50px;
	padding-left: 25px;
	padding-top: 35px;
	position: absolute;
	text-align: left;
	top: 50%;
	transform: translateY(-50%);
	width: 100px;
}
.checked img {
	opacity: 0.5;
}
.checked .checked_chouse img {
	opacity: 1;
}
div.question label {
	position: relative;
	max-width: 210px;
	margin-bottom: 15px;
}
div.question .checked_chouse .icon {
	display: block;
}
div.question .icon {
	background: #000 none repeat scroll 0 0;
	border-radius: 50%;
	color: #fff;
	padding: 2px 0px;
	position: absolute;
	right: -10px;
	top: -10px;
	display: none;
	width: 24px;
	height: 24px;
	text-align: center;
}
/* breadcrums */
.breadcrumb {
	background: none;
	padding: 0;
}
.breadcrumb a {
	color: #4b4f58;
	letter-spacing:0px;
	font-size:14px;
}
.breadcrumb > li + li::before {
	content: "- ";
	color: #4b4f58;
}
.breadcrumb > .active {
	color: #4b4f58;
	font-weight:normal;
	text-transform:capitalize;
}
.breadcrumb > li:first-child + li::before {
	content: "| ";
}
.view-facet-search .views-exposed-form .views-exposed-widget {
	float: unset;
}
.view-facet-search .form-item-sort-by {
	float: right;
}
.view-facet-search .form-item-sort-by .selectBox {
	font-family: Roboto, "sans-serif";
	letter-spacing: 0px;
	float: unset;
	width: 180px;
	height: 60px;
	box-shadow: none;
	border-radius: 3px;
	padding-top: 15px;
	padding-left:30px;
}
#views-exposed-form-facet-search-block .selectBox-dropdown .selectBox-label {
	font-weight:bold;
}
.change_taxonomy {
	margin-top: 82px;
}
.tubesca_core_flag_fields_button span {
	display: block;
}
.tubesca_core_flag_fields_button, .tubesca_core_flag_fields_button:hover {
	text-align: left;
	width: 220px;
	color: #fff;
	background: #1b66ea;
	border: 0;
	position: relative;
	float: right;
}
.tubesca_core_flag_fields_button i {
	font-size: 30px;
	position: absolute;
	right: 38px;
	top: 17px;
}
/*bean-choose-model*/
.bean-choose-model .panel-heading {
	position: relative;
}
.bean-choose-model .panel {
	margin-bottom: 0;
}
.bean-choose-model .panel-heading > div i {
	bottom: -13px;
	cursor: pointer;
	display: block;
	font-size: 24px;
	left: 50%;
	position: absolute;
	transform: translateX(-50%);
	z-index: 100;
}
.bean-choose-model .panel-heading > div:first-child i.fa-plus-circle {
	display: none;
}
.bean-choose-model .panel-heading > div.collapsed:first-child i.fa-plus-circle {
	display: block;
}
.bean-choose-model .panel-heading > div.collapsed:first-child i.fa-minus-circle {
	display: none;
}
.bean-choose-model .panel-heading p {
	margin-top: 10px;
}
a.a2a_dd span {
	display: none;
}
.a2a_kit {
	position: absolute;
	right: 15px;
	top: 29px;
}
.show0 {
	display: none !important;
}
.header-left-first .bean-country-selector-widget {
	float: right;
	margin-right: 28px;
}
.select_region i {
	color: #1b66ea;
	font-size: 30px;
	padding: 5px 16px;
}
.select_region {
	color: #363f50;
	padding: 0;
	margin-top: 9px;
	z-index: 100;
}
.select_region span.region_name {
	position: relative;
	top: -6px;
	letter-spacing: 0px;
	font-family: Roboto, "sans-serif";
	font-size: 12px;
}
.select_region span.select_region_caret i {
	font-size: 16px;
}
.select_region span.select_region_caret {
	position: relative;
	top: -4px;
}
.martix_info {
	background-color: #1b66ea;
	margin-left: 50px;
	margin-right: 50px;
	padding: 40px 70px;
}
.martix_info p {
	font-family: 'Rajdhani', sans-serif;
	letter-spacing: 2.4px;
	color: #fff;
	font-size: 24px;
	margin-top: 20px;
}
#matrix .question h2 {
	font-family: 'Rajdhani', sans-serif;
	font-size: 20px;
	font-weight: normal;
	padding-bottom: 39px;
}
#matrix .question .step {
	font-family: 'Rajdhani', sans-serif;
	font-size: 30px;
	font-weight: bold;
	text-transform: uppercase;
	color: #fff;
}
.martix_info img {
	vertical-align: middle;
	margin-right: 20px;
}
.martix_info .choose-bulb {
	min-height: 200px;
	line-height: 200px;
	float: left;
}
.matrix_submit {
	margin-top: 50px;
	margin-bottom: 10px;
}
.matrix_button_submit, .matrix_button_submit:hover {
	color: #fff;
	background: #1b66ea;
	border-radius: 25px;
	letter-spacing: 2.4px;
	text-transform: uppercase;
}
.view-display-id-page .views-fieldset .views-fieldset:first-child:after {
	background: #cccccc none repeat scroll 0 0;
	content: "";
	display: block;
	height: 76%;
	position: absolute;
	right: 0;
	top: 0;
	width: 1px;
}
.view-display-id-page .views-fieldset .views-fieldset:nth-child(2) {
	margin-top: 50px;
}
.wishbtn img, .remove_all img {
	border: 0 none;
	display: block;
	float: right;
	margin-left: 12px;
	margin-right: 0;
}
.orange_footer {
	background: #ff9347 none repeat scroll 0 0;
	border-radius: 5px;
	color: #fff;
	margin-top: 30px;
	padding: 25px 70px;
	position: relative;
	text-align: center;
	font-size: 18px;
}
.orange_footer img {
	margin: 0px;
	border: 0px;
}
.orange_footer span {
	margin-top: 15px;
	display: block;
}
.orange_footer .plus_icon {
	bottom: -21px;
	left: 50%;
	position: absolute;
	transform: translateX(-50%);
}

/****** Bean Choose Model ******/

.bean-choose-model .table-header {
	border: 1px solid #d6d6d6;
	text-align: left;
	background-color: #4b4f58;
  color: #ffffff;
	font-size: 20px;
	padding: 0px 20px 0px 20px;
	font-weight: bold;
	font-family: 'Rajdhani', sans-serif;
}
.bean-choose-model .table-header p{
	
}
.bean-choose-model .table-header .row div{
	padding: 25px 0px;
}
.bean-choose-model .table-header .row div.border-row{
	border-left: 1px solid #d6d6d6;
	border-right: 1px solid #d6d6d6;	
}
.bean-choose-model .border-row-grey{
	border-left: 1px solid #d6d6d6;
	border-right: 1px solid #d6d6d6;
}
.bean-choose-model .panel-heading{
	padding-top: 0px;
	padding-bottom: 0px;
}
.bean-choose-model .panel-heading > .row{
	
}
.bean-choose-model .panel-heading > .row > div{
  padding: 10px 0px;
	min-height: 60px;
	background-color: #fff;
	font-family: 'Rajdhani', sans-serif;
  font-size: 20px;
	color: #4b4f58;
}
.bean-choose-model .panel-heading > .row > div p{
  font-size: 20px;
	font-family: 'Rajdhani', sans-serif;
	color: #4b4f58;
}
.bean-choose-model .panel-heading > .row > div:first-child{
	background-color: #f6f6f7;
}
@media screen and (min-width:940px) and (max-width:1024px) {
	div.question .answers {
	   min-height: 200px;
    }
	.wishlist_buttons .wishbtn,
	.wishlist_buttons .remove_all {
		margin-right:15px;
	}
}
@media only screen and (max-width : 768px) {
	.wishlist_buttons .wishbtn,
	.wishlist_buttons .remove_all {
		float:none;
		width:100%;
		margin-right:0px;
		margin-bottom:10px;
	}
	.list-version-v2.bean-contact-list .contact-info {
		width:100%;
	}
	#mini-panel-footer_menus .column {
		min-height:300px;
	}
}

@media only screen and (max-width : 640px) {
	div.question > .number { 
		display:none !important;
	}
	.martix_info {
		padding: 20px 20px;
		margin-left: 20px;
		margin-right: 20px; 
		text-align:center;
	}
	div.question {
		padding-left: 20px;
		padding-right: 20px;
	}
	#matrix.container {
		padding-left: 15px;
		padding-right: 15px;
	}
	div.question .answers {
		display: block;
		float: left;
		margin-right: 15px;
		width: 45%;
		min-height: 180px;
	}
	.martix_info .choose-bulb {
		float: none;
		margin-left: auto;
		margin-right: auto;
		width: 133px;
	}
	.fulllist .views-field-nothing-2 a { 
		margin-bottom: 20px;
		margin-right: 0px;
		margin-top:0px;
    }
	.view-display-id-page .views-fieldset .views-fieldset:nth-child(2) {
		margin-top:20px;
	}
	.wishlist_buttons .wishbtn,
	.wishlist_buttons .remove_all {
		float:none;
		width:100%;
		margin-right:0px;
		margin-bottom:10px;
	}
	.view-display-id-page .views-fieldset .views-fieldset:first-child::after {
		display:none;
	}
}

.mobile_filter_menu i{   font-size: 34px;
    position: relative;
    top: 5px;}
.mobile_filter_menu{ text-align: center;
	color: #1b66ea;
    display: block;
    font-family: "Rajdhani",sans-serif;
    font-size: 16px;
    font-weight: bold;
    letter-spacing: 2.4px;
    text-transform: uppercase;
    width: 100%;
	margin-top:20px;
	display:none; cursor:pointer;}
	.filter_info{display:none;}
#filters {display:block;}
.fa-chevron-circle-up {display:none;}
.product-tabs-vertical > .panel > .panel-heading i.fa{ display:none;}
@media only screen and (max-width : 992px){
.mobile_filter_menu,.filter_info {display:block;}
#filters {display:none;}
#filters > div::after {width: 100%;}
.change_taxonomy {margin-top:25px;}
.filter_info div {padding-top:10px; padding-bottom:10px;  font-size: 16px; color:#3e4247; }
div.filter_border{border-bottom:1px solid #e4e4e4;}

.product-tabs-wrapper {min-height: auto; }
.nano-scrollbar-product > .content {  overflow: hidden;
    padding-left: 0;
    position: relative;
    width: 100%;}


.product-tabs-vertical > .panel > .panel-heading i.fa{    color: #fff;
    float: right; font-size:18px;}

.product-tabs-vertical > .panel > .panel-heading i.fa{ display:block;}

#accordion_inner table tr, #accordion_inner table tbody {display:block;}
#accordion_inner table td {display:block; height:150px; float:left; width:50%;  border: 1px solid #d6d6d6 !important; padding-top: 5px;
padding-bottom: 5px; font-size:16px;}




	}
.top_menu_shadow{
    box-shadow: 3px 6px 10px -4px #696969;
   
    z-index: 100;
}

#toogle_menu_button {border:0px;}
#toogle_menu_button i{font-size:50px; color:#a29b9b;}
.thick_close{display:block; font-size:50px;}
.return_button p{   background: #ebebeb none repeat scroll 0 0;
   }
.return_button p a {  
 padding-bottom: 20px;
    padding-top: 20px;
	color: #484c54;
    display: block;
    font-size: 17px;
    margin-left: 30px;
    width: 100%;
	font-family:"Rajdhani",sans-serif;
	    font-weight: bold;
    letter-spacing: 0.5px;}
.return_button p a i{margin-right:20px;}
.return_text { 
background: #0e2b9a;
padding-bottom: 20px;
    padding-top: 20px;
	color: #fff;
    display: block;
    font-size: 15px;
    padding-left: 30px;
    width: 100%;
	font-family:"Rajdhani",sans-serif;
	    font-weight: bold;
    letter-spacing: 0.5px;
}
.footer_text_header {
	font-size: 14px !important;
	padding-bottom: 13px;
}
</style>

<?php global $tubesca_nav_vars, $tc_translations; ?>
</head>
<body>

 <table id="header_pdf">
      <tr>   
         <td><img id="logo" src="<?php print $tubesca_nav_vars['path_prefix']; ?>/sites/all/themes/tubesca_comabi/logo.png" alt="Logo"></img></td>
        
      </tr>
   </table>

<div class="print-content"><?php print $content; ?></div>

<div id="footer print_footer">
  <div class="footer_text_header"><?php print $tc_translations['string-area-416']; ?></div>
  <div class="footer_text"></div>
  <div class="footer_text"> <?php print $tc_translations['string-area-417']; ?></div>
  <div class="footer_text"> <?php print $tc_translations['string-area-418']; ?></div>
  <div class="footer_text"> <?php print $tc_translations['string-area-419']; ?></div>
</div>

  </body>
</html>
