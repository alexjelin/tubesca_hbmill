<?php
  global $tc_translations, $tubesca_nav_vars;
?>
<html>
<head>
<style>
/****** CSS style for pdf Version of Product display content type *********/

body {
	font-family: 'Roboto', sans-serif;
	margin-bottom: 80px;
	margin-top: 106px;
	color:#5e6266;
}
#logo {
	width: 400px !important;
	position: fixed;
	top: -15px;
}
#header_pdf {
	border: 0px solid;
	width: 100% !important;
	font-family: 'Roboto', sans-serif;
	position: fixed;
	top: -5px;
}
#header_pdf td {
	width: 50%;
}

#header_pdf td:nth-child(2) div:nth-child(1) {
	color: #4b4f58;
	font-size: 18px;
	font-weight:bold;
}
#header_pdf td:nth-child(2) div:nth-child(2) {
	color: #8a8e97;
	font-size: 16px;
}
#header_pdf td:nth-child(2){
	text-align:right;
	line-height:15px;
}

ul li {
	padding: 0px;
	margin: 0px;
	list-style: none;
}
ul {
	padding: 0px;
	margin: 0px;
}


.footer_text_header {
	font-size: 14px !important;
	padding-bottom: 13px;
}
.footer_text {
	font-size:12.5px;
	padding-bottom: 3px;
}

#footer:after {
	content: "Page:" counter(page);
	position: fixed;
	bottom: 86px;
	left: 985px;
	font-size: 12.5px;
}
#footer {
	position: fixed;
	bottom: 85px;
	width: 90%;
}
.sub_titles {
	font-size: 20px;
	font-weight:bold;
	border-bottom: 1px solid black;
	padding:5px;
	margin-top: 0px;
	margin-bottom: 0px;
}
#page1 table {
	border-collapse: collapse;
	margin-bottom: 15px;
	margin-top:15px;
	font-size: 14px !important;
}
#page1 table tr th {
	border: 0px solid;
}
#page1 table tr th {
	border: 0;
}

#page1 table:nth-child(2),#page1 table:nth-child(3){
	width:100%;
}

#page1 table:nth-child(2) tr td, #page1 table:nth-child(3) tr td, #page1 table:nth-child(2) tr th, #page1 table:nth-child(3) tr th{
	padding-bottom:10px;
	text-align:left;
}

#page1 table:nth-child(2) tr td:first-child, #page1 table:nth-child(3) tr td:first-child {
	width: 40%;
}

#page1 table:nth-child(1) tr td:first-child,
#page1 table:nth-child(1) tr th:first-child {
  border-left: 0 !important;
}


#page1 table:nth-child(1) tr td:last-child,
#page1 table:nth-child(1) tr th:last-child {
  border-right: 0 !important;
}

#page1 table:nth-child(1) tr th {
	border-top: 0px solid !important;
}

#page1 table:nth-child(1) tr td, #page1 table:nth-child(1) tr th {
	border: 0.7px solid #d6d6d6;
	padding: 2px !important;
	text-align: center;
}
#page {
	page-break-after: always;
}

#page1 table,#page table{
	width:100%;
}

#page  table tr:nth-child(1) td:nth-child(2) {
	font-weight:bold;
}
.info-desc{
	font-size: 14px;
}
.info-desc ul li{
	list-style: disc;
}
.info-desc ul li p{
	margin: 0px;	
}

#page  table tr td{
}
#page  table .thumbnails{
	height: 140px;
}
#page  table .thumbnails img{
	border: 2px solid #e9ebed;
}
.base_image{
	height: 192px;
	text-align: left;
}
.base_image img {
	height: 190px;
	width: 150px;
	border: 2px solid #e9ebed;
}
.thumbnails li {
	height: 136px;
	display: inline-block;
	width: 50%;
}
.logo-enterprise{
	margin-top: 5px;
}
.logo-enterprise img {
	max-width: 150px;
	height: auto;
}
.normes-logos img {
    display: inline;
    height: auto;
    width: 40px;
    margin-left: 5px;
		vertical-align: bottom;
}
</style>
</head>
<body onLoad="window.print()">
<table id="header_pdf">
  <tr>
    <td><img id="logo" src="<?php print $tubesca_nav_vars['path_prefix']; ?>/sites/all/themes/tubesca_comabi/logo.png" alt="Logo"></td>
    <td><div><?php print $title; ?></div>
      <div><?php print $sub_title; ?></div>
      <div><?php print $logo_enterprise; ?></div></td>
  </tr>
</table>
<div id="footer">
  <div class="footer_text_header"><?php print $tc_translations['string-area-416']; ?></div>
  <div class="footer_text"></div>
  <div class="footer_text"> <?php print $tc_translations['string-area-417']; ?></div>
  <div class="footer_text"> <?php print $tc_translations['string-area-418']; ?></div>
  <div class="footer_text"> <?php print $tc_translations['string-area-419']; ?></div>
</div>
<div id="page">
  <div class="sub_titles" ><?php print $tc_translations['string-area-392']; ?></div>
  <br/>
  <table>
    <tr>
      <td width="30%" align="left"><div class="base_image" ><?php print $gallery_base_image; ?></div>
      <?php print $gallery_thumbnails; ?>
      <div style="clear:both; height: 1px;"></div>
      <?php print $usage; ?>
      <?php print $normes; ?>
      <?php print $sold_in_france; ?>
      </td>
      <td class="info-desc" width="70%" valign="top"><?php print $top_axess.$body; ?> <?php print $arguments; ?>
        <?php // print $qr_code; ?></td>
    </tr>
  </table>
</div>
<div id="page1">
  <div class="sub_titles"><?php print $tc_translations['string-area-50']; ?></div>
  <?php print $table1; ?><br/>
	<div class="sub_titles"><?php print $tc_translations['string-area-10']; ?></div>
	<?php print $table2; ?><br/>
	<div class="sub_titles"><?php print $tc_translations['string-area-182']; ?></div>
	<?php print $table3; ?> </div>
</body>
</html>