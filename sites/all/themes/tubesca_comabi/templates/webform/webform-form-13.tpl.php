<?php
/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 *
 * If a preview is enabled, these keys will be available on the preview page:
 * - $form['preview_message']: The preview message renderable.
 * - $form['preview']: A renderable representing the entire submission preview.
 */
?>
<?php
global $variables;
global $tc_translations;


// Print out the progress bar at the top of the page
print drupal_render($form['progressbar']);

// Print out the preview message if on the preview page.
if (isset($form['preview_message'])) {
  print '<div class="messages warning">';
  print drupal_render($form['preview_message']);
  print '</div>';
}

$display = $variables['display'];
$output = '';

$status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
    'success' => t('Success'),
);


$result_errors = '';
foreach (drupal_get_messages($display) as $type => $messages) {

  foreach ($messages as $key => $value) {
    if ($type == 'status') {
      $result_errors = '<div id="success_form">' . $value . "</div>";
      break;
    }
    else {
      $result_errors.="" . $tc_translations['string-area-395'] . ": " . $value . "<br/>";
    }
  }

  if ($type == 'error') {
    $result_errors = '<div id="alerts_form">' . $result_errors . '</div>';
  }
}

if (strlen($result_errors) > 0) {
  print $result_errors;
}


print '<div id="contact_header">'. $tc_translations['string-area-424'] .'</div>';


// Print out the main part of the form.
// Feel free to break this up and move the pieces within the array.
print drupal_render($form['submitted']);


$form['actions']['previous'] = NULL;

// Always print out the entire $form. This renders the remaining pieces of the
// form that haven't yet been rendered above (buttons, hidden elements, etc).
print drupal_render_children($form);

$page_num = $form['details']['page_num']['#value'];
$page_count = $form['details']['page_count']['#value'];

// Check if its not the last step and catcha present in form.
if (($page_num == $page_count)) {
  print '<div id="contact_footer">("'. $tc_translations['string-area-293'] .'")</div>';
}
?>

<script>
  jQuery(document).ready(function() {
    if (jQuery('#alerts_form').length > 0) {
      jQuery('html, body').animate({
        scrollTop: jQuery("#node-13").offset().top
      }, 500);
    }
  });
</script>


