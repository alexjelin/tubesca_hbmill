<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<style>
#lightboxFrame body {
	margin: 0px;
}
</style>


<?php 

$counter=0;
foreach ($hreflang_results as $key=>$hreflang) {
		 
	$element = array(
	  '#tag' => 'link', // The #tag is the html tag - 
	  '#attributes' => array( // Set up an array of attributes inside the tag
		'href' => $hreflang[0], 
		'rel' => 'alternate',
		'hreflang' => $hreflang[1],
    ),
  ); 
  
  drupal_add_html_head($element, 'hreflang'.$counter);
  $counter++;
  
}

?> 
  
<div class="container">
<div class="inner_white clearfix product-info-wrapper">
  <h1 class="product_title"><?php print $title; ?></h1>
  <h2 class="product_subtitle"><?php print $sub_title; ?></h2>
  <div class="grey_separator"></div>
  <div class="row">
    <div class="col col-xs-12 col-sm-4 col-md-4 col-lg-5 hidden-xs hidden-sm"> <?php print $gallery_label; ?>
      <div class="images">
        <div class="col col-xs-12 col-sm-12 col-md-9 col-lg-9"> <?php print $gallery_base_image; ?>
          <div class="col col-xs-12 col-sm-12 col-md-3 col-lg-3"> <?php print $gallery_thumbnails; ?> </div>
        </div>
      </div>
      <div class="col col-xs-12 col-sm-12 col-md-8 col-lg-7">
        <div class="product_slick hidden-md hidden-lg"> <?php print render($mobile_slick); ?> </div>
        <div class="product_buttons_info clearfix  hidden-sm hidden-md hidden-lg"> <?php print $choose_model_btn; ?> <?php print $download_pdf_btn; ?></div>
        <div class="product-description"><?php print $top_axess; ?> <?php print render($content['body']); ?> <?php print $arguments; ?> </div>
      </div>
    </div>
    <div class="row">
      <div class="col col-xs-12 col-sm-5 col-md-4 col-lg-5">
        <div class="product_grey_info"> <?php print $usage; ?>
          <div class="clearfix"> <?php print $normes; ?> </div>
          <?php print $logo_enterprise; ?> <?php print $sold_in_france; ?> </div>
      </div>
      <div class="col col-xs-12 col-sm-7 col-md-8 col-lg-7 hidden-xs">
        <div class="product_buttons_info clearfix "> <?php print $download_pdf_btn; ?> <?php print $choose_model_btn; ?> </div>
        <?php print $compare_btn; ?> </div>
    </div>
  </div>
</div>
<?php print $middle_block; ?> <?php print $cross_selling; ?>
<?php print $flag_messages; ?>