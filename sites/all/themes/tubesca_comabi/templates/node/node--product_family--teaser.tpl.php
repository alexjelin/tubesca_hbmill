<div class="clearfix product-info-wrapper product-teaser">
  <div class="row">
    <h2 class="col-xs-12 product_title"><?php print $title; ?></h2>
    <h3 class="col-xs-12 product_subtitle"><?php print $sub_title; ?></h3>
    <div class="col-xs-6 col-md-3">
      <div class="product_image"><?php print $gallery_label; ?><?php print $image; ?></div>
    </div>
    <div class="col-xs-12 col-md-4 product-description"><?php print $arguments; ?></div>
    <div class="col-xs-12 col-md-5"> <?php print $view_the_product; ?>
      <div class="clearfix"></div>
      <?php print $choose_model_btn; ?>
      <div class="clearfix"></div>
      <?php print $compare_btn; ?></div>
  </div>
  <br/>
  <div class="row">
    <div class="col-xs-12">
      <div class="row">
        <?php if($normes!=''):?>
        <div class="col-xs-12 col-md-3 col normes" style="text-align: center;"><?php print $normes; ?></div>
        <?php endif;?>
        <?php if($usage!=''):?>
        <div class="col-xs-12 col-md-3 col" style="text-align: center;"><?php print $usage; ?></div>
        <?php endif;?>
        <?php if($logo_enterprise!=''):?>
        <div class="col-xs-12 col-md-3 col"><?php print $logo_enterprise; ?></div>
        <?php endif;?>
      </div>
    </div>
    <div class="col-xs-12 col-md-6"> </div>
  </div>
  <hr />
</div>
