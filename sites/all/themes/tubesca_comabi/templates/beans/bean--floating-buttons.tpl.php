<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<?php 
global $user;
global $language;
global $tc_translations, $tubesca_nav_vars;
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <div class="floating-buttons">
     <div id="floating-buttons-inner" class="right-form-form">
        <?php $links_aide_choix = $tubesca_nav_vars['help_choose']; ?>
        <div class="btn-wrapper"><a href="/<?php print $language->language.'/'.$links_aide_choix; ?>" class="floating-btn floatings-bulb"><span class="choose-bulb"><?php print $tc_translations['string-area-149'];?></span></a></div>  
        <div class="btn-wrapper"><?php
		 $links_mon_expert = $tubesca_nav_vars['my_expert']; 
		 $links_contact = url('node/13');   
		 if(($user->uid >0)&& !in_array(15, array_keys($user->roles))&& !in_array(11, array_keys($user->roles))){ ?> <a href="/<?php print $language->language.'/'.$links_mon_expert; ?>" class="floating-btn floatings-mail"><span class="contact-mail"><?php print $tc_translations['string-area-403'];?></span></a><?php }else {
		?><a href="<?php print $links_contact; ?>" class="floating-btn floatings-mail"><span class="contact-mail"><?php print $tc_translations['string-area-402'];?></span></a> <?php } ?></div> 
        <div class="btn-wrapper btn-open"><a class="floating-btn btn-plus floating-button-plus"></a></div>
     </div>
</div>
<script>
   jQuery('.floating-buttons').click(function() {   
    jQuery(this).toggleClass('show');
   })
   jQuery('.btn-open').click(function() {
    jQuery(this).toggleClass('btn-close');
   })
   jQuery('.floating-btn').not('.btn-plus').hover(function() { 
    jQuery('.floating-buttons').toggleClass('fb-width');
   })
   jQuery('.floating-btn.floatings-bulb').not('.btn-plus').hover(
    function() { 
      jQuery('.choose-bulb').css("display", "block");
	}, function(){
      jQuery('.choose-bulb').css("display", "none");
   })
   jQuery('.floating-btn.floatings-mail').not('.btn-plus').hover(
    function() { 
      jQuery('.contact-mail').css("display", "block");
	}, function(){
      jQuery('.contact-mail').css("display", "none");
   })
</script>
  </div>
</div>
