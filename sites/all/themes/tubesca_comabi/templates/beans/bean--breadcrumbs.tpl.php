<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
  global $tubesca_nav_vars;
?>
<?php if(arg(0)==$tubesca_nav_vars['my_list']){ 
global $language;
global $tc_translations;
?>

<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="container">
    <div class="breadcrumbs">
      <ol class="breadcrumb">
        <li><a href="/<?php print $language->language; ?>"><?php print strtolower($tc_translations['string-area-66']); ?> </a></li>
        <li class="active"><span><?php print strtolower($tc_translations['string-area-45']); ?></span></li>
      </ol>
      <div class="a2a_kit a2a_kit_size_32 a2a_default_style"><a class="mylist-doc" href="/<?php print $language->language; ?>/printpdf/<?php print $tubesca_nav_vars['wishlist_pdf']; ?>">&nbsp;</a><a class="mylist-pdf print-wishlist" href="">&nbsp;</a><a class="mylist-mail ctools-use-modal ctools-modal-modal-popup-medium" href="modal_forms/nojs/webform/862">&nbsp;</a></div>
    </div>
  </div>
</div>
<?php }elseif(isset($field_bg_image)){ ?>
<div class="<?php print $classes; ?> pane-bean-breadcrumbs clearfix"<?php print $attributes; ?>>
  <div class="container">
    <div class="breadcrumbs"> <?php print strtolower(theme('breadcrumb', array('breadcrumb'=>drupal_get_breadcrumb()))); ?>
      <div class="a2a_kit a2a_kit_size_32 a2a_default_style"><a class="a2a_dd" href="https://www.addtoany.com/share">
        <div class="share-widget">&nbsp;</div>
        </a></div>
    </div>
  </div>
</div>
<div class="bean_wrapper" <?php print $bgd_image; ?>>
  <div class="container">
    <div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
      <div class="breadcrumbs-banner">&nbsp;</div>
      <h1><?php print $banner_title; ?></h1>
      <h2><?php print $banner_desc; ?></h2>
    </div>
  </div>
</div>
<?php }else{?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="container">
    <div class="breadcrumbs"> 
 <?php print strtolower(theme('breadcrumb', array('breadcrumb'=>drupal_get_breadcrumb()))); ?>
      <div class="a2a_kit a2a_kit_size_32 a2a_default_style"><a class="a2a_dd" href="https://www.addtoany.com/share">
        <div class="share-widget">&nbsp;</div>
        </a></div>
    </div>
  </div>
</div>
<?php } ?>
<script async src="https://static.addtoany.com/menu/page.js"></script> 
