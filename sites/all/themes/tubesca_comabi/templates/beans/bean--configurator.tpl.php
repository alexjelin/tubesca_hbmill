<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
 
 global $tc_translations;
 
?>
<div class="bean-configurator">
<div class=" clearfix row">
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0px 30px;">
    <?php print $description_top; ?>
    <div id="filters" style="padding: 15px 0px;">
      <?php print $filters_form; ?>      
    </div>
    <?php print $description; ?>
    <p>&nbsp;</p>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    <p>&nbsp;</p>
    <?php print $the_image; ?>
    <?php print $add_to_list; ?>
   <br/><br/><br/><br/>
	<button class="btn tubesca_core_flag_fields_button" data-nid="" data-field='field_qty' data-flag="wishlist"><span><?php print $tc_translations['string-area-390']; ?></span><i class="fa fa-angle-right" aria-hidden="true"></i></button>
  </div>
  <?php print $access_denied_message; ?>
</div>
</div>
<script>
(function($) {
$(document).ready(function(e) {
	var master_div = $('#filters');
	  <?php print $filters_js; ?>
	start(myArray, myArray2);
	$('#reset').on('click',function(event){
		event.preventDefault() 
		start(myArray, myArray2);
	});
	function start(arr1, arr2){

		$('.tubesca_core_flag_fields_button').attr("disabled", true);
		for (var key in arr1) {
			switch(arr1[key]) {
				case 'slider_simple':
					$("#"+key+'_div').append('<div id="'+key+'"></div>');
					if ($('#'+key).hasClass('ui-slider')) {
							$('#'+key).unbind();
							$('#'+key).slider( "destroy" );
							
					}
					create_slider_simple(arr2, key);
				break;
				case 'slider_two_s':
					master_div.append('<div id="'+key+'"></div>');
					if ($('#'+key).hasClass('ui-slider')) {
							$('#'+key).unbind();
							$('#'+key).slider( "destroy" );
							
					}
					create_slider_two_s(arr2, key);
				break;
				case 'checkbox':	
					create_checkbox(arr2, key,'',1);
				break;
				case 'options':	
					create_options(arr2, key,'',1);
				break;
		
			}
		}
	}


	Array.prototype.diff = function(arr2) {
    var ret = [];
    for(var i in this) {   
        if(arr2.indexOf( this[i] ) > -1){
            ret.push( this[i] );
        }
    }
    return ret;
};

	function chage_filters(arr, field_name){
		var value = [];
		var for_compare = [];
		var keys = [];
		for (var key in myArray2) {
			switch(myArray[key]) {
				case 'slider_simple':
					value = [];
					if ($('#'+key).slider("option", "value") > 0){
						for_compare[key] = [];
						keys.push(key);
						for (var key2 in myArray2[key]) {
								value.push(myArray2[key][key2]*1);
						}
						var uniquevalue = [];
						$.each(value, function(i, el){
							if($.inArray(el, uniquevalue) === -1) uniquevalue.push(el);
						});
						value = uniquevalue;
						value.sort(function(a, b){return a-b});
						for (var key2 in myArray2[key]) {
							if ( myArray2[key][key2] == value[$('#'+key).slider("option", "value") - 1]) {
								for_compare[key].push(key2);
							}
						}
					}

				break;
				case 'slider_two_s':
					
				break;
				case 'checkbox':	
					checked_vals = [];
					change = [];
					$('#'+key).find('input[type=checkbox]').each(function(){
				
						if($(this).prop('checked')) {
							checked_vals.push($(this).val())
							
						}
					
					});
					$.each( checked_vals, function( index, data ) {
						$.each( myArray2[key], function( index_inner, data_inner ) {
							if (data == data_inner){
								change.push(index_inner);
							}
						});
					});
					if (change.length > 0){
						keys.push(key);
						for_compare[key] = change;
					}
				break;
				case 'options':	
				
					var checked_vals = [];
					var change = [];
					
					checked_vals.push($('#'+key + "_div SELECT").selectBox().val());
					$.each( checked_vals, function( index, data ) {
						$.each( myArray2[key], function( index_inner, data_inner ) {
							if (data == data_inner){
									change.push(index_inner);
							}
						});
					});
					
					
					if (change.length > 0){
						keys.push(key);
						for_compare[key] = change;
					}






				break;
		
			}
		}
		if (keys.length > 0){
			if (keys.length == 1){
				$('.tubesca_core_flag_fields_button').attr("disabled", false);
				$('.tubesca_core_flag_fields_button').attr('data-nid', for_compare[keys[0]][0]);
			} else {
				start_array =  for_compare[keys[0]];
				$.each( keys, function( index, value ) {
					start_array = start_array.diff(for_compare[value]) ;
				});
				if (start_array.length > 0){
					$('.tubesca_core_flag_fields_button').attr("disabled", false);
					$('.tubesca_core_flag_fields_button').attr('data-nid', start_array[0]);
				} else {
					$('.tubesca_core_flag_fields_button').attr("disabled", true);
				}
			}

		} else {
			$('.tubesca_core_flag_fields_button').attr("disabled", true);
		}
	}

	function create_options(arr, field_name, limit = [], first_time = 0){
			
			$('#'+field_name + "_div SELECT").selectBox('destroy');
			

			$('#'+field_name).unbind();
			$("#"+field_name+" option").attr('disabled','disabled');
			$("#"+field_name+" option[value=none]").removeAttr('disabled');

			if (first_time == 1){
				$("#"+field_name+" option[value=none]").prop('selected', true);
			}

			var check_array = [];

			if (limit.length > 0){
				$.each( limit, function( index, data ) {
					check_array.push(arr[field_name][data])
				});
			} else {
				check_array = arr[field_name];
			}

			$.each( check_array, function( index, data ) {
					$("#"+field_name+" option[value='"+data+"']").removeAttr('disabled');
			});			
			$('#'+field_name + "_div SELECT").selectBox();
			$('#'+field_name).change(function(){
				
				var checked_vals = [];
				var change = [];
				
				checked_vals.push($('#'+field_name + "_div SELECT").selectBox().val());
				$.each( checked_vals, function( index, data ) {
				$.each( arr[field_name], function( index_inner, data_inner ) {
						if (data == data_inner){
									if(limit.length > 0){
								if ($.inArray( index_inner, limit ) > -1){
									change.push(index_inner);
								}
							} else {
								change.push(index_inner);
							}
						
						}
					});
				});
				
				
				if (change.length > 0){
					var uniquevalue = [];
					$.each(change, function(i, el){
						if($.inArray(el, uniquevalue) === -1) uniquevalue.push(el);
					});
					change = uniquevalue;
					chage_filters(change, field_name);
				}

			});



	}
	
	function create_checkbox(arr, field_name, limit = [], first_time = 0){
		var curVal = [];
		if (first_time == 0){
	
			$('#'+field_name).find('input[type=checkbox]').each(function(){
					if($(this).prop('checked')) {
						curVal.push($(this).val())
					}
			});
		}
		$('#'+field_name).find('input[type=checkbox]').each(function(){
			$(this).prop('checked', false);
			$(this).addClass('disable_ckeckbox');
			$("label[for='"+$(this).attr('id')+"']").addClass('disable_ckeckbox');
			$("label[for='"+$(this).attr('id')+"']").removeClass('checked');
		});
		$('.'+field_name).unbind();
		var check_array = [];

		if (limit.length > 0){
			$.each( limit, function( index, data ) {
				check_array.push(arr[field_name][data])
			});
		} else {
			check_array = arr[field_name];
		}

		$.each( check_array, function( index, data ) {
				$('#'+field_name).find('input[type=checkbox]').each(function(){
					if ( $(this).val() == data){
						$("label[for='"+$(this).attr('id')+"']").removeClass('disable_ckeckbox');
						$(this).removeClass('disable_ckeckbox');
						if ($.inArray( $(this).val(), curVal ) != -1){
							$(this).prop('checked', true);
							$("label[for='"+$(this).attr('id')+"']").addClass('checked');
						}
					}
				});
		});
		
		$('.'+field_name).change(function() {	
			var checked_vals = []
			var change = []
			$("label[for='"+$(this).attr('id')+"']").toggleClass('checked');
			$('#'+field_name).find('input[type=checkbox]').each(function(){
				
				if($(this).prop('checked')) {
					checked_vals.push($(this).val())
					
				}
			
			});
			$.each( checked_vals, function( index, data ) {
				$.each( arr[field_name], function( index_inner, data_inner ) {
					if (data == data_inner){
															if(limit.length > 0){
								if ($.inArray( index_inner, limit ) > -1){
									change.push(index_inner);
								}
							} else {
								change.push(index_inner);
							}

					}
				});
			});

			
			if (change.length > 0){
				var uniquevalue = [];
				$.each(change, function(i, el){
					if($.inArray(el, uniquevalue) === -1) uniquevalue.push(el);
				});
				change = uniquevalue;
				
			}
			chage_filters(change, field_name);
		});
		
	}

	function create_slider_simple(arr, field_name, limit = []){
		var curValue = $('#'+field_name + ' .valuebox').text();
		var value = [];
		var change = [];

		if ($('#'+field_name).hasClass('ui-slider')) {
			$('#'+field_name).unbind();
			$('#'+field_name).slider( "destroy" );
			
		}
		
		for (var key in arr[field_name]) {
			if(limit.length > 0){
				if ($.inArray( key, limit ) > -1){
					value.push(arr[field_name][key]*1);
				}
			} else {
				value.push(arr[field_name][key]*1);
			}
			
		}
		var uniquevalue = [];
		$.each(value, function(i, el){
			if($.inArray(el, uniquevalue) === -1) uniquevalue.push(el);
		});
		value = uniquevalue;

		var curData = '<?php print $tc_translations['string-area-420']; ?>';
		if (curValue != '' && curValue != '<?php print $tc_translations['string-area-420']; ?>'){
			$.each( value, function( index, data ) {
				if (data == curValue*1){
					
					curValue = index+1;
					curData = value[index];
				}
			});
		} else {
			curValue = 0;	
		}
		value.sort(function(a, b){return a-b});
		$("#"+field_name).slider({
        min: 0,
        max: value.length,
		value: curValue,
		create: function(){
			var handle = $(this).find('.ui-slider-handle');
			var bubble = $('<div class="valuebox">'+curData+'</div>');
			
			handle.append(bubble);
		},
		slide: function(event, ui) {
			currentState = value[ui.value-1];
			if (ui.value == 0) {
				currentState = '';
			}
			$('#'+field_name+' .valuebox').text(currentState);	
        }       
		}).on("slidechange", function(e,ui) {
			change = [];
			for (var key in arr[field_name]) {
				if ( arr[field_name][key] == value[ui.value-1] || ui.value == 0){
					if(limit.length > 0){
						if ($.inArray( key, limit ) > -1){
							change.push(key);
						}
					} else {
						change.push(key);
					}
				} 
			}
			chage_filters(change, field_name);
		});
		
	}
	
	function create_slider_two_s(arr, field_name, limit = []){
		
		var curVal = '';
		var value = {};
		if ($('#'+field_name).hasClass('ui-slider')) {
			curVal = $('#'+field_name).slider("option", "values");
			$('#'+field_name).unbind();
			$('#'+field_name).slider( "destroy" );
			
		}

		
		for (var key in arr[field_name]) {	
			if(limit.length > 0){
				if ($.inArray( key, limit ) != -1){
					 value[key]= arr[field_name][key];
				}
			} else {
				  value[key]= arr[field_name][key];	
			}
		}
		
		var firstKey="";
		for(firstKey in value) break;
		
		var min = value[firstKey];
		var max = value[firstKey];
		
		for (var key in value) {
			if (min > value[key]) {
				min = value[key]
			}
			if (max < value[key]) {
				max = value[key]
			}

		}
		var val_min = min;
		var val_max = max;

		if (curVal.length == 2){
			if (1*min <= curVal[0] && curVal[0] <= 1*max){
				val_min = curVal[0];
			}
			if (min <= curVal[1] && curVal[1]<= max){
				val_max = curVal[1];
			}
		}

		$( "#"+field_name ).slider({
			range:true,
			min: 1*min,
			max: 1*max,
			values: [ 1*val_min, 1*val_max ],
			create: function(){
				var handle = $(this).find('.ui-slider-handle');
				var bubble = $('<div class="valuebox"></div>');
				handle.append(bubble);
			},
			slide: function( event, ui ) {
				$(this).find('.ui-slider-handle:first .valuebox').text(ui.values[0]);
				$(this).find('.ui-slider-handle:last .valuebox').text(ui.values[1]);
			}
		}).on("slidechange", function(e,ui) {
		
			change = [];
			for (var key in arr[field_name]) {
				if (ui.values[0] <= arr[field_name][key] && arr[field_name][key] <= ui.values[1]){
					if(limit.length > 0){
						if ($.inArray( key, limit ) > -1){
							change.push(key);
						}
					} else {
						change.push(key);
					}
				} 
			}
			if (ui.values[1] != ui.values[0]) {
				chage_filters(change, field_name);
			}
			
		});
	}
});
})(jQuery);
</script>
<style>
  #filters > .filter_title{
		display: none;
	}
	#reset{
		margin-bottom: 30px;
		display: none;
	}
</style>