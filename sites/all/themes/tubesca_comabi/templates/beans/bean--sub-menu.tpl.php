<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
global $language;

	$tree = menu_tree_all_data('menu-mid-menu');
	foreach ($tree as $branch) {
		if ($branch['link']['mlid'] == $menu_id) {
			$menu_items =  $branch['below'];
		}
	}
	$i=0;
	$html ='';
	foreach ($menu_items as $branch) {
		$tax = '';
		$node_no_tax = 0;
		$html .= '<div class="panel panel-default">
						<div class="panel-heading">';
						if ($mega_menu == 1){
							
							$tids = array();
							$nids = array();
							$html .= '<a data-href="/'.$language->language.'/'.drupal_get_path_alias($branch['link']['link_path'],$language->language).'" data-toggle="collapse" data-parent="#accordion-menu-'.$menu_id.'" href="#col'.$menu_id.'lapse'.$i.'"><span class="accordion-heading right"><span class="sub-accordion-heading">'.$branch['link']['link_title'].'</span></span></a></div>
						<div id="col'.$menu_id.'lapse'.$i.'" class="panel-collapse collapse in">';
						  $html .= '<div class="panel-body">';
							foreach ($branch['below'] as $taxonomy_menu){
								if(strpos($taxonomy_menu['link']['link_path'], 'taxonomy/term/') !== false) {
								  $tids[] = str_replace('taxonomy/term/','',$taxonomy_menu['link']['link_path']);
								}else{	
								  $tids[] = str_replace('node/','',$taxonomy_menu['link']['link_path']);
									$node_no_tax = 1;
								}
								foreach ($taxonomy_menu['below'] as $nodes){
									$nids[] = str_replace('node/','',$nodes['link']['link_path']);
									break;
								}

							}
						//preprintr($tids);
						//array_shift($tids);
						//array_shift($nids);
						
						if($node_no_tax){
							$html .='<div class="hidden-xs hidden-sm hidden-md">'.views_embed_view("sub_menu_node", "block", implode(',',$tids)).'</div>
								<div class="hidden-lg">'.views_embed_view("sub_menu_node", "block_1", implode(',',$tids)).'</div> ';
								 $html .= '</div></div>';
						}else{
						  $html .='<div class="hidden-xs hidden-sm hidden-md">'.views_embed_view("sub_menu_taxonomy", "block", implode(',',$tids)).'</div>
								<div class="hidden-lg">'.views_embed_view("sub_menu_taxonomy", "block_1", implode(',',$tids)).'</div> ';
								$html .= '</div>';
								foreach($nids as $nid){
					        $html .= views_embed_view("product_family_tab", "block", $nid);
								}
								$html .= '</div>';
						}
							} else {
							$html .= '<a href="'.url($branch['link']['link_path']).'"><span class="accordion-heading right"><span class="sub-accordion-heading">'.$branch['link']['link_title'].'</span></span></a></div>';
						}

						$html .='</div>';
						$i++;
	}


?>

<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
	<div class="panel-group product-tabs-vertical tb-mega-sub-menu" id="accordion-menu-<?php print $menu_id; ?>">
		<?php print $html; ?>
  </div>
</div>
