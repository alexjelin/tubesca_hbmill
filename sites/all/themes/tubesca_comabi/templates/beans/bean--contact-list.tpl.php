<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
 
?>
<?php global $tc_translations; ?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="row content"<?php print $content_attributes; ?>>
    <div class="block-icon"><?php print $top_icon; ?></div>
    <div class="block-title"><?php print $block_title; ?></div>
    <div class="clearfix"></div> 
    <div class="contact-info-wrapper collapse">
        <?php if($directeur_des_ventes!=''){?>
        <div class="contact-info">
             <h3 class="uppercase"><?php print $directeur_des_ventes_label; ?></h3>  
             <span><?php print $directeur_des_ventes; ?></span>
        </div> 
        <?php } ?>
        <div class="contact-info">     
             <h3> <?php print $tc_translations['string-area-70']." : ";?> </h3>
             <span>9h00 - 12h00 | 14h00 - 17h00</span></br>
        </div> 
        <?php if($responsable_region_tel!=''||$responsable_region_mail!=''||$responsable_region!=''){ ?>    
        <div class="contact-info version-2">     
             <h3 class="uppercase"><?php print $responsable_region_label; ?></h3>
             <?php if($responsable_region!=''){?><span class="uppercase"><?php print $responsable_region; ?></span><?php } ?>
             <?php if($responsable_region_mail!=''){?><a href="mailto:<?php print $responsable_region_mail; ?>"><?php print $responsable_region_mail; ?></a></br><?php } ?>
             <?php if($responsable_region_tel!=''){?><span class="contact-tel"><a href="tel:<?php print $responsable_region_tel; ?>"><?php print $responsable_region_tel; ?></a></span><?php } ?>
        </div>
        <?php } ?>
        <?php if($adv!=''||$adv_mail!=''||$adv_tel!=''){ ?>   
        <div class="contact-info version-2"> 
             <h3 class="uppercase"><?php print $adv_label; ?></h3>
             <?php if($adv!=''){?><span class="uppercase"><?php print $adv; ?></span><?php } ?>
             <?php if($adv_mail!=''){?><?php print $adv_mail; ?><?php } ?>
             <?php if($adv_tel!=''){?><?php print $adv_tel; ?><?php } ?>
        </div>
        <?php } ?>
        <?php if($pole_commande!=''||$pole_commande_mail!=''||$pole_commande_tel!=''){ ?>  
        <div class="contact-info">   
             <h3><?php print $pole_commande_label; ?></h3>
             <?php if($pole_commande!=''){?><span class="uppercase"><?php print $pole_commande; ?></span><?php } ?>
             <?php if($pole_commande_mail!=''){?><a href="mailto:<?php print $pole_commande_mail; ?>"><?php print $pole_commande_mail; ?></a></br><?php } ?>
             <?php if($pole_commande_tel!=''){?><span class="contact-tel"><a href="tel:<?php print $pole_commande_tel; ?>"><?php print $pole_commande_tel; ?></a></span><?php } ?>
        </div>
        <?php } ?>
        <?php if($sav_lourd_horaires!=''||$sav_lourd_mail!=''||$sav_lourd_tel!=''||$sav_leger_horaires!=''||$sav_leger_mail!=''||$sav_leger_tel!=''){ ?>
        <div class="contact-info">   
             <h3><?php print $sav_lourd_label; ?></h3>
             <?php if($sav_lourd_horaires!=''){?><span><?php print $sav_lourd_horaires; ?></span></br></br><?php } ?>
             <?php if($sav_lourd_mail!=''){?><a href="mailto:contac@tubesca-comabi.com "><?php print $sav_lourd_mail; ?></a></br><?php } ?>
             <?php if($sav_lourd_tel!=''){?><span class="contact-tel"><?php print $sav_lourd_tel; ?></span></br><?php } ?>
             <h3><?php print $sav_leger_label; ?></h3>
             <?php if($sav_leger_horaires!=''){?><span><?php print $sav_leger_horaires; ?></span></br></br><?php } ?>
             <?php if($sav_leger_mail!=''){?><a href="mailto:contac@tubesca-comabi.com "><?php print $sav_leger_mail; ?></a></br><?php } ?>
             <?php if($sav_leger_tel!=''){?><span class="contact-tel"><a href="tel:<?php print $sav_leger_tel; ?>"><?php print $sav_leger_tel; ?></a></span></br><?php } ?>
        </div>
        <?php } ?>
        <?php print $static_contact; ?>
        <div class="clearfix"></div>
        <a class="contact-list-btnv2" href="<?php print $link ?>"><?php print $link_title ?></a>
    </div>
        <button data-toggle="collapse" data-target=".contact-info-wrapper" class="toogle-button"></button>
         
  </div>
</div>
<script>
   jQuery('.toogle-button').click(function() {
      jQuery(this).toggleClass('clicked');
   });
</script>