<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
 
global $language;
global $tubesca_nav_vars;

$lang = $language->language;
$link_country_selector = $tubesca_nav_vars['country_selector'];

?>

<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
	<a href="/<?php print $redirect_switcher_url; ?>" class="btn btn-default select_region"><span class="globe"><i class="fa fa-globe" aria-hidden="true"></i></span><span class="region_name"><?php echo $country_label." - ".$lang_label; ?></span><span class="select_region_caret"><i class="fa fa-caret-right" aria-hidden="true"></i></span></a>
  </div>
</div>
