<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
 global $language;
 global $tc_translations;
     $options = array();
 
    $vid = taxonomy_vocabulary_machine_name_load('family')->vid;
 
    $options_source = taxonomy_get_tree($vid);

	$select = "<select name='tax_select' class='tax_select'>";
    foreach($options_source as $item ) {
		$selected = '';
		if (arg(2) == $item->tid){
			$selected = "selected";
		}
		if ($item->depth > 0){
			$name = $item->name;
			$select .= "<option value='/".$language->language."/".drupal_get_path_alias('taxonomy/term/' . $item->tid)."' ".$selected.">".$name ."</option>";
		}
		
    }
	$select .="</select>";
	
?>
<div class="<?php print $classes; ?> clearfix container">
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="inner_white clearfix">
			<div class="mobile_filter_menu"><span>FILTER / TRIER <i class="fa fa-chevron-circle-up" aria-hidden="true"></i><i class="fa fa-chevron-circle-down" aria-hidden="true"></i></span></div>
			<div id="filters">
				<div class="change_taxonomy">
					<form  method="post" id="taxonomy">
						<?php print $select; ?>
					</form>	
					
				</div>	
				

				<div class="filter_info">

				</div>
				<?php
				  print $filters_form;
				  //print render($filters_js);
				?>
				<p class="form">					
					<form target="_self" method="post" name="config">
						<input id="nids_input" type="hidden" name="nids" value="">
						<input type="submit" value="<?php print $tc_translations['string-area-179'];?>" class="btn round-btn blue-light-btn" style="visibility: hidden;">
					</form>	
				</p>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		<div class="inner_white clearfix">
			<h1 class="facets-title"><?php print $filters_form_title; ?></h1>
			<?php
				print $faceted_search;
			?>
		</div>
	</div>
</div>
<?php print $flag_messages; ?>
<script>
(function($) {
$(document).ready(function(e) {
	$('.mobile_filter_menu').on('click', function(){
		$('#filters').toggle();
		$('.fa-chevron-circle-up').toggle();
		$('.fa-chevron-circle-down').toggle();
	});
	$('.tax_select').on('change', function() {

		var targetvalue = $(this).val();
		$("#taxonomy").prop("action", targetvalue);
		$("#taxonomy").submit();
	});




	var tid = <?php print arg(2); ?>;
	var master_div = $('#filters');
		<?php print $filters_js; ?>
	start(myArray, myArray2);
	$('#reset').on('click',function(event){
		event.preventDefault();
		start(myArray, myArray2);
		var custom_settings = {};
					custom_settings.url = '/<?php print $language->language; ?>/tubesca/filters/'+tid;
					Drupal.ajax['custom_ajax_action'] = new Drupal.ajax(null, $(document.body), custom_settings);
					Drupal.ajax['custom_ajax_action'].specifiedResponse();
	});
	function start(arr1, arr2){
		$('#nids_input').val('');
		for (var key in arr1) {
			switch(arr1[key]) {
				case 'slider_simple':
					$("#"+key+'_div').append('<div id="'+key+'"></div>');
					if ($('#'+key).hasClass('ui-slider')) {
							$('#'+key).unbind();
							$('#'+key).slider( "destroy" );
							
					}
					$('.filter_info #'+key + '_info_div').remove();
					$('.filter_info').append('<div id="'+key + '_info_div'+'"></div>');
					create_slider_simple(arr2, key);
				break;
				case 'slider_two_s':
					$("#"+key+'_div').append('<div id="'+key+'"></div>');
					if ($('#'+key).hasClass('ui-slider')) {
							$('#'+key).unbind();
							$('#'+key).slider( "destroy" );
							
					}
					create_slider_two_s(arr2, key);
				break;
				case 'checkbox':	
					create_checkbox(arr2, key,'',1);
				break;
				case 'options':	
					$('.filter_info #'+key + '_info_div').remove();
					$('.filter_info').append('<div id="'+key + '_info_div'+'"></div>');
					create_options(arr2, key,'',1);
				break;
		
			}
		}
	}
	function chage_filters(arr, field_name){
		var nids ='';
		$.each( arr, function( index, data ) {
			nids =data  +','+ nids;
			
		});
		nids=nids.slice(0,-1);
		$('#nids_input').val(nids);
		var custom_settings = {};
					custom_settings.url = '/<?php print $language->language; ?>/tubesca/filters/'+tid+'/'+nids;
					Drupal.ajax['custom_ajax_action'] = new Drupal.ajax(null, $(document.body), custom_settings);
					Drupal.ajax['custom_ajax_action'].specifiedResponse();
		for (var key in myArray) {
			if(field_name != key){
				switch(myArray[key]) {
					case 'slider_simple':						
						create_slider_simple(myArray2, key, arr);
					break;
					case 'slider_two_s':
						create_slider_two_s(myArray2, key, arr);
						
/*
						$('#'+key).find('.ui-slider-handle:first .valuebox').text(min);
						$('#'+key).find('.ui-slider-handle:last .valuebox').text(max);*/
						
					break;
					case 'checkbox':	
						create_checkbox(myArray2, key, arr);
					break;
					case 'options':	
						create_options(myArray2, key, arr);
					break;
			
				}
			}
		}
	}

	function create_options(arr, field_name, limit = [], first_time = 0){
			
			$('#'+field_name + "_div SELECT").selectBox('destroy');
			

			$('#'+field_name).unbind();
			$("#"+field_name+" option").attr('disabled','disabled');
			$("#"+field_name+" option[value=none]").removeAttr('disabled');

			if (first_time == 1){
				$("#"+field_name+" option[value=none]").prop('selected', true);
			}

			var check_array = [];

			if (limit.length > 0){
				$.each( limit, function( index, data ) {
					check_array.push(arr[field_name][data])
				});
			} else {
				check_array = arr[field_name];
			}

			$.each( check_array, function( index, data ) {
					$("#"+field_name+" option[value='"+data+"']").removeAttr('disabled');
			});			
			$('#'+field_name + "_div SELECT").selectBox();
			$('#'+field_name).change(function(){
				
				var checked_vals = [];
				var change = [];
				if ($('#'+field_name + "_div SELECT").selectBox().val() != 'none')
				{
				
					$('#'+field_name + '_info_div').html($('#'+field_name + '_div h3').text()+' : '+$('#'+field_name + "_div SELECT").selectBox().val()+''+$('#'+field_name + '_div .info').text());
					$('#'+field_name + '_info_div').addClass('filter_border');
				} else {
					$('#'+field_name + '_info_div').removeClass('filter_border');
					$('#'+field_name + '_info_div').html('');
				}
			
				checked_vals.push($('#'+field_name + "_div SELECT").selectBox().val());
				$.each( checked_vals, function( index, data ) {
				$.each( arr[field_name], function( index_inner, data_inner ) {
						if (data == data_inner){
									if(limit.length > 0){
								if ($.inArray( index_inner, limit ) > -1){
									change.push(index_inner);
								}
							} else {
								change.push(index_inner);
							}
						
						}
					});
				});
				
				
				
				if (change.length > 0){
					var uniquevalue = [];
					$.each(change, function(i, el){
						if($.inArray(el, uniquevalue) === -1) uniquevalue.push(el);
					});
					change = uniquevalue;
					chage_filters(change, field_name);
				}

			});



	}
	
	function create_checkbox(arr, field_name, limit = [], first_time = 0){
		var curVal = [];
		if (first_time == 0){
	
			$('#'+field_name).find('input[type=checkbox]').each(function(){
					if($(this).prop('checked')) {
						curVal.push($(this).val())
					}
			});
		}
		$('#'+field_name).find('input[type=checkbox]').each(function(){
			$(this).prop('checked', false);
			$(this).addClass('disable_ckeckbox');
			$("label[for='"+$(this).attr('id')+"']").addClass('disable_ckeckbox');
			$("label[for='"+$(this).attr('id')+"']").removeClass('checked');
		});
		$('.'+field_name).unbind();
		var check_array = [];

		if (limit.length > 0){
			$.each( limit, function( index, data ) {
				check_array.push(arr[field_name][data])
			});
		} else {
			check_array = arr[field_name];
		}

		$.each( check_array, function( index, data ) {
				$('#'+field_name).find('input[type=checkbox]').each(function(){
					if ( $(this).val() == data){
						$("label[for='"+$(this).attr('id')+"']").removeClass('disable_ckeckbox');
						$(this).removeClass('disable_ckeckbox');
						if ($.inArray( $(this).val(), curVal ) != -1){
							$(this).prop('checked', true);
							$("label[for='"+$(this).attr('id')+"']").addClass('checked');
						}
					}
				});
		});
		
		$('.'+field_name).change(function() {	
			var checked_vals = []
			var change = []
			$("label[for='"+$(this).attr('id')+"']").toggleClass('checked');
			$('#'+field_name).find('input[type=checkbox]').each(function(){
				
				if($(this).prop('checked')) {
					checked_vals.push($(this).val())
					
				}
			
			});
			$.each( checked_vals, function( index, data ) {
				$.each( arr[field_name], function( index_inner, data_inner ) {
					if (data == data_inner){
															if(limit.length > 0){
								if ($.inArray( index_inner, limit ) > -1){
									change.push(index_inner);
								}
							} else {
								change.push(index_inner);
							}

					}
				});
			});


			
			if (change.length > 0){
				var uniquevalue = [];
				$.each(change, function(i, el){
					if($.inArray(el, uniquevalue) === -1) uniquevalue.push(el);
				});
				change = uniquevalue;
				
			}
			if(change.length > 0){
				chage_filters(change, field_name);
			}else {
				chage_filters(limit, field_name);
			}
		});
		
	}

	function create_slider_simple(arr, field_name, limit = []){
		var curValue = $('#'+field_name + ' .valuebox').text();
		var value = [];
		var change = [];

		if ($('#'+field_name).hasClass('ui-slider')) {
			$('#'+field_name).unbind();
			$('#'+field_name).slider( "destroy" );
			
		}
		
		for (var key in arr[field_name]) {
			if(limit.length > 0){
				if ($.inArray( key, limit ) > -1){
					value.push(arr[field_name][key]*1);
				}
			} else {
				value.push(arr[field_name][key]*1);
			}
			
		}
		var uniquevalue = [];
		$.each(value, function(i, el){
			if($.inArray(el, uniquevalue) === -1) uniquevalue.push(el);
		});
		value = uniquevalue;
		var curData = 'choisir';
		if (curValue != '' && curValue != 'choisir'){
			$.each( value, function( index, data ) {
				if (data == curValue*1){
					
					curValue = index+1;
					curData = value[index];
				}
			});
		} else {
			curValue = 0;	
		}
		min_val = 0;
		if (curValue != 0)
		{
			min_val=1;
		}
		value.sort(function(a, b){return a-b});
		$("#"+field_name).slider({
        min: min_val,
        max: value.length,
		value: curValue,
		create: function(){
			var handle = $(this).find('.ui-slider-handle');
			var bubble = $('<div class="valuebox">'+curData+'</div>');
			
			handle.append(bubble);
		},
		slide: function(event, ui) {
			currentState = value[ui.value-1];
			if (ui.value == 0) {
				currentState = '';
			}
			$('#'+field_name+' .valuebox').text(currentState);	
        }       
		}).on("slidechange", function(e,ui) {
			change = [];
			if (ui.value != 0){
				$('#'+field_name + '_info_div').html($('#'+field_name + '_div h3').text()+' : '+value[ui.value-1]+''+$('#'+field_name + '_div .info').text());
				$('#'+field_name + '_info_div').addClass('filter_border');
			} else {
				$('#'+field_name + '_info_div').removeClass('filter_border');
				$('#'+field_name + '_info_div').html('');
			}

			
			for (var key in arr[field_name]) {
				if ( arr[field_name][key] == value[ui.value-1] || ui.value == 0){
					if(limit.length > 0){
						if ($.inArray( key, limit ) > -1){
							change.push(key);
						}
					} else {
						change.push(key);
					}
				} 
			}
			chage_filters(change, field_name);
		});
		
	}
	
	function create_slider_two_s(arr, field_name, limit = []){
		var curVal = '';
		var value = {};
		if ($('#'+field_name).hasClass('ui-slider')) {
			curVal = $('#'+field_name).slider("option", "values");
			$('#'+field_name).unbind();
			$('#'+field_name).slider( "destroy" );
			
		}
		
		for (var key in arr[field_name]) {	
			if(limit.length > 0){
				if ($.inArray( key, limit ) != -1){
					 value[key]= arr[field_name][key];
				}
			} else {
				  value[key]= arr[field_name][key];	
			}
		}
		
		var firstKey="";
		for(firstKey in value) break;
		
		var min = Math.floor(value[firstKey]*1);
		var max = Math.ceil(value[firstKey]*1);
		for (var key in value) {
			if (min > value[key]*1) {
				min = Math.floor(value[key]*1);
			}
			if (max < value[key]*1) {
				max =  Math.ceil(value[key]*1);
			}
		}
		var val_min = min;
		var val_max = max;
		if (curVal.length == 2){
			if (1*min <= curVal[0] && curVal[0] <= 1*max){
				val_min =  Math.floor(curVal[0]);
			}
			if (min <= curVal[1] && curVal[1]<= max){
				val_max =  Math.ceil(curVal[1]);
			}
		}
		
		$( "#"+field_name ).slider({
			range:true,
			min: 1*min,
			max: 1*max,
			values: [ 1*val_min, 1*val_max ],
			create: function(){
				var handle = $(this).find('.ui-slider-handle');
				var bubble = $('<div class="valuebox"></div>');
				handle.append(bubble);
			},
			slide: function( event, ui ) {
				$(this).find('.ui-slider-handle:first .valuebox').text(ui.values[0]);
				$(this).find('.ui-slider-handle:last .valuebox').text(ui.values[1]);
			}
		}).on("slidechange", function(e,ui) {
		
			change = [];
			for (var key in arr[field_name]) {
				if (ui.values[0] <= arr[field_name][key] && arr[field_name][key] <= ui.values[1]){
					if(limit.length > 0){
						if ($.inArray( key, limit ) > -1){
							change.push(key);
						}
					} else {
						change.push(key);
					}
				} 
			}
			if (ui.values[1] != ui.values[0]) {
				chage_filters(change, field_name);
			}
			
		});
	}
});
})(jQuery);
</script>