<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}  
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
 
?>
<?php
  global $tc_translations, $tubesca_nav_vars, $language;
	$pf_exids = array();
	$pf_exids[1] = "ECHAFAUDAGE MAÇON M49-M49 - MACON CLE EN MAIN FORMULE 6";
	$pf_exids[2] = "tcFamECHAFAUDAGES-FACADER-200-Progress-FACADE CLE EN MAIN FORMULE 1,tcFamECHAFAUDAGES-FACADER-200-Progress-FACADE CLE EN MAIN FORMULE 2";
	$pf_exids[3] = "ECHAFAUDAGE MULTIDIRECTIONNEL-M368 - MULTI CLE EN MAIN FORMULE 4,ECHAFAUDAGE MULTIDIRECTIONNEL-M368 - MULTI CLE EN MAIN FORMULE 5";
	$pf_exids[4] = "tcProdACCES-LEGERECHAFAUDAGESGOLIATH-120-composition,tcProdACCES-LEGERECHAFAUDAGESGOLIATH-180-composition";
	$pf_exids[5] = "tcProdACCES-LEGERECHAFAUDAGESSK4-et-SK6";
	$pf_exids[6] = "tcProdACCES-LEGERECHAFAUDAGESJUNIOR-BANCHE-composition";
	$pf_exids[7] = "tcProdACCES-LEGERECHAFAUDAGESSK-10-pliant-pro";
	$pf_exids[8] = "tcProdACCES-LEGERECHAFAUDAGESJUNIOR-150-composition,tcProdACCES-LEGERECHAFAUDAGESJUNIOR-200---composition";
	$pf_exids[9] = "tcProdACCES-LEGERECHAFAUDAGESGENERIS-300/4---Composition";
	$pf_exids[10] = "tcProdACCES-LEGERECHAFAUDAGESSPEEDY-2----composition";
	$pf_exids[11] = "tcProdACCES-LEGERECHAFAUDAGEST-ONE-180-fixe---composition,tcProdACCES-LEGERECHAFAUDAGEST-ONE-155-pliant---composition,tcFamACCES-LEGERECHAFAUDAGES_T-ONE 155 FIXE 3T_PRODUIT,tcFamACCES-LEGERECHAFAUDAGES_T-ONE 180 FIXE 3T_PRODUIT,tcFamACCES-LEGERECHAFAUDAGES_T-ONE 155 PLIANT 3T_PRODUIT,tcProdACCES-LEGERECHAFAUDAGEST-ONE-180-Export,tcProdACCES-LEGERECHAFAUDAGEST-ONE-155-Export";
	$pf_exids[12] = "tcProdACCES-LEGERECHAFAUDAGESINSIDER-120-COLOR---composition,tcProdACCES-LEGERECHAFAUDAGESINSIDER-120-SX-PLIANT---composition";
	$pf_exids[13] = "tcProdACCES-LEGERECHAFAUDAGESGENERIS-750/3---composition,tcProdACCES-LEGERECHAFAUDAGESGENERIS-850---composition,tcProdACCES-LEGERECHAFAUDAGESGENERIS-950---composition";
	$pf_exids[14] = "tcProdACCES-LEGERECHAFAUDAGESTOUR-GENERIS-900---composition";
	$pf_exids[15] = "##C8B8B28F-48E2-4E35-8981-933F50ACE4FC";
	$pf_exids[16] = "tcProdACCES-LEGERECHAFAUDAGESJUNIOR-250-composition";
	$pf_exids[17] = "tcProdACCES-LEGERECHAFAUDAGESCROSS-180";
	$pf_exids[18] = "tcProdACCES-LEGERECHAFAUDAGESTOTEM-2-BOX-180---composition,tcProdACCES-LEGERECHAFAUDAGESTOTEM-2-BOX-250---composition,tcProdACCES-LEGERECHAFAUDAGESTOTEM-2-BOX-300----composition,tcProdACCES-LEGERECHAFAUDAGESTOTEM-2-LINE-180----composition,tcProdACCES-LEGERECHAFAUDAGESTOTEM-2-LINE-250---composition,tcProdACCES-LEGERECHAFAUDAGESTOTEM-2-LINE-300----composition";
	$pf_exids[19] = "tcProdACCES-LEGERECHAFAUDAGESSTARLIUM-800---composition";
	$pf_exids[20] = "tcProdACCES-LEGERECHAFAUDAGESSTARLIUM-600-escalier---composition";
	$pf_exids[21] = "tcProdACCES-LEGERECHAFAUDAGESPROLIUM-BOX-180-SX---composition,tcProdACCES-LEGERECHAFAUDAGESPROLIUM-BOX-250-SX---composition,##151C9163-E000-4504-8D84-E9125CA1DFF7,##D389A2C8-239A-4C52-B74C-E8F529618846,tcProdACCES-LEGERECHAFAUDAGESPROLIUM-line-400-lisses---composition,tcProdACCES-LEGERECHAFAUDAGESPROLIUM-line-400-SX---composition,tcProdACCES-LEGERECHAFAUDAGESPROLIUM-line-600-lisses---composition,tcProdACCES-LEGERECHAFAUDAGESPROLIUM-line-600-SX---composition";
	$pf_exids[22] = "tcFamACCES-LEGERPLATES-FORMES-COBRA BANC DE PLATRIER";
	$pf_exids[23] = "tcProdACCES-LEGERPLATES-FORMESTETRIS-100";
	$pf_exids[24] = "tcProdACCES-LEGERPLATES-FORMESHELIS";
	$pf_exids[25] = "tcProdACCES-LEGERPLATES-FORMESPASSERELLE-BANCHE";
	$pf_exids[26] = "tcProdACCES-LEGERPLATES-FORMESPLATE-FORME-SECURITY";
	$pf_exids[27] = "tcProdACCES-LEGERPLATES-FORMESPLATE-FORME-TRUCK'ACCES";
	$pf_exids[28] = "tcFamACCES-LEGERECHELLES-KLIPEO PACK";
	$pf_exids[29] = "tcProdACCES-LEGERECHELLESECHELLE-DE-COUVREUR-EN-EPICEA";
	$pf_exids[30] = "tcFamACCES-LEGERECHELLES-ECHELLE LAVEUR DE VITRE";
	$pf_exids[31] = "##7BAA073F-E265-427E-A2EA-9F8A44A63BBB";
	$pf_exids[32] = "tcProdACCES-LEGERECHELLESECHELLE-DE-FOUILLE";
	$pf_exids[33] = "tcProdACCES-LEGERECHELLESTRANSFORMABLE-ISOLANTE-2-plans";
	$pf_exids[34] = "tcProdACCES-LEGERECHELLESPLATINIUM-TRANSFORMABLE-2-plans";
	$pf_exids[35] = "tcProdACCES-LEGERECHELLESPRONOR-TRANSFORMABLE-2-plans";
	$pf_exids[36] = "tcProdACCES-LEGERECHELLESPROLINE-TRANSFORMABLE-2-plans";
	$pf_exids[37] = "tcProdACCES-LEGERECHELLESPLATINIUM-TRANSFORMABLE-3-plans";
	$pf_exids[38] = "tcProdACCES-LEGERECHELLESPRONOR-TRANSFORMABLE-3-plans";
	$pf_exids[39] = "tcProdACCES-LEGERECHELLESPROLINE-TRANSFORMABLE-3-plans";
	$pf_exids[40] = "tcProdACCES-LEGERECHELLESPLATINIUM-300-COULISSE-CORDE-2-plans,tcProdACCES-LEGERECHELLESPLATINIUM-500-COULISSE-CORDE-2-plans";
	$pf_exids[41] = "tcFamACCES-LEGERECHELLES-PLATINIUM 300 AGRIP COULISSE CORDE 2 plans ,tcFamACCES-LEGERECHELLES-PLATINIUM 500 AGRIP COULISSE CORDE 2 plans";
	$pf_exids[42] = "tcProdACCES-LEGERECHELLESPRONOR-COULISSE-CORDE-2-plans";
	$pf_exids[43] = "tcProdACCES-LEGERECHELLESCOULISSE-CORDE-2-plans-POMPIERS";
	$pf_exids[44] = "tcProdACCES-LEGERECHELLESPLATINIUM-300-COULISSE-CORDE-3-plans,tcProdACCES-LEGERECHELLESPLATINIUM-500-COULISSE-CORDE-3-plans";
	$pf_exids[45] = "tcFamACCES-LEGERECHELLES-PLATINIUM 300 AGRIP COULISSE CORDE 3 plans,tcFamACCES-LEGERECHELLES-PLATINIUM 500 AGRIP COULISSE CORDE 3 plans";
	$pf_exids[46] = "tcProdACCES-LEGERECHELLESPRONOR-COULISSE-CORDE-3-plans";
	$pf_exids[47] = "tcProdACCES-LEGERECHELLESCOULISSE-CORDE-3-plans-POMPIERS";
	$pf_exids[48] = "tcProdACCES-LEGERECHELLESCOULISSE-CORDE-2-plans-ISOLANTE";
	$pf_exids[49] = "##763C5C74-0BB2-4531-A78F-CB4307B0870B";
	$pf_exids[50] = "tcProdACCES-LEGERECHELLESCOULISSE-CORDE-3-plans-ISOLEX";
	$pf_exids[51] = "tcProdACCES-LEGERECHELLESPLATINIUM-COULISSE-MAIN-2-plans,tcProdACCES-LEGERECHELLESPLATINIUM-COULISSE-MAIN-2-plans-AVEC-CROCHETS";
	$pf_exids[52] = "tcFamACCES-LEGERECHELLES-PLATINIUM AGRIP COULISSE MAIN 2 plans";
	$pf_exids[53] = "tcProdACCES-LEGERECHELLESPRONOR-COULISSE-MAIN-2-plans-AVEC-CROCHETS";
	$pf_exids[54] = "tcProdACCES-LEGERECHELLESPRONOR-COULISSE-MAIN-2-plans-AVEC-CROCHETS";
	$pf_exids[55] = "tcProdACCES-LEGERECHELLESCOULISE-MAIN-2-plans-ISOLANTE";
	$pf_exids[56] = "tcFamACCES-LEGERECHELLES-PLATINIUM SIMPLE";
	$pf_exids[57] = "tcFamACCES-LEGERECHELLES-PLATINIUM SIMPLE EVASEE";
	$pf_exids[58] = "tcFamACCES-LEGERECHELLES-PRONOR SIMPLE";
	$pf_exids[59] = "tcProdACCES-LEGERECHELLESSIMPLE-FIXE-A-CROCHETS-POMPIERS";
	$pf_exids[60] = "tcProdACCES-LEGERECHELLESSIMPLE-PLIANTE-A-CROCHETS-POMPIERS";
	$pf_exids[61] = "tcProdACCES-LEGERECHELLESSIMPLE-MIXTE-ACIER/ALUMINIUM";
	$pf_exids[62] = "tcProdACCES-LEGERECHELLESSIMPLE-EVASEE-MIXTE-ACIER/ALUMINIUM";
	$pf_exids[63] = "tcFamACCES-LEGERECHELLES-SIMPLE ISOLANTE";
	$pf_exids[64] = "tcFamACCES-LEGERECHELLES-MULTISCOPIC";
	$pf_exids[65] = "tcProdACCES-LEGERECHELLESSIMPLE-DE-MAGASIN";
	$pf_exids[66] = "tcFamACCES-LEGERECHELLES-ECHELLE MEUNIERE ALU A MARCHES";
	$pf_exids[67] = "tcProdACCES-LEGERECHELLESDOUBLE-EVASEE-ALUMINIUM";
	$pf_exids[68] = "tcFamACCES-LEGERECHELLES-ECHELLE DOUBLE ISOLANTE";
	$pf_exids[69] = "tcFamACCES-LEGERECHELLES-COULISSE MAIN 2 plans POMPIER";
	$pf_exids[70] = "tcFamACCES-LEGERECHELLES-MULTIPLI";
	$pf_exids[71] = "tcProdACCES-LEGERECHELLESX-TENSO-SIMPLE";
	$pf_exids[72] = "tcFamACCES-LEGERECHELLES-ECHELLE PREMUR";
	$pf_exids[73] = "tcFamACCES-LEGERECHELLES-ECHELLE AUTOSTABLE TOUT ALU";
	$pf_exids[74] = "tcFamACCES-LEGERECHELLES-MODELE TOUT ALU";
	$pf_exids[75] = "##06B05A48-330C-4F3B-85C3-91777449DB54";
	$pf_exids[76] = "tcProdACCES-LEGERECHELLESECHELLE-D'APPUI-MIXTE-ALUMINIUM/-FIBRE-DE-VERRE";
	$pf_exids[77] = "tcFamACCES-LEGERMARCHEPIEDS-CONFORT FIXE,tcFamACCES-LEGERMARCHEPIEDS-CONFORT PLIANT,tcFamACCES-LEGERMARCHEPIEDS-CONFORT PLIANT EXTRA PLAT,tcFamACCES-LEGERMARCHEPIEDS-CONFORT PLIANT XL";
	$pf_exids[78] = "tcProdACCES-LEGERMARCHEPIEDSCOMBI'MODUL,tcProdACCES-LEGERMARCHEPIEDSMODUL-ALUMINIUM";
	$pf_exids[79] = "tcFamACCES-LEGERMARCHEPIEDS-PODIUM";
	$pf_exids[80] = "tcFamACCES-LEGERMARCHEPIEDS-MAGNUM 2";
	$pf_exids[81] = "tcFamACCES-LEGERMARCHEPIEDS-PROBAT";
	$pf_exids[82] = "tcFamACCES-LEGERMARCHEPIEDS-MAXIBAT";
	$pf_exids[83] = "tcFamACCES-LEGERMARCHEPIEDS-MP PRO,tcProdACCES-LEGERMARCHEPIEDSMP-PRO-AVEC-RAMPE";
	$pf_exids[84] = "tcProdACCES-LEGERMARCHEPIEDSSIMPLE-MARCHE";
	$pf_exids[85] = "tcFamACCES-LEGERMARCHEPIEDS-COMPACT ALU";
	$pf_exids[86] = "tcFamACCES-LEGERMARCHEPIEDS-COLORSTEP";
	$pf_exids[87] = "tcProdACCES-LEGERECHELLESDOUBLE-1-PLAN-DE-MONTEE-LYONNAISE";
	$pf_exids[88] = "tcProdACCES-LEGERMARCHEPIEDSMODUL-ACIER";
	$pf_exids[89] = "tcProdACCES-LEGERMARCHEPIEDSCOMPACT-FIBRE-DE-VERRE";
	$pf_exids[90] = "tcProdACCES-LEGERMARCHEPIEDSDOUBLE-MARCHE
	tcFamACCES-LEGERECHELLES-PLATINIUM ECHELLE DOUBLE DE CHANTIER";
	$pf_exids[91] = "tcProdACCES-LEGERECHELLESDOUBLE-A-MARCHES";
	$pf_exids[92] = "tcProdACCES-LEGERPLATES-FORMESPLATE-FORME-X'TREM-TEND";
	$pf_exids[93] = "tcProdACCES-LEGERPLATES-FORMESSHERPASCOPIC-TÉLESCOPIQUE
	tcProdACCES-LEGERPLATES-FORMESSHERPASCOPIC-GARDE-CORPS-REPLIABLE";
	$pf_exids[94] = "tcProdACCES-LEGERPLATES-FORMESPROFORT-X'TEND";
	$pf_exids[95] = "tcProdACCES-LEGERPLATES-FORMESSHERPAMATIC-FiXE";
	$pf_exids[96] = "tcProdACCES-LEGERPLATES-FORMESPROFORT-GARDE-CORPS-FiXE";
	$pf_exids[97] = "tcProdACCES-LEGERPLATES-FORMESPROFORT-GARDE-CORPS-repliable";
	$pf_exids[98] = "tcFamACCES-LEGERPLATES-FORMES-RAPTOR";
	$pf_exids[99] = "tcFamACCES-LEGERPLATES-FORMES-STABIL EVOLUTION,tcProdACCES-LEGERPLATES-FORMESPLATE-FORME-STABIL";
	$pf_exids[100] = "tcFamACCES-LEGERPLATES-FORMES-SHERPA FIBRE";
	$pf_exids[101] = "tcProdACCES-LEGERPLATES-FORMESESCALIER-DE-RAYONNAGE";
	$pf_exids[102] = "tcProdACCES-LEGERPLATES-FORMES1-PLAN-DE-MONTÉE-800-mm,tcProdACCES-LEGERPLATES-FORMES2-PLANS-DE-MONTÉE-1200-mm,tcProdACCES-LEGERPLATES-FORMES2-PLANS-DE-MONTÉE-800-mm";
	$pf_exids[103] = "tcProdACCES-LEGERECHAFAUDAGES-SPEEDY80";
	$pf_exids[104] = "tcProdACCES-LEGERECHAFAUDAGES-SPEEDY180";
	$pf_exids[105] = "tcFamACCES-LEGERPLATES-FORMES-MINIX 110";
	$pf_exids[106] = "tcFamACCES-LEGERPLATES-FORMES-PICOLO 140";
	$pf_exids[107] = "tcProdACCES-LEGERECHAFAUDAGES-SK10PLIANT";
	$pf_exids[108] = "tcProdACCES-LEGERECHAFAUDAGES-FIRST5";
	$pf_exids[109] = "tcProdACCES-LEGERECHAFAUDAGES-FIRST7-PRESTO";
	$pf_exids[110] = "tcFamACCES-LEGERECHELLES-STARLINE SIMPLE";
	$pf_exids[111] = "tcFamACCES-LEGERECHELLES-STARLINE COULISSE MAIN  2 plans";
	$pf_exids[112] = "tcFamACCES-LEGERECHELLES-STARLINE TRANSFORMABLE 2 plans";
	$pf_exids[113] = "tcFamACCES-LEGERECHELLES-STARLINE COULISSE CORDE 2 plans";
	$pf_exids[114] = "tcFamACCES-LEGERECHELLES-STARLINE TRANSFORMABLE 3 plans";
	$pf_exids[115] = "tcFamACCES-LEGERMARCHEPIEDS-ESCABEAU ALU PLUS";
	$pf_exids[116] = "tcFamACCES-LEGERMARCHEPIEDS-ESCABEAU ARTUCONFORT";
	$pf_exids[117] = "tcFamACCES-LEGERPLATES-FORMES-MINIX 70,tcFamACCES-LEGERPLATES-FORMES-PICOLO 70";
  $pf_ids = array();
	$f_ids = array();
	
  foreach($pf_exids as $answ_id => $pf_ext_id){
		if(strpos($pf_ext_id,',')!==false){
			$pf_ext_id_a = explode(',',$pf_ext_id);
			foreach($pf_ext_id_a as $ext_id){
				$pf_id = tubesca_core_import_entity_exist_ext(trim($ext_id),'product_family','fr');
				$f_tid = tubesca_core_get_family_by_extid(trim($ext_id),'fr');
				if($pf_id > 0){
					if(!empty($pf_ids[$answ_id])){
					  $pf_ids[$answ_id] .= ',';
						$f_ids[$answ_id] .= ',';
					}else{
						$pf_ids[$answ_id] .= '';
						$f_ids[$answ_id] .= '';
					}
					  $pf_ids[$answ_id] .= $pf_id;
						$f_ids[$answ_id] .= $f_tid;
				}else{
					//print $ext_id.'<br/>';
				}
			}
		}else{
			$pf_id = tubesca_core_import_entity_exist_ext(trim($pf_ext_id),'product_family','fr');
		  $f_tid = tubesca_core_get_family_by_extid(trim($pf_ext_id),'fr');
	    $pf_ids[$answ_id] = $pf_id;
			$f_ids[$answ_id] = $f_tid;
		}
  }
	
	$product_family_array = 'var product_family_array = {';
	$family_array = 'var family_array = {';
	
	foreach($pf_ids as $answ_id => $pf_id){
		if(strpos($pf_id,'-1')===false){
			if(((int)$answ_id>1)&&($product_family_array!='var product_family_array = {')&&($family_array !='var family_array = {')){
				$product_family_array .= ',';
				$family_array .= ',';
			}
			$product_family_array .= $answ_id.":'".$pf_id."'";
			if(strpos($f_ids[$answ_id],',')!==false){
				$f_tid = explode(',', $f_ids[$answ_id]);
				$family_array .= $answ_id.":'".$f_tid[0]."'";
			}else{
				$family_array .= $answ_id.":'".$f_ids[$answ_id]."'";
			}
		}
	}
	$product_family_array .= '};';
	$family_array .= '};';
	//preprintr($pf_ids);
	//preprintr($f_ids);



$fam_names=array("tcFamECHAFAUDAGES-FACADER-200-Progress","ECHAFAUDAGES-ROULANTS-ACIER","ECHAFAUDAGES-ROULANTS-ALUMINIUM","##3FBE5C5E-D7A3-4AE4-82B2-E85183723BFC","tcFamACCES-LEGERPLATES-FORMES", "##157B0BAA-5120-478C-9D8E-70B44299F25D","tcFamACCES-LEGERECHELLES","##25FF2E87-0ED4-4F40-9520-1827055436A9", "tcFamACCES-LEGERMARCHEPIEDS","##33B38F80-D762-485C-8467-D84C91252105","##5A5D0A7C-B8B4-4CE1-91E9-EEA478E940C7","tcFamECHAFAUDAGES-FIXESEchafaudage-Maçon-M-49","tcFamECHAFAUDAGES-FIXESEchafaudage-multidirectionnel",
"tcFamCRINOLINESCRINOLINES", "tcFamECHAFAUDAGES-FACADER-200-Progress-et-R-200-Record","tcFamECHAFAUDAGES-FACADER-GOYA48","tcFamECHAFAUDAGES-FACADER-MULTI","tcFamECHAFAUDAGES-FIXESÉchafaudage-Cover-200","tcFamECHAFAUDAGES-FIXESEchafaudage-de-facade-M-42--F42","tcFamECHAFAUDAGES-FIXESR-200-Record");

$fam_ids=array();
foreach($fam_names as $name){
	$result_tid=tubesca_core_get_pr_family_and_family_ext_id(trim($name));
    if($result_tid==-1){
		$result_tid=0;
	}

	array_push($fam_ids,$result_tid);
}

$q['q1'] = array('question' => $tc_translations['string-area-73'], 'filter' => 'DicoID_168',
    'answers' => array($tc_translations['string-area-74'] => array('img' => 'Q1_Intensif.png',
            'filter' => '<>"07"',
            'type' => 'question',
            'value' => 'q2a'),
        $tc_translations['string-area-75'] => array('img' => 'Q1_Occasionnel.png',
            'filter' => '=="07"',
            'type' => 'question',
            'value' => 'q2b'))
);

$q['q2a'] = array('question' => $tc_translations['string-area-76'], 'filter' => 'DicoID_409',
    'answers' => array($tc_translations['string-area-77'] => array('filter' => '<>###', 'img' => 'Q2_Q4_Q5_Q6_yes.png', 'type' => 'question', 'value' => 'q3a'), $tc_translations['string-area-78'] => array('filter' => '==###', 'img' => 'Q2_Q4_Q5_Q6_no.png', 'type' => 'question', 'value' => 'q3b'))
);

$q['q2b'] = array('question' => $tc_translations['string-area-79'], 'filter' => 'DicoID_409',
    'answers' => array($tc_translations['string-area-77'] => array('filter' => '<>###', 'img' => 'Q2_Q4_Q5_Q6_yes.png', 'type' => 'question', 'value' => 'q3c'), $tc_translations['string-area-78'] => array('filter' => '==###', 'img' => 'Q2_Q4_Q5_Q6_no.png', 'type' => 'question', 'value' => 'q3d'))
); 

$q['q3a'] = array('question' => $tc_translations['string-area-80'], 'filter' => 'FExtID',
    'answers' => array($tc_translations['string-area-81'] => array('filter' => 'IN  tcFamECHAFAUDAGES-FACADER-200-Progress,tcFamECHAFAUDAGES-FIXESEchafaudage-multidirectionnel,tcFamECHAFAUDAGES-FIXESEchafaudage-Maçon-M-49,tcFamECHAFAUDAGES-FACADER-200-Progress-et-R-200-Record,tcFamECHAFAUDAGES-FACADER-GOYA48,tcFamECHAFAUDAGES-FACADER-MULTI,tcFamECHAFAUDAGES-FIXESÉchafaudage-Cover-200,tcFamECHAFAUDAGES-FIXESEchafaudage-de-facade-M-42--F42,tcFamECHAFAUDAGES-FIXESR-200-Record', 'img' => 'Q3_EchafaudageFixe.png', 'type' => 'question', 'value' => 'q4a'),
        $tc_translations['string-area-82'] => array(
            'filter' => 'IN ECHAFAUDAGES-ROULANTS-ACIER, ECHAFAUDAGES-ROULANTS-ALUMINIUM', 'img' => 'Q3_EchafaudageRoulant.png', 'type' => 'question', 'value' => 'q4b'),
        $tc_translations['string-area-83'] => array(
            'filter' => 'IN tcFamACCES-LEGERPLATES-FORMES', 'img' => 'Q3_Q4_PlateForme.png', 'type' => 'question', 'value' => 'q4c'),
        ));


$q['q3b'] = array('question' => $tc_translations['string-area-84'], 'filter' => 'DicoID_413',
    'answers' => array($tc_translations['string-area-85'] => array('filter' => '=="'.trim($tc_translations['string-area-432']).'"', 'img' => 'Q3_Toits.png', 'type' => 'question', 'value' => 'q4d'),
        $tc_translations['string-area-86'] => array('filter' => '=="'.trim($tc_translations['string-area-433']).'"', 'img' => 'Q3_Toits.png', 'img' => 'Q3_LaverVitres.png', 'type' => 'question', 'value' => 'q4e'),
        $tc_translations['string-area-87'] => array('filter' => '=="'.trim($tc_translations['string-area-434']).'"', 'img' => 'Q3_EchelleCrinoline.png', 'type' => 'question', 'value' => 'q4f'),
        $tc_translations['string-area-88'] => array(
            'filter' => '=="'.$tc_translations['string-area-435'].'"', 'img' => 'Q3_descendre.png', 'type' => 'question', 'value' => 'q4g'),
        $tc_translations['string-area-89'] => array('filter' => '==">5m"', 'img' => 'Q3_sup5m.png', 'type' => 'question', 'value' => 'q4h'),
        $tc_translations['string-area-90'] => array('filter' => '=="<5m"', 'img' => 'Q3_inf5m.png', 'type' => 'question', 'value' => 'q4i'),
        ));

$q['q3c'] = array('question' => $tc_translations['string-area-91'], 'filter' => 'FExtID',
    'answers' => array($tc_translations['string-area-82'] => array(
            'filter' => 'IN ECHAFAUDAGES-ROULANTS-ACIER , ECHAFAUDAGES-ROULANTS-ALUMINIUM', 'img' => 'Q3_EchafaudageRoulant.png', 'type' => 'question', 'value' => 'q4j'),
        ));


$q['q3d'] = array('question' => $tc_translations['string-area-91'], 'filter' => 'FExtID',
    'answers' => array($tc_translations['string-area-93'] => array(
            'filter' => 'IN tcFamACCES-LEGERECHELLES', 'img' => 'Q3_Q4_Echelle.png', 'type' => 'question', 'value' => 'q4k'),
        $tc_translations['string-area-94'] => array('filter' => 'IN tcFamACCES-LEGERMARCHEPIEDS', 'img' => 'Q3_Q4_Marchepied.png', 'type' => 'question', 'value' => 'q7f'),
        $tc_translations['string-area-95'] => array('filter' => 'IN tcFamACCES-LEGERPLATES-FORMES', 'img' => 'Q3_Q4_PlateForme.png', 'type' => 'answer', 'value' => '117')
        ));



$q['q4a'] = array('question' => $tc_translations['string-area-92'], 'filter' => 'DicoID_413',
    'answers' => array($tc_translations['string-area-96'] => array('filter' => '=="'.trim($tc_translations['string-area-436']).'"', 'img' => 'Q4_Maconnerie.png',
            'type' => 'answer',
            'value' => '1'),
        $tc_translations['string-area-97'] => array('filter' => '=="'.trim($tc_translations['string-area-437']).'"', 'img' => 'Q4_FinitionsCouverture.png',
            'type' => 'answer',
            'value' => '2'),
        $tc_translations['string-area-98'] => array('filter' => '=="'.trim($tc_translations['string-area-438']).'"', 'img' => 'Q4_GrosOeuvre.png',
            'type' => 'answer',
            'value' => '3'))
);

$q['q4b'] = array('question' => $tc_translations['string-area-99'], 'filter' => 'DicoID_184',
    'answers' => array('<4' => array('filter' => '<4', 'img' => 'Q4_inf4m.png',
            'type' => 'question',
            'value' => 'q5a'),
        '<8' => array('filter' => ' BETWEEN 4 AND 8', 'img' => 'Q4_inf8m.png',
            'type' => 'question',
            'value' => 'q5b'),
        '>8' => array('filter' => '>8', 'img' => 'Q4_sup8m.png',
            'type' => 'question',
            'value' => 'q5c'))
);

$q['q4c'] = array('question' => $tc_translations['string-area-100'], 'filter' => 'DicoID_411',
    'answers' => array(
        $tc_translations['string-area-77'] => array('filter' => '<>###', 'img' => 'Q2_Q4_Q5_Q6_yes.png', 'type' => 'question', 'value' => 'q5d'),
        $tc_translations['string-area-78'] => array('filter' => '==###', 'img' => 'Q2_Q4_Q5_Q6_no.png', 'type' => 'question', 'value' => 'q5e'))
);

$q['q4d'] = array('question' => $tc_translations['string-area-80'], 'filter' => 'FExtID',
    'answers' => array(
        $tc_translations['string-area-93'] => array('filter' => 'IN tcFamACCES-LEGERECHELLES,tcFamCRINOLINESCRINOLINES', 'img' => 'Q3_Q4_Echelle.png', 'type' => 'question', 'value' => 'q5f'),)
);

$q['q4e'] = array('question' => $tc_translations['string-area-80'], 'filter' => 'FExtID',
    'answers' => array($tc_translations['string-area-93'] => array(
            'filter' => 'IN tcFamACCES-LEGERECHELLES,tcFamCRINOLINESCRINOLINES', 'img' => 'Q3_Q4_Echelle.png', 'type' => 'answer', 'value' => '30'),)
);

$q['q4f'] = array('question' => $tc_translations['string-area-80'], 'filter' => 'FExtID',
    'answers' => array($tc_translations['string-area-93'] => array('filter' => 'IN tcFamACCES-LEGERECHELLES,tcFamCRINOLINESCRINOLINES', 'img' => 'Q3_Q4_Echelle.png', 'type' => 'answer', 'value' => '31'),)
);

$q['q4g'] = array('question' => $tc_translations['string-area-80'], 'filter' => 'FExtID',
    'answers' => array($tc_translations['string-area-93'] => array('filter' => 'IN tcFamACCES-LEGERECHELLES,tcFamCRINOLINESCRINOLINES', 'img' => 'Q3_Q4_Echelle.png', 'type' => 'answer', 'value' => '32'),)
);

$q['q4h'] = array('question' => $tc_translations['string-area-80'], 'filter' => 'FExtID',
    'answers' => array($tc_translations['string-area-93'] => array('filter' => 'IN tcFamACCES-LEGERECHELLES,tcFamCRINOLINESCRINOLINES', 'img' => 'Q3_Q4_Echelle.png', 'type' => 'question', 'value' => 'q5g'),)
);


$q['q4i'] = array('question' => $tc_translations['string-area-80'], 'filter' => 'FExtID',
    'answers' => array($tc_translations['string-area-93'] => array('filter' => 'IN tcFamACCES-LEGERECHELLES,tcFamCRINOLINESCRINOLINES', 'img' => 'Q3_Q4_Echelle.png', 'type' => 'question', 'value' => 'q5h'),
        $tc_translations['string-area-94'] => array('filter' => 'IN tcFamACCES-LEGERMARCHEPIEDS', 'img' => 'Q3_Q4_Marchepied.png', 'type' => 'question', 'value' => 'q5i'),
        $tc_translations['string-area-83'] => array('filter' => 'IN tcFamACCES-LEGERPLATES-FORMES', 'img' => 'Q3_Q4_PlateForme.png', 'type' => 'question', 'value' => 'q5j'),)
);

$q['q4j'] = array('question' => $tc_translations['string-area-99'], 'filter' => 'DicoID_184',
    'answers' => array(
        '<4' => array('filter' => '<=4', 'img' => 'Q4_inf4m.png', 'type' => 'answer', 'value' => '103,104,105,106,107,108'),
        '<7' => array('filter' => 'BETWEEN 4 AND 7', 'img' => 'Q4_inf7m.png', 'type' => 'answer', 'value' => '109'),
        ));

$q['q4k'] = array('question' => $tc_translations['string-area-99'], 'filter' => 'DicoID_184',
    'answers' => array(
        '<7' => array('filter' => '<=7', 'img' => 'Q4_inf7m.png', 'type' => 'question', 'value' => 'q5k'),
        '>9' => array('filter' => '>9', 'img' => 'Q4_sup9m.png', 'type' => 'question', 'value' => 'q5l'),
        ));



$q['q5a'] = array('question' => $tc_translations['string-area-102'], 'filter' => 'DicoID_325',
    'answers' => array(
        $tc_translations['string-area-103'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-440']).'',
            'img' => 'Q5_Q6_Acier.png',
            'type' => 'answer',
            'value' => '4,5,6'),
        $tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '7'))
);

$q['q5b'] = array('question' => $tc_translations['string-area-102'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-103'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-440']).' ',
            'img' => 'Q5_Q6_Acier.png',
            'type' => 'answer',
            'value' => '8,9'),
        $tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '10,11,12'))
);
$q['q5c'] = array('question' => $tc_translations['string-area-102'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-103'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-440']).'',
            'img' => 'Q5_Q6_Acier.png',
            'type' => 'answer',
            'value' => '13,14,15,16'),
        $tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '17,18,19,20,21'))
);

$q['q5d'] = array('question' => $tc_translations['string-area-400'], 'filter' => 'DicoID_325',
    'answers' => array(
        /*
         * $tc_translations['string-area-103'] => array(
          'filter' => 'IN Acier,Acier epoxy, Acier époxy, Aluminium/Acier',
          'img' => 'Q5_Q6_Acier.png',
          'type' => 'answer',
          'value' => '23,24'),

         *          */
        $tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '22'))
);

$q['q5e'] = array('question' => $tc_translations['string-area-400'], 'filter' => 'DicoID_325',
    'answers' => array(
        $tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '25,26,27'))
);


$q['q5f'] = array('question' => $tc_translations['string-area-401'], 'filter' => 'DicoID_325',
    'answers' => array(
        $tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '28'),
        $tc_translations['string-area-105'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-442']).'',
            'img' => 'Q5_Q6_Bois.png',
            'type' => 'answer',
            'value' => '29'))
);

$q['q5g'] = array('question' => $tc_translations['string-area-106'], 'filter' => 'DicoID_384',
    'answers' => array(
        $tc_translations['string-area-107'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-452']).'"',
            'img' => 'Q5_Transformable.png',
            'type' => 'question',
            'value' => 'q6a'),
        $tc_translations['string-area-108'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-443']).'"',
            'img' => 'Q5_CoulisseACorde.png',
            'type' => 'question',
            'value' => 'q6b'),
        $tc_translations['string-area-109'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-444']).'"',
            'img' => 'Q5_CoulisseAMain.png',
            'type' => 'question',
            'value' => 'q6c'),
        $tc_translations['string-area-110'] => array(
            'filter' => '=="'.$tc_translations['string-area-445'].'"',
            'img' => 'Q5_Simple.png',
            'type' => 'question',
            'value' => 'q6d'),
        $tc_translations['string-area-111'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-446']).'"',
            'img' => 'Q5_Articulee.png',
            'type' => 'question',
            'value' => 'q6e'),)
);

$q['q5h'] = array('question' => $tc_translations['string-area-106'], 'filter' => 'DicoID_384',
    'answers' => array($tc_translations['string-area-110'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-445']).'"',
            'img' => 'Q5_Simple.png',
            'type' => 'question',
            'value' => 'q6f'),
        $tc_translations['string-area-112'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-447']).'"',
            'img' => 'Q5_Double.png',
            'type' => 'question',
            'value' => 'q6g'),
        $tc_translations['string-area-109'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-444']).'"',
            'img' => 'Q5_CoulisseAMain.png',
            'type' => 'question',
            'value' => 'q6h'),
        $tc_translations['string-area-111'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-446']).'"',
            'img' => 'Q5_Articulee.png',
            'type' => 'question',
            'value' => 'q6i'),
        $tc_translations['string-area-113'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-448']).'"',
            'img' => 'Q5_Telescopique.png',
            'type' => 'question',
            'value' => 'q6j'),
        $tc_translations['string-area-114'] => array(
		     'filter' => '=="'.trim($tc_translations['string-area-449']).'"',
            'img' =>
            'Q5_Securise.png',
            'type' => 'question',
            'value' => 'q6k'),)
);

$q['q5i'] = array('question' => $tc_translations['string-area-115'], 'filter' => 'DicoID_416',
    'answers' => array('1 plan' => array(
            'filter' => '==1',
            'img' => 'Q5_1p.png',
            'type' => 'question',
            'value' => 'q6l'),
        '2 plans' => array(
            'filter' => '==2',
            'img' => 'Q5_Q7_2p.png',
            'type' => 'question',
            'value' => 'q6m'),)
);

$q['q5j'] = array('question' => $tc_translations['string-area-118'], 'filter' => 'DicoID_384',
    'answers' => array($tc_translations['string-area-77'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-450']).'"',
            'img' => 'Q2_Q4_Q5_Q6_yes.png', 'type' => 'answer', 'value' => '92,93,94'),
        $tc_translations['string-area-78'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-451']).'"',
            'img' => 'Q2_Q4_Q5_Q6_no.png', 'type' => 'question', 'value' => 'q6n'))
);

$q['q5k'] = array('question' => $tc_translations['string-area-106'], 'filter' => 'DicoID_384',
    'answers' => array($tc_translations['string-area-110'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-445']).'"',
            'img' => 'Q5_Simple.png',
            'type' => 'answer',
            'value' => '110'),
        $tc_translations['string-area-109'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-444']).'"',
            'img' => 'Q5_CoulisseAMain.png',
            'type' => 'answer',
            'value' => '111'),
        $tc_translations['string-area-107'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-452']).'"',
            'img' => 'Q5_Transformable.png',
            'type' => 'answer',
            'value' => '112'),)
);

$q['q5l'] = array('question' => $tc_translations['string-area-106'], 'filter' => 'DicoID_384',
    'answers' => array($tc_translations['string-area-108'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-443']).'"',
            'img' => 'Q5_CoulisseACorde.png',
            'type' => 'answer',
            'value' => '113'),
        $tc_translations['string-area-107'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-452']).'"',
            'img' => 'Q5_Transformable.png',
            'type' => 'answer',
            'value' => '114'),)
);
$q['q6a'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array(
        $tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'question',
            'value' => 'q7a'),
        $tc_translations['string-area-121'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-453']).'',
            'img' => 'Q5_Q6_Q7_Fibre.png',
            'type' => 'question',
            'value' => 'q7a'),)
);
$q['q6b'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'question',
            'value' => 'q7b'),
        $tc_translations['string-area-121'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-453']).'',
            'img' => 'Q5_Q6_Q7_Fibre.png',
            'type' => 'question',
            'value' => 'q7c'),)
);

$q['q6c'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '51,52,53,54'),
        $tc_translations['string-area-121'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-453']).'',
            'img' => 'Q5_Q6_Q7_Fibre.png',
            'type' => 'answer',
            'value' => '55'),)
);
$q['q6d'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array(
        $tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '56,57,58,59,60'),
        $tc_translations['string-area-103'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-440']).'',
            'img' => 'Q5_Q6_Acier.png',
            'type' => 'answer',
            'value' => '61,62'),
        $tc_translations['string-area-121'] => array(
            'filter' => 'IN '.$tc_translations['string-area-453'].'',
            'img' => 'Q5_Q6_Q7_Fibre.png',
            'type' => 'answer',
            'value' => '63'),)
);



$q['q6e'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '64'),)
);
$q['q6f'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '65,66'),) //last
);
$q['q6g'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '67'),
        $tc_translations['string-area-121'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-453']).'',
            'img' => 'Q5_Q6_Q7_Fibre.png',
            'type' => 'answer',
            'value' => '68'),)
);
$q['q6h'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '69'),)
);
$q['q6i'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '70'),)
);
$q['q6j'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '71'),)
);



$q['q6k'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '72,73,74,75'),
        $tc_translations['string-area-121'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-453']).'',
            'img' => 'Q5_Q6_Q7_Fibre.png',
            'type' => 'answer',
            'value' => '76'),)
);
$q['q6l'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'question',
            'value' => 'q7d'),
        $tc_translations['string-area-105'] => array(
             'filter' => 'IN '.trim($tc_translations['string-area-442']).'',
            'img' => 'Q5_Q6_Bois.png',
            'type' => 'answer',
            'value' => '87'),
        'Acier' => array(
            'filter' => 'IN '.trim($tc_translations['string-area-440']).'',
            'img' => 'Q5_Q6_Acier.png',
            'type' => 'answer',
            'value' => '88'),)
);
$q['q6m'] = array('question' => $tc_translations['string-area-120'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '90'),
        $tc_translations['string-area-105'] => array(
             'filter' => 'IN '.trim($tc_translations['string-area-442']).'',
            'img' => 'Q5_Q6_Bois.png',
            'type' => 'answer',
            'value' => '91'),)
);

$q['q6n'] = array('question' => $tc_translations['string-area-122'], 'filter' => 'DicoID_411',
    'answers' => array(
        $tc_translations['string-area-77'] => array(
            'filter' => '<>###',
            'img' => 'Q2_Q4_Q5_Q6_yes.png', 'type' => 'question', 'value' => 'q7e'),
        $tc_translations['string-area-78'] => array(
            'filter' => '==###',
            'img' => 'Q2_Q4_Q5_Q6_no.png', 'type' => 'answer', 'value' => '101,102'))
);

//here
$q['q7a'] = array('question' => $tc_translations['string-area-123'], 'filter' => 'DicoID_416',
    'answers' => array('2P' => array(
            'filter' => '==2',
            'img' => 'Q5_Q7_2p.png',
            'type' => 'answer',
            'value' => '33,34,35,36'),
        '3P' => array('img' => 'Q7_3p.png',
            'filter' => '==3',
            'type' => 'answer',
            'value' => '37,38,39'),)
);
$q['q7b'] = array('question' => $tc_translations['string-area-123'], 'filter' => 'DicoID_416',
    'answers' => array('2P' => array(
            'filter' => '==2',
            'img' => 'Q5_Q7_2p.png',
            'type' => 'answer',
            'value' => '40,41,42,43'),
        '3P' => array('img' => 'Q7_3p.png',
            'filter' => '==3',
            'type' => 'answer',
            'value' => '44,45,46,47'),)
);
$q['q7c'] = array('question' => $tc_translations['string-area-123'], 'filter' => 'DicoID_416',
    'answers' => array('2P' => array(
            'filter' => '==2',
            'img' => 'Q7_3p.png',
            'type' => 'answer',
            'value' => '48,49'),
        '3P' => array(
            'filter' => '==3',
            'img' => 'Q7_3p.png',
            'type' => 'answer',
            'value' => '50'),) //last
);
$q['q7d'] = array('question' => $tc_translations['string-area-124'], 'filter' => 'DicoID_410',
    'answers' => array($tc_translations['string-area-125'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-455']).'"',
            'img' => 'Q7_XL.png',
            'type' => 'answer',
            'value' => '77,78,79'),
        $tc_translations['string-area-126'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-454']).'"',
            'img' => 'Q7_L.png',
            'type' => 'answer',
            'value' => '80,81,82,83,84,85,86'),)
);
$q['q7e'] = array('question' => $tc_translations['string-area-127'], 'filter' => 'DicoID_325',
    'answers' => array($tc_translations['string-area-104'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-441']).'',
            'img' => 'Q5_Q6_Q7_Alu.png',
            'type' => 'answer',
            'value' => '95,96,97,98,99'),
        $tc_translations['string-area-121'] => array(
            'filter' => 'IN '.trim($tc_translations['string-area-453']).'',
            'img' => 'Q5_Q6_Q7_Fibre.png',
            'type' => 'answer',
            'value' => '100'),)
);
$q['q7f'] = array('question' => $tc_translations['string-area-124'], 'filter' => 'DicoID_410',
    'answers' => array($tc_translations['string-area-126'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-454']).'"',
            'img' => 'Q7_L.png',
            'type' => 'answer',
            'value' => '115'),
        $tc_translations['string-area-125'] => array(
            'filter' => '=="'.trim($tc_translations['string-area-455']).'"',
            'img' => 'Q7_XL.png',
            'type' => 'answer',
            'value' => '116'),)
);

?>
<form target="_self" method="post" name="config" id="chouse">
		<input id="nids_input" type="hidden" name="nids" value="">
        <input id="filter_input" type="hidden" name="filters" value="">
</form>	
<script>

jQuery(document).ready(function ($) {
	
	<?php print $family_array; ?>
	
	<?php print $product_family_array; ?>

	var lngp = '<?php print $language->prefix; ?>';
	var arrayFromPHP = <?php echo json_encode($q); ?>;
	var steps =[];
	var the_answers =[];
	function create_q(question){
		html = '<div class="clearfix">';
		$.each( arrayFromPHP[question]['answers'], function( index, data ) {
				
				html = html + '<div class="answers"><label for="'+question+'_'+index+'"><img src="<?php print $tubesca_nav_vars['path_prefix']; ?>/sites/all/themes/tubesca_comabi/images/'+data['img']+'" /><span>'+index+'</span><input id="'+question+'_'+index+'" type="radio" name="'+question+'" value="'+data['value']+'" data-type="'+data['type']+'" ><div class="icon" data-question="q'+(question.charAt(1))+'" data-from="'+question+'"><i class="fa fa-times" aria-hidden="true" ></i></div></label></div>'
		});
		html = html + '</div>';
		steps.push(question.substr(0, 2));
		
		$('#matrix').append('<div id="'+question.substr(0, 2)+'" class="question clearfix"><div class="number">'+question.charAt(1)+'.</div><div class="number2"></div><p class="step">étape '+question.substr(1, 1)+' : </p><h2>'+arrayFromPHP[question]['question']+'</h2>'+html+'</div>');
		$('input:radio[name="'+question+'"]').on('change', function(event){
				var name = $(this).attr('name').substr(0, 2);
				var remove_flag=0;				
				$('#'+name).addClass('checked');
				$('#'+name+' label').removeClass('checked_chouse');
				$(this).parent('label').addClass('checked_chouse');
				$.each( steps, function( index, data ) {
					if (remove_flag == 1){

						$( "#"+data ).remove();
						delete steps[index];
						delete  the_answers[index];
					}
					
					if (data == name){
						remove_flag = 1;
					}
				});
				

				var answer_query = '';
                var answer_code=$(this).val();
				
				
				var myStringArray = ["tcFamECHAFAUDAGES-FACADER-200-Progress","ECHAFAUDAGES-ROULANTS-ACIER","ECHAFAUDAGES-ROULANTS-ALUMINIUM","##3FBE5C5E-D7A3-4AE4-82B2-E85183723BFC","tcFamACCES-LEGERPLATES-FORMES", "##157B0BAA-5120-478C-9D8E-70B44299F25D","tcFamACCES-LEGERECHELLES","##25FF2E87-0ED4-4F40-9520-1827055436A9", "tcFamACCES-LEGERMARCHEPIEDS","##33B38F80-D762-485C-8467-D84C91252105","##5A5D0A7C-B8B4-4CE1-91E9-EEA478E940C7","tcFamECHAFAUDAGES-FIXESEchafaudage-Maçon-M-49","tcFamECHAFAUDAGES-FIXESEchafaudage-multidirectionnel",
"tcFamCRINOLINESCRINOLINES", "tcFamECHAFAUDAGES-FACADER-200-Progress-et-R-200-Record","tcFamECHAFAUDAGES-FACADER-GOYA48","tcFamECHAFAUDAGES-FACADER-MULTI","tcFamECHAFAUDAGES-FIXESÉchafaudage-Cover-200","tcFamECHAFAUDAGES-FIXESEchafaudage-de-facade-M-42--F42","tcFamECHAFAUDAGES-FIXESR-200-Record"];  

 
var myStringArray2 = <?php echo json_encode($fam_ids); ?>;
var arrayLength = myStringArray.length;

				$.each( arrayFromPHP[$(this).attr('name')]['answers'], function( index, data ) {

					if(data['value']==answer_code){
						
						for (var i = 0; i < arrayLength; i++) {
							console.log(data['filter'] + "  - " +myStringArray[i]);
							console.log(data['filter'].indexOf(myStringArray[i]));
                                if( data['filter'].indexOf(myStringArray[i])!=-1){
									
									data['filter'] = data['filter'].replace(myStringArray[i], myStringArray2[i]);
									
									//data['filter'] = data['filter'].replace("S", "");
									//data['filter'] = data['filter'].replace("tcFamCRINOLINECRINOLINE", "");
										  
								 }
                              }	
								
						answer_query = data['filter'];					
					}
					
				});
				
				var name_number = $(this).attr('name').substr(1, 1); 
				the_answers[Number(name_number)]=arrayFromPHP[$(this).attr('name')]['filter']+'--'+answer_query; 
							
			
			    for (var i = Number(name_number)+1; i < 8; i++) {	
			
	               the_answers.splice(i, 1);
  
			    }
                console.log(the_answers);
					
				
				if ($(this).data('type') == 'question'){
					$('.matrix_submit').remove();
					create_q(this.value);
				} else {
					var answer = $(this).val();
				
					var nids='';
					answer = answer.split(',');
					uri = 'taxonomy/term/'+family_array[answer[0]];
			

					$.each( answer, function( index, data ) {
					nids =product_family_array[data]  +','+ nids;
					
					}); 
					
					nids = nids.slice(0, -1);
					$('#nids_input').val(nids);
					
					
					var str = '';
					var counter = 0;
					
					
					for (var element in the_answers) {
					
						if (counter == 0) {
							str += the_answers[element];
						} else {
							str += ";" + the_answers[element];
						}
					
						counter++;
					}
					
					    
					$('#filter_input').val(str); 
					
					var result;
					$.ajax({
						async: false,
						type: 'POST',
						url: '<?php print 'http'.((isset($_SERVER['HTTPS']))?'s':'').'://'.$_SERVER['SERVER_NAME'].$languag->language.'/tubesca/response'; ?>', // Servlet URL  
					    dataType: "json", 
						//ataType: "text", 
						data: {
							str
						},
						success: function(data) {
					
							result = data;
					
						},
						error: function(xhr, type) {
							alert('server error occurred');
						}
					});
					
					console.log(result);    
					var index = 0;
					var tids;
					var nids_new = '';
					
					for (var i in result) {
						if (index == 0) {
							tids = result[i].tid;
						}
					
						if (index == result.length - 1) {
							nids_new += result[i].nid;
						} else {
							nids_new += result[i].nid + ",";
						}
					
						index++;
					
					}
					
					$('#nids_input').val(nids_new);

					//$("#chouse").prop("action", '/'+ lngp +'/taxonomy/term/'+family_array[answer[0]]);
					$("#chouse").prop("action", '/'+ lngp +'/taxonomy/term/'+ tids); 
					//console.log($( ".matrix_submit" ).length);
					if ($( ".matrix_submit" ).length == 0)
					{
						$('#'+question.substr(0, 2)).append("<div class='matrix_submit'><button class='btn matrix_button_submit'>Voir les résultats</button></div>");
						$('.matrix_button_submit').on('click', function(){
							$("#chouse").submit();
						});
					} 
					
					

				}
				
		});
		steps = steps.filter(function(n){ return n != undefined }); 
			
	}
	create_q('q1');

	$(document.body).on('click', '.icon' ,function(event){
		var remove_flag=0;
		var name = $(this).data('question');
		$.each( steps, function( index, data ) {
					if (remove_flag == 1){
						$( "#"+data ).remove();
						delete steps[index];
						delete  the_answers[index];
						
					}
					if (data == name){
						remove_flag = 1;
						$( "#"+data ).remove();
						delete steps[index];
						delete  the_answers[index];
						$( "#"+data ).removeClass('checked');
						$('#'+data+' label').removeClass('checked_chouse');
					}
		});
		steps = steps.filter(function(n){ return n != undefined }); 
		create_q($(this).data('from'));
		return false;
	});

});
</script>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    
	<div id="matrix" class="container">
		<div class="martix_info clearfix">
			<div class="choose-bulb"><img src="<?php print $tubesca_nav_vars['path_prefix']; ?>/sites/all/themes/tubesca_comabi/images/light.png" /></div>
			<?php print render($content['field_description']); ?>
		</div>
	</div>
  </div>
</div>