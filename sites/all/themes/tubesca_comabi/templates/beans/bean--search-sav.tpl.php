<?php 
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<?php global $tc_translations; ?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <div class="field field-name-field-image field-type-image field-label-hidden"><?php print $bean_icon; ?></div>
    <div class="field-item even">
      <p><?php print $bean_desc; ?></p>
    </div>
    <form id="search-sav-form" >
      <input class="search-field"  name="search_sav_text" placeholder="<?php print $tc_translations['string-area-131']; ?>" type="text" />
      <br/>
      <button class="search-button" type="button" name="search" />
      <?php print $tc_translations['string-area-131']; ?>
      </button>
    </form>
  </div>
</div>