<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
 global $language,$tubesca_nav_vars,$tc_translations;
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> <?php print $bgd_color; ?> >
  <div class="content"<?php print $content_attributes; ?>>
  <div class="search-page-filters"><ul><li <?php print (arg(1)=='product')?'class="active"':''; ?> ><a href = "/<?php print $language->language; ?>/<?php print $tubesca_nav_vars['search_page'] ?>/product/<?php print arg(2); ?>"><span><?php print $tc_translations['string-area-405']; ?></span><img src="/sites/all/themes/tubesca_comabi/images/tab1_bgd.png" /></a></li><li <?php print (arg(1)=='info')?'class="active"':''; ?> ><a href = "/<?php print $language->language; ?>/<?php print $tubesca_nav_vars['search_page'] ?>/info/<?php print arg(2); ?>"><span><?php print $tc_translations['string-area-404']; ?></span><img src="/sites/all/themes/tubesca_comabi/images/tab2_bgd.png" /></a></li><li <?php print (arg(1)=='sav')?'class="active"':''; ?> ><a href = "/<?php print $language->language; ?>/<?php print $tubesca_nav_vars['search_page'] ?>/sav/<?php print arg(2); ?>"><span><?php print $tc_translations['string-area-406']; ?></span><img src="/sites/all/themes/tubesca_comabi/images/tab3_bgd.png" /></a></li></ul><div class="clearfix"> </div></div><br/>
  <?php
		if(arg(0)=='search-page'){
		  switch(arg(1)){
				case 'all' : 
				  $viewn = 'search_full_index';
					$disp = 'block_5';
				break;
				
				case 'info':
					$viewn = 'search_full_index';
					$disp = 'block_2';
				break;
				
				case 'product':
				  $viewn = 'search_full_index';
					$disp = 'block_4';
				break;
				
				case 'sav':
					$viewn = 'sav';
					$disp = 'block';
				break;
				
				default:
				  $viewn = 'search_full_index';
					$disp = 'block_5';
				break;
			}
			// load the view 
	    $view = views_get_view($viewn);
			
	    // set active display on the view	
			$view->set_display($disp);
			$view->set_arguments(array(arg(2)));
			$view->pre_execute();
      $view->execute();
	    // display the results
			if(($disp == 'block_4') || ($disp == 'block_5')){
			  foreach($view->result as $aresult){
				  //preprintr($aresult);
				  $ntitle = tubesca_core_title_nid(str_replace('node/','',$aresult->entity));
				  if(strtolower($ntitle)==strtolower(arg(2))){
			      $aresult->_entity_properties['search_api_relevance'] += 1000;
				  }
			  }
			}
      usort($view->result,'tubesca_core_sort_relevance');
	    print $view->render();
		}
  ?>
  </div>
</div>
