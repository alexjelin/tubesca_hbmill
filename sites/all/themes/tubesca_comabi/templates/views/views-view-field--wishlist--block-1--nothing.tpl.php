<?php
	global $user;
  global $tc_translations, $tubesca_nav_vars, $language;
	$count = 0;
	$flag =  flag_get_user_flags('commerce_product');

foreach ($flag['wishlist'] as $key => $value){
	$flagging = entity_load_single('flagging', $value->flagging_id);
	$count += $flagging->field_qty['und'][0]['value'];
}
?>

	<a href="/<?php print $language->language.'/'.$tubesca_nav_vars['my_list'];?>"><b><?php print $tc_translations['string-area-45'];?> </b><br><b><?php print $count; ?></b> <?php print $tc_translations['string-area-48'];?></a>