<?php 
  if(strpos($output,',')!==false){
    global $language, $tubesca_nav_vars;
    $tags = explode(',', $output);
	$i = 0;
	foreach($tags as $tid){
		if($i==0) print '<span class="tags_wrapper">';
		$atag = taxonomy_term_load($tid);
		if($i>0){
			print ', ';
		}
		print '<a href="/'.$language->language.'/'.$tubesca_nav_vars['news'].'/'.$atag->tid.'" >'.$atag->name.'</a>';
		$i++;
	}
	if($i>0) print '</span>';
  }
?>