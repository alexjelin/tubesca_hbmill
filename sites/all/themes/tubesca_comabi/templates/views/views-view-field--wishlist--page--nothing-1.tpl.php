<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */ 
        global $tc_translations;
	$data = explode(':::',$output);
	$node = commerce_product_load($data[0]);
?>
<div id="demo-<?php print $data[0]; ?>" class="collapse">
<h3><?php print $tc_translations['string-area-358'] ; ?> <?php print $data[1]; ?> :</h3>
<a data-toggle="collapse" data-target="#demo-<?php print $data[0]; ?>" class="close_whislist_info" ><i class="fa fa-times" aria-hidden="true"></i></a>
<?php 
	

	$html = "";
	$count= 1;
	$min_ones = 0;
	foreach($node->field_component_id['und'] as $key =>  $value){
		$comp  = commerce_product_load($value['target_id']);
		$min_ones++;
		if ($key % 2 == 0){
			$html .= '<div class="row maliste_row">
						<div class="col col-xs-3 maliste_comapre_title odd">
							<div class="inner_wrapper">'.$comp->title.' - '.$node->field_description_comp['und'][$key]['value'].'</div>
						</div>
						<div class="col col-xs-3 maliste_comapre_amount odd">'.$node->field_amount['und'][$key]['value'].'</div>';
		} else {
			$html .= '<div class="col col-xs-3 maliste_comapre_title even">
						<div class="inner_wrapper">'.$comp->title.' - '.$node->field_description_comp['und'][$key]['value'].'</div>
					</div>
					<div class="col col-xs-3 maliste_comapre_amount even">'.$node->field_amount['und'][$key]['value'].'</div>
					</div>';
		}
		
		
	}
	if (($key % 2 == 0) && ($min_ones != 0)){
		$html .= "</div>";
	}
	print $html;
?>
</div>