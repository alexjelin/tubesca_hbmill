<table <?php 
global $tc_translations; 
$components_results = array();
$field_component_dubl = NULL;

if ($classes) {
  print 'class="table-wishlist' . $classes . '" ';
}
?><?php print $attributes; ?>>
    <?php if (!empty($title) || !empty($caption)) : ?>
    <caption><?php print $caption . $title; ?></caption>
  <?php endif; ?>

  <tbody>
    <?php foreach ($rows as $row_count => $row): ?>
      <tr <?php
      if ($row_classes[$row_count]) {
        print 'class="' . implode(' ', $row_classes[$row_count]) . '"';
      }
			$flag = flag_get_user_flags('commerce_product', $row['product_id']);
			foreach($flag as $flagging_object) { $flagging_id = $flagging_object->flagging_id; }
			$flagging = entity_load_single('flagging', $flagging_id);
   		$product_count = $flagging->field_qty['und'][0]['value'];
      ?>>
          <?php foreach ($row as $field => $content): ?>

          <?php
          switch ($field) {

            case 'field_image_setting1':
              print "<td><div id='visual_image'>" . $content . "</div></td>";

              break;

            case 'title':
              print "<td class='" . $field_classes[$field][$row_count] . "'>" . $content .' x '.$product_count. "<br/>";
              break;

            case 'sku':
              print "<div> Ref : " . $content . "</div>";
              break;

            case 'field_float_setting1':
              if (!empty($content)) {
                //print "<div> Hauter plancher: " . $content . "</div>";
              }
              break;

            case 'field_float_setting1_1':
              if (!empty($content)) {
                //print "<div>  Hauter plancher 2: " . $content . "</div>";
              }
              break;

            case 'field_component_id':

              $field_component_ids = explode("||", $content);
              $field_component_dubl = $field_component_ids;
              if (strlen($field_component_ids[0]) > 0) {
                for ($index = 0; $index < count($field_component_ids); $index++) {
                  $components_results[$index] = "<div class='inner_cells'>" . $field_component_ids[$index] . " - ";
                }
              }
              break;

            case 'field_description_comp':

              $field_descriptions_comp = explode("||", $content);
              if (strlen($field_descriptions_comp[0]) > 0) {
                for ($index = 0; $index < count($field_descriptions_comp); $index++) {
									if(isset($components_results[$index]))
                    $components_results[$index].=$field_descriptions_comp[$index] . " - ";
                }
              }
              break;

            case 'field_amount':
              $field_amounts = explode("||", $content);
              if (strlen($field_amounts[0]) > 0 && !empty($field_amounts)) {
                for ($index = 0; $index < count($field_amounts); $index++) {
                  if (is_array($field_amounts)) {
										if(isset($components_results[$index]))
                      $components_results[$index].=$field_amounts[$index] . "</div>";
                  }
                }
              }
              ?>



              <?php
              $counter_nom = 0;

              foreach ($components_results as $value) {
                if (strlen($field_component_dubl[0]) > 0) {
			
                  if ($counter_nom == 0) {
                    print " <br/> <div> ".$tc_translations['string-area-358']." : </div><table id='num_table'>";
                  }
				  
	  
				 if (($counter_nom % 2) == 0){
					    if (isset($value)) {
                         print "<tr><td><div class='inner_numenclatures'>" . $value . "</div></td>";
                       } 
				  }
					  else{
					 	if (isset($value)) {
                         print "<td><div class='inner_numenclatures'>" . $value . " </div></td></tr>";
                       } 
					  }
                  
                } // check for print the numencature
						if (count($components_results) == ($counter_nom+1)) {
                    print "</table></td>";
                  }
                $counter_nom++;
              }

              break;
          }
          ?> 


        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>