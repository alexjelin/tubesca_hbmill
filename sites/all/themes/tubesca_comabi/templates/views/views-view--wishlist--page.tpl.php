<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>

<div class="<?php print $classes; ?>"> <?php print render($title_prefix); ?>
  <?php if ($title): ?>
  <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
  <div class="view-header"> <?php print $header; ?> </div>
  <?php endif; ?>
  <?php if ($exposed): ?>
  <div class="view-filters"> <?php print $exposed; ?> </div>
  <?php endif; ?>
  <?php if ($attachment_before): ?>
  <div class="attachment attachment-before"> <?php print $attachment_before; ?> </div>
  <?php endif; ?>
  <?php if ($rows): ?>
  <div class="view-content"> <?php print $rows; ?>
    <?php $flag =  flag_get_user_flags('commerce_product'); 
foreach($flag['wishlist'] as $key => $value){
	$nids .=$key.","; 
}
$nids = substr($nids, 0, -1);
global $language;
global $tc_translations, $tubesca_nav_vars;
?>
    <div class="wishlist_buttons clearfix">
      <div class="clearfix buttons-wrapper">
      <a class="btn wishbtn bnt-default" href="/<?php print $language->language; ?>/printpdf/<?php print $tubesca_nav_vars['wishlist_pdf']; ?>"><img src="<?php print $tubesca_nav_vars['path_prefix']; ?>/sites/all/themes/tubesca_comabi/images/download.png" /> <?php print $tc_translations['string-area-133']; ?></a> 
      <a class="btn wishbtn bnt-default print-wishlist" href="" ><img src="<?php print $tubesca_nav_vars['path_prefix']; ?>/sites/all/themes/tubesca_comabi/images/print.png" />  <?php print $tc_translations['string-area-173']; ?>
       </a> 
       <a class="ctools-use-modal ctools-modal-modal-popup-medium wishbtn btn bnt-default" href="modal_forms/nojs/webform/862"> 
       <img src="<?php print $tubesca_nav_vars['path_prefix']; ?>/sites/all/themes/tubesca_comabi/images/sendto.png" /> <?php print $tc_translations['string-area-289']; ?></a>
        <button class="remove_all btn btn-default" data-nids="<?php print $nids; ?>"><img src="<?php print $tubesca_nav_vars['path_prefix']; ?>/sites/all/themes/tubesca_comabi/images/remove.png" /> <?php print $tc_translations['string-area-134']; ?></button>
        </div>
      <iframe src="" name="printframe" style="display:none;"></iframe>
    </div>
  </div>
  <?php elseif ($empty): ?>
  <div class="view-empty"> <?php print $empty; ?> </div>
  <?php endif; ?>
  <?php if ($pager): ?>
  <?php print $pager; ?>
  <?php endif; ?>
  <?php if ($attachment_after): ?>
  <div class="attachment attachment-after"> <?php print $attachment_after; ?> </div>
  <?php endif; ?>
  <?php if ($more): ?>
  <?php print $more; ?>
  <?php endif; ?>
  <?php if ($footer): ?>
  <div class="view-footer"> <?php print $footer; ?> </div>
  <?php endif; ?>
  <?php if ($feed_icon): ?>
  <div class="feed-icon"> <?php print $feed_icon; ?> </div>
  <?php endif; ?>
</div>
<?php /* class view */ ?>
