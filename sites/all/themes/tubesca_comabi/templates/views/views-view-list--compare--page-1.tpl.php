<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<?php print $wrapper_prefix; ?>
<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<?php
global $lexicon;
$field_order = '';
for ($i = 0; $i < 3; $i++) {
  if (!empty($classes_array[$i])) {
    $filter_node = explode('node', $classes_array[$i]);
    $filter_node = $filter_node[1];
    $filter_node = node_load($filter_node);
    $wrapper = entity_metadata_wrapper('node', $filter_node);
    $delimiter = ',';
    $escaped_filters = str_replace(';', ',', $wrapper->field_field_order3->value());
    if ($field_order != '') {
      $field_order = $field_order . ',' . $escaped_filters;
    }
    else {
      $field_order = $escaped_filters;
    }
    //if($field_order!='') break;
  }
}
if (strpos($field_order, $delimiter) === false)
  $delimiter = ';';
$field_order = explode($delimiter, $field_order);
//print $wrapper->field_field_order3->value();
$i = 0;
$j = 0;
$compare_table = '<table>';
$pure_order = array();
$compare_products = array();
foreach ($field_order as $dicoID) {
  $dicoID = trim($dicoID);
  if (empty($pure_order) || (!in_array($dicoID, $pure_order) && $dicoID != '')) {
    $pure_order[] = $dicoID;
  }
  else {
    continue;
  }
  //preprintr($pure_order);
  if ($i > 0)
    $compare_table .= '</table>';
  $field_name_value = $lexicon[$dicoID]['field_name'];
  $compare_table .= '<table class="table_charac"><tr class="item-list clearfix picture_compare_col"><td>' . $lexicon[$dicoID]['label'] . '</td>';
  $i++;
  $index = 0;
  foreach ($rows as $id => $row):
    ?>
    <?php if ($j == 0) { ?>
      <div class=" <?php print $classes_array[$id]; ?>">

        <?php
        $compare_products[$index] = explode(",", $row);
        $index++;
        ?>

      </div>
      <?php
    }

    $value_node = explode('node', $classes_array[$id]);
    $value_node = $value_node[1];
    $value_node = node_load($value_node);
    $wrapperv = entity_metadata_wrapper('node', $value_node);
    if ($field_name_value) {
      $compare_table .= '<td>' . $wrapperv->{$field_name_value}->value() . '</td>';
    }
    else {
      //print 'dicoID:'.$dicoID;
    }
  endforeach;
  $j++;
}
?>
<?php print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>

<?php
$label_picture = $compare_products[0][0];
$label_title = $compare_products[0][2];

$row_string_picture = '<table><tr class="item-list clearfix picture_compare_col"><td>' . $compare_products[0][0] . '</td>';
$row_string_title = '<tr class="item-list clearfix"><td>' . $compare_products[0][2] . '</td>';

for ($i = 0; $i < count($compare_products); $i++) {
  $row_string_picture.="<td>" . $compare_products[$i][1] . "</td>";
  $row_string_title .= "<td>" . $compare_products[$i][3] . "</td>";
}

$row_string_picture.="</tr>";
$row_string_title.="</tr></table>";
$compare_table = '<div class="compare_data compare_pdf">' . $row_string_picture . $row_string_title . $compare_table . '</div>';
?>
<?php print $compare_table; ?>