<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 $uri = 'public://placeholder-image.png';
 $file = file_load_multiple(array(), array('uri' => $uri));
 $file = reset($file);
 if(isset($file->fid)){
   $img_file = $file;
 }
 $line_separator = '<p>&nbsp;</p>';
 $image_style_file = 'search';
 
 
 if(isset($fields['item_bundle']->content) && ($fields['item_bundle']->content=='node:product_family')){
	 //PRODUCT FAMILY
	 $suffix = '?tab=accessories';
?>
<?php print (isset($fields['field_image_setting1']->content))?$fields['field_image_setting1']->content:theme('image_style',array('style_name' => $image_style_file, 'path' => $img_file->uri, 'attributes' => array('class' => array('tubesca-search-image','tubesca-search-image-'.$img_file->fid)))); ?>
<?php print $line_separator; ?>
<a href="/<?php print $fields['language']->content; ?>/<?php print drupal_get_path_alias('node/'.$fields['item_entity_id']->content, $fields['language']->content).$suffix;
 ?>"><?php print $fields['node_title']->content; ?></a>
 <p><?php print $fields['search_api_excerpt']->content; ?></p>
<div class="clearfix"></div>
<?php }elseif(isset($fields['item_bundle']->content) && ($fields['item_bundle']->content=='node:media')){ 
  //MEDIA
	$uri = 'public://pdf_icon.png';
	$file = file_load_multiple(array(), array('uri' => $uri));
	$file = reset($file);
	if(isset($file->fid)){
		$img_file = $file;
	}
	$suffix = '?tab=accessories';
?>
<?php print (isset($fields['field_image_setting1']->content))?$fields['field_image_setting1']->content:theme('image_style',array('style_name' => $image_style_file, 'path' => $img_file->uri, 'attributes' => array('class' => array('tubesca-search-image','tubesca-search-image-'.$img_file->fid)))); ?>
<?php print $line_separator; ?>
<a href="/<?php print $fields['language']->content; ?>/<?php print drupal_get_path_alias('node/'.$fields['node_field_spare_parts_nid']->content, $fields['language']->content).$suffix; ?>"><?php print $fields['node_title']->content; ?></a>
 <p><?php print $fields['search_api_excerpt']->content; ?></p>
<div class="clearfix"></div>
<?php }
  /*
	elseif(isset($fields['item_bundle']->content) && ($fields['item_bundle']->content=='taxonomy_term:family')){ 
  //FAMILY
?>
<?php 
  $lang = tubesca_core_term_language($fields['item_entity_id']->content);
print (isset($fields['field_image_setting1']->content))?$fields['field_image_setting1']->content:theme('image_style',array('style_name' => $image_style_file, 'path' => $img_file->uri, 'attributes' => array('class' => array('tubesca-search-image','tubesca-search-image-'.$img_file->fid)))); ?>
<?php print $line_separator; ?>
<a href="/<?php print $lang; ?>/<?php print drupal_get_path_alias('taxonomy/term/'.$fields['item_entity_id']->content, $lang);
 ?>"><?php print $fields['taxonomy_term_name']->content; ?></a>
 <p><?php print $fields['search_api_excerpt']->content; ?></p>
<div class="clearfix"></div>
<?php } */ 
?>