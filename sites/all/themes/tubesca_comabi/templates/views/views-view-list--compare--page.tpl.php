<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<?php print $wrapper_prefix; ?>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <?php print $list_type_prefix; ?>
  <li class="first_block">
  <?php
	  $block = module_invoke('block', 'block_view', '1');
		print render($block['content']);
  ?>
  </li>
    <?php 
		  global $lexicon, $tc_translations;
			$field_order = '';
			for($i=0;$i<3;$i++){
				if(!empty($classes_array[$i])){
					$filter_node = explode('node', $classes_array[$i]);
					$filter_node = $filter_node[1];
					$filter_node = node_load($filter_node);
					$wrapper = entity_metadata_wrapper('node', $filter_node);
					$delimiter = ',';
					$escaped_filters = str_replace(';',',', $wrapper->field_field_order3->value());
					if($field_order != ''){
					  $field_order = $field_order.','.$escaped_filters;
					}else{
						$field_order = $escaped_filters;
					}
					//if($field_order!='') break;
				}
			}
			if(strpos($field_order,$delimiter)===false) $delimiter = ';';
			$field_order = explode($delimiter, $field_order);
			//print $wrapper->field_field_order3->value();
			$i = 0;
			$j = 0;
			$compare_table = '';
			$pure_order = array();
  		foreach($field_order as $dicoID){
				$dicoID = trim($dicoID);
				if(empty($pure_order) || (!in_array($dicoID, $pure_order) && $dicoID!='')){
					$pure_order[] = $dicoID;
				}else{
					continue;
				}
				//preprintr($pure_order);
        if($i>0) $compare_table .= '</ul></div>';
				$field_name_value = $lexicon[$dicoID]['field_name'];
			  $compare_table .= '<div class="item-list clearfix"><ul class="row-'.($j%2 ? "odd" : "even").'"><li>'.$lexicon[$dicoID]['label'].'</li>';
        $i++;
		    foreach ($rows as $id => $row): ?>
          <?php if($j==0){ ?>
          <li class="<?php print $classes_array[$id]; ?>"><?php print $row; ?></li>
        <?php
					}
					
			    $value_node = explode('node', $classes_array[$id]);
			    $value_node = $value_node[1];
			    $value_node = node_load($value_node );
			    $wrapperv = entity_metadata_wrapper('node', $value_node);
					if($field_name_value){
						if($dicoID == 168){
					    $usage_arr = array('01' => $tc_translations['string-area-428'],'03' => $tc_translations['string-area-74'],'05' => $tc_translations['string-area-429'],'07' => $tc_translations['string-area-430']);
							$compare_table .= '<li>'.$usage_arr[$wrapperv->{$field_name_value}->value()].'</li>';
					  }else{
   			      $compare_table .= '<li>'.$wrapperv->{$field_name_value}->value().'</li>';
						}
					}else{
						//print 'dicoID:'.$dicoID;
					}
		    endforeach;
				$j++;
		  }
		  $compare_table = '<div class="compare_data">'.$compare_table.'</div>';
		 ?>
<?php print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>
<?php print $compare_table; ?>