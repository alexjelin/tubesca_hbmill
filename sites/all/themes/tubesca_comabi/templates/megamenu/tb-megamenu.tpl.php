<div <?php print $attributes;?> class="<?php print $classes;?>">
  <?php if($section == 'frontend') :?>
	<div class="top_menu_shadow">
	<button  data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar tb-megamenu-button" type="button">
        <div id="navbar-hamburger">
          <span class="sr-only">Toggle navigation</span>
          <i class="fa fa-bars" aria-hidden="true"></i>
        </div>
        <div id="navbar-close" class="hidden">
          <div class="thick_close">&#10006;</div>
        </div>
      </button>
	  </div>
    <div class="nav-collapse <?php print $block_config['always-show-submenu'] ? ' always-show' : '';?>">
  <?php endif;?>
  <?php print $content;?>
  <?php if($section == 'frontend') :?>
    </div>
  <?php endif;?>
</div>
