<?php
/**
 * @file
 * Template for a One Two Two panel layout.
 *
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['firstLeft']: Content in first row left column.
 *   - $content['secondLeft']: Content in the second row left column.
 *   - $content['right']: Content in the row right column.
 *   - $content['bottom']: Content in the bottom row.
 */
?>
<header>
<div class="header-right">
  <div class="header-right-first">
    <?php if ($content['right']) print $content['right']; ?>
  </div>
</div>
<div class="header-left">
  <div class="header-left-first">
    <?php if ($content['firstLeft']) print $content['firstLeft']; ?>
  </div>
  <div class="header-left-second">
    <?php if ($content['secondLeft']) print $content['secondLeft']; ?>
  </div> 
</div>
<div class="header-bottom">
  <div class="header-bottom-first">
    <div id="ajax_replace_auto"></div>
    <?php if ($content['bottom']) print $content['bottom']; ?>
  </div>
</div>
</header>