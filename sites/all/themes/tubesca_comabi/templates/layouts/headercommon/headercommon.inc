<?php

// Plugin definition
$plugin = array(
  'title' => t('Header Common'),
  'category' => t('Custom'),
  'icon' => 'headercommon.png',
  'theme' => 'headercommon',
  'css' => 'headercommon.css',
  'regions' => array(
    'firstLeft' => t('First Left row'),
    'secondLeft' => t('Second Left row'),
    'right' => t('Right row'),
    'bottom' => t('Bottom row'),
  ),
);
