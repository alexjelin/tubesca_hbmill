(function($) {
	var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

	

    $(document).ready(function(e) {
			$('.mobile-slider .slide__caption').each(function(e){
				if(typeof $(this).find('.slide__link a').attr('href') !== 'undefined'){
				  $(this).css('cursor','pointer');
				}
			});
			$('.mobile-slider .slide__caption').click(function(e){
			  if(typeof $(this).find('.slide__link a').attr('href') !== 'undefined'){
					var href = $(this).find('.slide__link a').attr('href');
					window.location = href;
				}
			}); 
		  $('.breadcrumb li.active').wrapInner('<span></span>');
		  $('.print-wishlist').click(function(e) {
			  e.preventDefault();
			  $('iframe[name="printframe"]').attr('src', '/'+Drupal.settings.tubesca_core.lang+'/print/'+Drupal.settings.tubesca_core.tubesca_nav_vars.wishlist_pdf);
    });
			
		 $('<div class="panel-separator"></div>').appendTo( "#panel-bootstrap-region-bottom" );
		 $('.pane-bean-floating-buttons').appendTo( "#panel-bootstrap-region-bottom" );
     $('.ctools-use-modal').on('click', function() {
    if ($('.modal-open').length > 0) {
      $('html, body').animate({
        scrollTop: $(".modal-open").offset().top
      }, 500);
    }
  });

  $('#edit-submitted-typology-client .form-item.form-item-submitted-typology-client').on('click', function() {
    $('#edit-submitted-typology-client .form-item.form-item-submitted-typology-client').removeClass("checked");
    $(this).addClass("checked");
});

			$(window).on('load', function() {
				$('#toogle_menu_button').on('click', function() {
				
					$(this).find("#navbar-hamburger").toggleClass('hidden');
					$(this).find("#navbar-close").toggleClass('hidden');
					  
  });
		$(".mobile_menu .tb-megamenu-submenu").css('width', 'auto');
$(".mobile_menu li.level-1").unbind('mouseenter mouseleave');
$('.mobile_menu .full_menu .product-tabs-vertical .panel-heading a').off();
			/*menu */
$('.mobile_menu .full_menu .product-tabs-vertical .panel-heading a').on('click', function(event){
	event.preventDefault();
	$($(this).attr("href")).toggle();
	$('.mobile_menu .full_menu .product-tabs-vertical .panel-heading').toggle();
	$('.mobile_menu li.level-1 > a').toggle();
	$(this).closest( "li" ).prepend('<div class="mobile_menu_return"><div class="return_button"><p><a href="#" data-target="'+$(this).attr("href")+'"><i class="fa fa-chevron-left" aria-hidden="true"></i>RETOUR</a></p></div><div class="return_text">'+$(this).find('.sub-accordion-heading').text()+'</div></div>');

});
$(document).on( 'click', '.mobile_menu .return_button a', function(event){

	event.preventDefault();
	$($(this).data('target')).removeClass('collapse');
	$($(this).data('target')).toggle();
	$('.mobile_menu .full_menu .product-tabs-vertical .panel-heading').toggle();
	$('.mobile_menu li.level-1 > a').toggle();
	$('.mobile_menu_return').remove();
});
/*end menu */


		//goto acordeon
			
			var url_acordeon = 'none';
			url_acordeon = getUrlParameter('tab');
			if (url_acordeon) {
				if ($( "#"+url_acordeon+" a.goto_tubesca" ).hasClass( "collapsed" ))
				{
					$("#"+url_acordeon+" a.goto_tubesca").trigger("click");
				}
				
				var width = $( window ).width();
					if(width < 992){
						$('html, body').animate({
						 scrollTop: $("#"+url_acordeon).offset().top
					 }, 1);
						 
					} else {
					
						console.log($("#accordion").offset().top);
						$('html, body').animate({
						 scrollTop: $("#accordion").offset().top
					 }, 1);
						
					}
			}
			 
				
			 
			// end goto
			
	});

			 

			$(".goto_tubesca").on('click', function(){
					var width = $( window ).width();
					if(width < 992){
						
						var el = $(this).data('goto');
						setTimeout(
						  function() 
						  {
						$('html, body').animate({
						 scrollTop: $("#"+el).offset().top
					 }, 1);
						 }, 0.5);
					//	window.location = '#'+el;
					}
				});



     $('#mailchimp_modal').on('show.bs.modal', function (e) {
		    var frameSrc = $('#mailchimp_modal_iframe').closest('.bean-newsletter').find('.field-name-field-link a').attr('href');
        $('#mailchimp_modal_iframe').attr("src", frameSrc);
	   });
		
		$('.bean-newsletter .field-name-field-link a').click(function(e){
      $('#mailchimp_modal').modal();
		  return false;
	  });

    $('#mailchimp_modal_generic').on('show.bs.modal', function (e) {
		    var frameSrc = $('#mailchimp_modal_generic_iframe').closest('.bean-generic-32').find('a.generic-btn').attr('href');
        $('#mailchimp_modal_generic_iframe').attr("src", frameSrc);
	  });
		$('.bean-generic-32 a.generic-btn').click(function(e){
    
    $('#mailchimp_modal_generic').modal();
		  return false;
	  });

			// modals.
			$('#myModal').modal();
		
			$('.tb-mega-sub-menu .panel-heading > a').click(function(e) {
        if(typeof $(this).attr('data-href') !== 'undefined'){
					window.location = $(this).attr('data-href');
				}
      });
			$(document).on('click', '[data-toggle="lightbox"]', function(event) {
				event.preventDefault();
				$(this).ekkoLightbox();
			});
 		  $('.panels-bootstrap-row-bootstrap_side-1').wrapInner('<div class="container"></div>');
			$('.panels-bootstrap-row-bootstrap_side_left-1').wrapInner('<div class="container"></div>');
			$('#doc-centre').on('click', function(e) { 
				window.location = '/'+Drupal.settings.tubesca_core.lang+'/'+Drupal.settings.tubesca_core.tubesca_nav_vars.doc_centre+'?type=' + $(this).closest('form').find('.media-filter').selectBox('value')+'&product=' + $(this).closest('form').find('.family-filter').selectBox('value')+'&search=' + $(this).closest('form').find('#search_text').val()+'&show='+$(this).closest('form').find('#show_all').val();
				return false;
      });
			$('.search-button').on('click', function(e) { 
				window.location = '/'+Drupal.settings.tubesca_core.lang+'/'+Drupal.settings.tubesca_core.tubesca_nav_vars.search_page+'/sav/' + $(this).closest('form').find('.search-field').val();
				return false;
      });
			$('#search-sav-form').on('submit', function(e) { 
				window.location = '/'+Drupal.settings.tubesca_core.lang+'/'+Drupal.settings.tubesca_core.tubesca_nav_vars.search_page+'/sav/' + $(this).closest('form').find('.search-field').val();
				return false;
      })
			
			$('#search-page-form').on('submit', function(e) { 
				window.location =  '/'+Drupal.settings.tubesca_core.lang+'/'+Drupal.settings.tubesca_core.tubesca_nav_vars.search_page+'/product/' + $(this).closest('form').find('#auto_text').val();
				return false;
      });
			$('#search-page-form .search-block-icon').on('click', function(e) { 
				$('#search-page-form').submit();
      });
			$('#ajax_replace_auto .search-block-btn').on('click', function(e) { 
				$('#search-page-form').submit();
      });
		 //$(".full_menu > .nav-child").addClass('open-always');
	   $( ".tb-megamenu-main-menu" ).addClass('pc_menu');
	   $( ".tb-megamenu-main-menu" ).clone().removeClass('pc_menu').addClass('mobile_menu').appendTo( ".pane-tb-megamenu-main-menu" );
		 
		 
		 $('.mobile_menu .panel-collapse').each(function(index) {
			 $(this).attr("id",$(this).attr('id')+"_mobile");
		 });
		 $('.mobile_menu .panel-heading > a').each(function(index) {
       if(typeof $(this).attr('data-href') !== 'undefined'){
			   $(this).attr('href',$(this).attr('href')+'_mobile');
			 }
		 });
     $('.pc_menu .tb-mega-sub-menu .panel-heading').click(function(e){
			 $(this).children('a').click();
		 });
	   $('.pc_menu .tb-mega-sub-menu .panel-heading').hover(function(e){
			 $collapsible_element = $(this).children('a').closest('.panel.panel-default').children('.panel-collapse');
			 if(!$collapsible_element.hasClass('in')){
				$(this).closest('.tb-mega-sub-menu').find('.panel-collapse').removeClass('in');
		    $collapsible_element.addClass('in');
			 }
		 },function(e){}
		 );
	    $(".mobile_menu  .tb-megamenu-nav.nav").accordion({
		    collapsible: true,
			  active: false
	    }); //end
	  	// function for each menu option to return to the top after click event 
	  $(".mobile_menu .tb-megamenu-nav .tb-megamenu-item").unbind().click(function() {
	    $(".mobile_menu .tb-megamenu-nav").prepend($(this));
		}); // end
		// function for menu button to animate differents animations after open and close actions
	  (function() {
			"use strict";
	 		var toggles = document.querySelectorAll(".c-hamburger");
			for (var i = toggles.length - 1; i >= 0; i--) {
	  		var toggle = toggles[i];
				toggleHandler(toggle);
			};
	
			function toggleHandler(toggle) {
		  	toggle.addEventListener("click", function(e) {
				  e.preventDefault();
				  (this.classList.contains("is-active") === true) ? this.classList.remove("is-active"): this.classList.add("is-active");
			  });
			}
	})(); //end

    });
   
	  $(document).on('change','#find-solution',function(){
			$(this).closest('.bean-find-solution').find('.field-name-field-link  a').attr('href', $(this).val());
		});

    Drupal.behaviors.nanoScrollbar = {
      attach: function(context, settings) {
				
		    $('.nano-scrollbar-product > .panel-body').wrap('<div class="content"></div>');
		    $('.nano-scrollbar-product').nanoScroller({ contentClass: 'content'});
        $('.nano-scrollbar-product').on('shown.bs.collapse',function(e) {
          $(this).nanoScroller({
            contentClass: 'content',
				  });
      });
			
			/*jQuery.each(Drupal.settings.tubesca.zzz, function (i, val) {
				console.log(val);
			});*/
			
	   }
   };

	
	$(window).resize(function() {

	// JS code for contact form (plugin for style radio and check buttons
	$("#webform-client-form-13 input[name='submitted[typology_client]']").on('change', function() {
    $("#webform-client-form-13 #edit-submitted-typology-client .radio").removeAttr("style");
	  $("#webform-client-form-13 #edit-submitted-typology-client .radio").removeClass("checked");
		var value_radio = $("#webform-client-form-13 input[name='submitted[typology_client]']:checked").val();
		switch (value_radio) {
			case "utilisateur":
	  		$("#webform-client-form-13 #edit-submitted-typology-client .radio:nth-child(3)").css({
		  		"background": "#1b66ea",
					"color": "white"
				});
				$("#webform-client-form-13 #edit-submitted-typology-client .radio:nth-child(3)").addClass("checked");
			break;
			case "loueur":
			$("#webform-client-form-13 #edit-submitted-typology-client .radio:nth-child(2)").css({
			  "background": "#1b66ea",
				"color": "white"
			});
			$("#webform-client-form-13 #edit-submitted-typology-client .radio:nth-child(2)").addClass("checked");
			  break;
			case "distributeur":
				$("#webform-client-form-13 #edit-submitted-typology-client .radio:nth-child(1)").css({
					"background": "#1b66ea",
			    "color": "white"
				});
		    $("#webform-client-form-13 #edit-submitted-typology-client .radio:nth-child(1)").addClass("checked");
	    break;
	}
	}); //end
	});
	
	$(".mobile_menu .btn-navbar.tb-megamenu-button").ready(function() {
   	$(".mobile_menu .btn-navbar.tb-megamenu-button").attr('id', "toogle_menu_button");
	  $(".mobile_menu .btn-navbar.tb-megamenu-button").removeClass();
	  $(".mobile_menu #toogle_menu_button").addClass("c-hamburger--htx");
	 // var span_el = document.createElement("span");
	  //span_el.innerHTML = "Toggle Menu";
	  //$(".mobile_menu #toogle_menu_button").append(span_el);
	});
	
	 Drupal.behaviors.ajaxExample = {
    attach: function (context, settings) {

 
			$(".webform-client-form input[type='radio']").kalypto({
	      hideInputs: false,
	      toggleClass: "toggleR",
	    });
	    $(".webform-client-form .webform-progressbar .webform-progressbar-page:nth-child(3)").css("left", "40%");
	    $(".webform-client-form .webform-progressbar .webform-progressbar-page:last-child").css("left", "84%");
			
			$(".webform-component-file").each(function(index, element) {
        $(element).find('button.form-submit').text($(element).find('label').text());
      });
			
    }
  };
	
	Drupal.behaviors.autoFocus = {
    attach:function()
    {
      $(".form-text:first").focus();
    }
  };
    $(document).ready(function(){
        $('img.promo-desktop').on('click',function(e){
		$(this).parent().find('a.btn-black')[0].click();
	});
    });
  		
})(jQuery);

jQuery.noConflict();
jQuery(document).ready(function(){
jQuery(".active-radio").click(function(e){
if (jQuery("#edit-submitted-je-souhaite-1").prop("checked")){
    jQuery("#edit-submitted-je-souhaite-1").prop("checked", false);
    }else{
   jQuery("#edit-submitted-je-souhaite-1").prop("checked", true);
 }  
});
});


jQuery.noConflict();
jQuery(document).ready(function(){
jQuery(".radio-active").click(function(e){
if (jQuery("#edit-submitted-je-souhaite-2").prop("checked")){
    jQuery("#edit-submitted-je-souhaite-2").prop("checked", false);
    }else{
   jQuery("#edit-submitted-je-souhaite-2").prop("checked", true);
 }  
});
});
