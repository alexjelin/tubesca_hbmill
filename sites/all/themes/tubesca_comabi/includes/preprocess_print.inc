<?php
/**
 * @file
 * Preprocess : print
 */
 
/**
 *  Override or insert variables for the page templates.
 * Implements template_preprocess_print().
 */
function tubesca_comabi_preprocess_print( &$variables, $hook ) {
  
  if($variables['node']->type=='product_family'){
		global $lexicon; // = tubesca_core_dico_to_field();
		global $language;
		global $user;
		global $tc_translations,$tubesca_nav_vars;
		
		$body = field_get_items('node', $variables['node'], 'body');
		$body = field_view_value('node', $variables['node'], 'body', $body[0]);
		$variables['body'] = '';
		if(isset($body['#markup'])){
		  $variables['body'] = $body['#markup'];
		}
		
		$top_axess = '';
	$isTopAxess = field_get_items('node', $variables['node'], 'field_boolean_setting6');
  
	if($isTopAxess[0]['value']==1){
		$temp_var = field_get_items('node', $variables['node'], 'field_integer_setting13');
    $temp_var = field_view_value('node', $variables['node'], 'field_integer_setting13', $temp_var[0]);
		if(!empty($temp_var['#markup'])){
			$top_axess .= '<p class="top-axess-price">'.$temp_var['#markup'].'€</p>';
		}
		$temp_var = field_get_items('node', $variables['node'], 'field_sentetance_price');
    $temp_var = field_view_value('node', $variables['node'], 'field_sentetance_price', $temp_var[0]);
		if(!empty($temp_var['#markup'])){
		  $top_axess .= '<div class="top-axess-sentetance">'.$temp_var['#markup'].'</div>';
		}
	}
		$variables['top_axess'] = $top_axess;
		$family = field_get_items('node', $variables['node'], 'field_family');
		$family = field_view_value('node', $variables['node'], 'field_family', $family[0]);
		$family = $family['#options']['entity'];
		
		$parent_family = taxonomy_get_parents($family->tid);
		$variables['family'] = $family->name;
		foreach($parent_family as $pf){
		$variables['parent_family'] = $pf->name; 
		}
		$temp_var = field_get_items('taxonomy_term', $family, 'field_tools_rules');
		$tools_rules = array();
		if(!empty($temp_var)){
		foreach($temp_var as $tr){
			$tools_rules[] = $tr['value'];
		}
		}
		
		$field_erp_id = field_get_items('node', $variables['node'], 'field_erp_id');
		$field_erp_id = field_view_value('node', $variables['node'], 'field_erp_id', $field_erp_id[0]);
		
		//preprintr($field_erp_id);
		//Buttons
		$download_pdf_btn = '';
		$choose_model_btn = '';
		$compare_btn = '';
		$choose_model_btn2 = '';
		if($user->uid > 0){
		$download_pdf_btn = '<div class="btn1"><a href="/printpdf/'.$variables['node']->nid.'">'.$tc_translations['string-area-63'].'</a></div>';
		if(in_array(9, $tools_rules)){ //configurator
		$choose_model_btn = '<div class="btn2"><a href="/'.$language->language.'/'.$tubesca_nav_vars['configurator'].'/'.$field_erp_id['#markup'].'">'.$tc_translations['string-area-325'].'</a></div>';
		}else{
		$choose_model_btn = '<div class="btn2"><a href="#ma-liste">'.$tc_translations['string-area-24'].'</a></div>';
		}
		if(in_array(11, $tools_rules)){ //comparateur
		$compare_btn = '<div class="producy_compare">'.flag_create_link('compare', $variables['node']->nid).'</div>'; //<a href="#">Ajouter au comparateur <span class="glyphicon glyphicon-plus"></a></div>';
		}
		if(in_array(1, $tools_rules) && in_array(9, $tools_rules)){ //configurator
		$choose_model_btn2 = '<div class="btn2 choose-model"><a href="/'.$language->language.'/'.$tubesca_nav_vars['configurator'].'/'.$field_erp_id['#markup'].'">'.$tc_translations['string-area-3'].'</a></div>';
		}elseif(in_array(1, $tools_rules)){
		$choose_model_btn2 = '<div class="btn2 choose-model"><a href="#choose-modele">'.$tc_translations['string-area-5'].'</a></div>';
		}
		}else{
		$choose_model_btn = '<div class="btn2"><a href="/user">'.$tc_translations['string-area-6'].'</a></div>';
		}
		
		$variables['download_pdf_btn'] = $download_pdf_btn;
		$variables['choose_model_btn'] = $choose_model_btn;
		$variables['compare_btn'] = $compare_btn;
		
		//Usage
		$usage_var = field_get_items('node', $variables['node'], 'field_usage'); 
		$variables['usage'] = '&nbsp;';
		$usage_arr = array('01' => $tc_translations['string-area-428'],'03' => $tc_translations['string-area-74'],'05' => $tc_translations['string-area-429'],'07' => $tc_translations['string-area-430']);
		if(empty($usage_var[0])){
			$usage_var = field_get_items('node', $variables['node'], 'field_text_setting4');
			$usage_var = field_view_value('node', $variables['node'], 'field_text_setting4', $usage_var[0]);
			//preprintr($usage_var);
			if($usage_var['#markup']!=''){
				$variables['usage'] = '<div class="product-usage"><span>'.$tc_translations['string-area-64'].' : '.tubesca_core_label_trad($usage_arr[$usage_var['#markup']]).'</span></div>';
			}
		}else{
			$variables['usage'] = '<div><span>'.$tc_translations['string-area-64']. tubesca_core_label_trad($usage_var[0]['taxonomy_term']->name).'</span></div>';
		}
		if(empty($tools_rules)) $tools_rules = array(1,2,5,6,7,8,9,10,11,12);
			
		$variables['middle_block'] = '';
		$first_block = 0;
		if(in_array(1, $tools_rules)||in_array(5, $tools_rules)||in_array(6, $tools_rules)||in_array(7, $tools_rules)){
		//$variables['middle_block'] = '<div class="product-tabs-wrapper"><div class="container"><div class="panel-group product-tabs-vertical" id="accordion">';
		if(in_array(1, $tools_rules)){
			
			$sort_field = '';
			if(in_array(2, $tools_rules)){
				$sort_field = 'field_max_work_height';
				$sort_field_id = '199';
			}elseif(in_array(3, $tools_rules)){
				$sort_field = 'field_weight';
				$sort_field_id = '34';
			}elseif(in_array(4, $tools_rules)){
				$sort_field = 'field_step_width';
				$sort_field_id = '85';
			}
			
			$references = field_get_items('node', $variables['node'], 'field_reference');
			$refs = array();
			if(!empty($references)){
				foreach($references as $ref){
					$refs[] = $ref['product_id'];
				}
			}
			$sorted_ids = array();
			$references = commerce_product_load_multiple($refs);
			$wrapped_refs = array();
			foreach($references as $rid => $reference){
				$ref_entity = entity_metadata_wrapper('commerce_product',$reference);
				if($ref_entity->$sort_field->value()!=''){
					$sorted_ids[$rid] = $ref_entity->$sort_field->value();
				}
				$wrapped_refs[$rid] = $ref_entity;
				//print $ref_entity->sku->value().': '.$ref_entity->$sort_field->value().'<br/>';
			}
			asort($sorted_ids);
			$dicoID401 = field_get_items('node', $variables['node'], 'field_field_field_order2');
			$dicoID401 = field_view_value('node', $variables['node'], 'field_field_field_order2', $dicoID401[0]);
			if(!empty($dicoID401['#markup'])){
				$delimiter = ',';
				if(strpos($dicoID401['#markup'],$delimiter)===false) $delimiter = ';';
				$field_order2 = explode($delimiter,$dicoID401['#markup']);
			}
			$i=0;
			$collapsed = '';
			$in2 =' in';
			$inner_pannel_pane ='';
			$table_header = 0;
			foreach($sorted_ids as $ref_id => $ref){
				$i++;
				if($lexicon[$sort_field_id]['field_type'] == 'Float'){
					$ref = str_replace('.',',',$ref);	
				}
				$table_header++;
				//$inner_pannel_pane .='<div class="panel panel-default"><div class="panel-heading"><a class="'.$collapsed.'" data-toggle="collapse" data-parent="#accordion_inner" href="#collapse_inner_'.$i.'">'.$ref.$lexicon[$sort_field_id]['unit'].'</a></div><div id="collapse_inner_'.$i.'" class="panel-collapse collapse'.$in2.'"><div class="panel-body">';
				$in2='';
				if($table_header==1) $inner_pannel_pane_th = '';
				$collapsed = 'collapsed';
				if(!empty($field_order2)){
					$inner_pannel_pane .='';
					if($table_header==1) $inner_pannel_pane_th .='<tr>'.'<th>Ref</th>';
					$inner_i = 1;
					$num_settings = count($field_order2)+1;
					foreach($field_order2 as $dicoID){
						if( ($inner_i>1) && ($inner_i % $num_settings) == 1) $inner_pannel_pane .='<tr>';
						if($inner_i == 1){
							$inner_pannel_pane .='<tr><td>'.$wrapped_refs[$ref_id]->label().'</td>';
							$inner_i++;
						}
						if(strpos($lexicon[$dicoID]['field_name'],'[')){
							$fname = explode('[',$lexicon[$dicoID]['field_name']);
							$fname = $fname[0];
						}else{
							$fname = $lexicon[$dicoID]['field_name'];
						}
						$dicoID_value = '';
						if($lexicon[$dicoID]['element_type']==3){
							if(is_array($wrapped_refs[$ref_id]->$fname->value())){
								foreach($wrapped_refs[$ref_id]->$fname->value() as $array_key => $array_val){
									if(isset($array_val['value'])){
										$dicoID_value.= $array_val['value'];
									}else{
										if($array_key=='value')
										  $dicoID_value.= $array_val;
									}																
								}
								//preprintr($wrapped_refs[$ref_id]->$fname->value()); // a case
							}else{
								$dicoID_value = $wrapped_refs[$ref_id]->$fname->value();
							}
							if($lexicon[$dicoID]['field_type'] == 'Float'){
							  $dicoID_value = str_replace('.',',',$dicoID_value);	
							}
						  //print $dicoID.' '.$lexicon[$dicoID]['label'].': '.$wrapped_refs[$ref_id]->$fname->value().'<br/>'; // a case
							//$path_to_icon = tubesca_core_display_dico_icon($pf->field_erp_ext_id['und'][0]['value'],$dicoID);
							$path_to_icon = -1; //Show only labels
							if($path_to_icon!=-1){
								$prefix = '<img src="'.$path_to_icon.'" data-toggle="tooltip" data-placement="top" title="'.$lexicon[$dicoID]['label'].'">';
							}else{
								$prefix = '<span>'.$lexicon[$dicoID]['label'].' :</span> ';
							}
							if($table_header==1) $inner_pannel_pane_th .= '<th>'.$prefix.'</th>';
							$inner_pannel_pane .='<td>'.$dicoID_value.$lexicon[$dicoID]['unit'].'</td>';
							$inner_i++;
							if( ($inner_i % $num_settings) == 1) $inner_pannel_pane .='</tr>';
						}elseif($lexicon[$dicoID]['element_type']==2){
							$tempy = field_get_items('node', $variables['node'], $fname);
							$tempy = field_view_value('node', $variables['node'], $fname, $tempy[0]);
							//print $lexicon[$dicoID]['label'].': '.$fname.'<br/>';
							//preprintr($tempy);
							//print render($tempy); // a case
						}
					}
					
					
					
				}
				
			}
			$inner_pannel_pane_th .= '</tr>';
			if($inner_pannel_pane!=''){
			  $inner_pannel_pane = '<table>'.$inner_pannel_pane_th.$inner_pannel_pane.'</table>';
				$inner_pannel_pane = '<div class="sub_titles">'.$tc_translations['string-area-50'].'</div>'.$inner_pannel_pane.'<br/>';
			  //preprintr($dicoID401);	
			  //$inner_pannel_pane .= $choose_model_btn2;	 
		  	$variables['table1'] = $inner_pannel_pane;
			}else{
				$variables['table1'] = '';
			}
			//$inner_accordion_title = '<h2 data-toggle="tooltip" data-placement="top" title="La hauteur de travail est généralement de 2m" ><img src="/sites/all/themes/tubesca_comabi/images/pictos/dicoID_'.$sort_field_id.'.png">'.$lexicon[$sort_field_id]['label'].'</h2>';
			
			$first_block = 1;
			/*$inner_accordion = '
						<!-- INNER ACCORDION -->'.$inner_accordion_title.'						
						<div class="panel-group" id="accordion_inner">'.$inner_pannel_pane.'</div>
						<!-- END INNER ACCORDION -->';
			$variables['middle_block'] .='<div class="panel panel-default"><div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><i class="table_size_icon tab_icon"></i><span>Voir les</br>différents modèles</span></a></div><div id="collapse1" class="panel-collapse collapse in nano-scrollbar-product"><div class="panel-body">'.$inner_accordion.'</div></div></div>'; */
		}
		if(in_array(5, $tools_rules)){
			if($first_block==0){
				$in = ' in';
				$first_block = 1;
			}else{
				$in = '';	
			}
			//DicoID 402
			$dicoID402 = field_get_items('node', $variables['node'], 'field_field_order3');
			$dicoID402 = field_view_value('node', $variables['node'], 'field_field_order3', $dicoID402[0]);
			$porduct_family_entity = entity_metadata_wrapper('node',$variables['node']);
			if(!empty($dicoID402['#markup'])){
				$dicoID402['#markup']=str_replace('338325','338;325',$dicoID402['#markup']); //temp fix for this erorr in erp data
				$delimiter = ',';
				if(strpos($dicoID402['#markup'],$delimiter)===false) $delimiter = ';';
				$field_order3 = explode($delimiter,$dicoID402['#markup']);
				//$inner_accordion = '<table><tr><th>'.$tc_translations['string-area-10'].'</th><th>'.$tc_translations['string-area-367'].'</th></tr>';
				$inner_accordion = '<table>';
				foreach($field_order3 as $dicoID){
			    $temp_ext_id = field_get_items('taxonomy_term', $family, 'field_erp_ext_id');
          $temp_ext_id = field_view_value('taxonomy_term', $family, 'field_erp_ext_id', $temp_ext_id[0]);
					$temp_ext_id = $temp_ext_id['#markup'];
					/*if(in_array($temp_ext_id, array('tcFamACCES-LEGERECHELLES','tcFamACCES-LEGERPLATES-FORMES','tcFamACCES-LEGERMARCHEPIEDS'))&& $dicoID == 189){
						$dicoID = 464;
					}*/
					if($lexicon[$dicoID]['cardinality']!=1){
						eval('$field_value = $porduct_family_entity->' . $lexicon[$dicoID]['field_name'] . '->value();');
            $field_value = $field_value[$lexicon[$dicoID]['delta']]['value'];
					}else{
						$fname = $lexicon[$dicoID]['field_name'];
						//print $lexicon[$dicoID]['field_name'].'<br/>'; 
						if(isset($porduct_family_entity->$fname)){
							$field_value = $porduct_family_entity->$fname->value();
							if($field_value=='') $field_value .= ' ';//.$dicoID.' -> '.$fname;
						}else{
							//$field_value = $lexicon[$dicoID]['field_name'].' - '.$dicoID;
						}
					}
					if($dicoID == 168){
						$field_value = ($usage_var['#markup']!='')?$usage_arr[$usage_var['#markup']]:$usage_var[0]['taxonomy_term']->name;
					}
					if($field_value!=''){
						if($lexicon[$dicoID]['field_type'] == 'Float'){
							$field_value = str_replace('.',',',$field_value);	
						}
						if (is_array($field_value)) {
                $field_value = $field_value['value'];
            }
						$inner_accordion .= '<tr><td>'.$lexicon[$dicoID]['label'].'</td><td>'.$field_value.$lexicon[$dicoID]['unit'].'</td></tr>'; //Do filter on empty fields?
					}
				}
				$inner_accordion .= '</table>';
				//$inner_accordion .= $choose_model_btn2;
			}
			if($inner_accordion!='' && $inner_accordion !='<table></table>'){
				$variables['table2'] .='<div class="sub_titles">'.$tc_translations['string-area-10'].'</div>';
			  $variables['table2'] .= $inner_accordion.'<br/>';
			}
		}
		if(in_array(6, $tools_rules)){
			if($first_block==0){
				$in = ' in';
				$first_block = 1;
			}else{
				$in = '';	
			}
			$tempy = field_get_items('node', $variables['node'], 'field_video_promo');
			$tempy = field_view_value('node', $variables['node'], $fname, $tempy[0]);
			//if(!empty($tempy['#markup'])){
				//$inner_accordion_video .= '<div class="embed-container"><iframe width="100%" height="455" src="https://www.youtube.com/embed/'.$tempy['#markup'].'" frameborder="0" allowfullscreen></iframe></div>';
			//}
			$inner_accordion = '';
			$tempy = field_get_items('node', $variables['node'], 'field_certificate');
			if(!empty($tempy[0])){
				$inner_accordion .= '<li><a href="'.file_create_url($tempy[0]['uri']).'" class="download_link" target="_blank">'.(!empty($tempy[0]['description'])?$tempy[0]['description']:$lexicon['194']['label']).'</a></li>';
			}
			$tempy = field_get_items('node', $variables['node'], 'field_notice');
			if(!empty($tempy[0])){
				$inner_accordion .= '<li><a href="'.file_create_url($tempy[0]['uri']).'" class="download_link" target="_blank">'.(!empty($tempy[0]['description'])?$tempy[0]['description']:$lexicon['195']['label']).'</a></li>';
			}
			//preprintr($tempy);
			if($inner_accordion!=''){
				$inner_accordion = '<ul>'.$inner_accordion.'</ul>';
			}
			$inner_accordion = $inner_accordion_video.$inner_accordion;
			$variables['middle_block'] .= $inner_accordion;
		}
		if(in_array(7, $tools_rules)){
			if($first_block==0){
				$in = ' in';
				$first_block = 1;
			}else{
				$in = '';	
			}
			//$inner_accordion = '<div class="product-accessories-downloads">';
			//What DicoID?? - 426
			$tempy = field_get_items('node', $variables['node'], 'field_file_setting2');
			if(!empty($tempy[0])){
			 // $inner_accordion .= '<div class="btn1"><a href="'.file_create_url($tempy[0]['uri']).'">Télécharger la vue éclatée</a></div>';	
			}
			
			if($user->uid == 0){
				//$inner_accordion .= '<div class="btn2">Vous avez besoin de pièces détachées ?<br/><a href="#">Connectez-vous</a></div>';
			}
			//$inner_accordion .= '</div>';
			$inner_accordion = '';
			$spare_parts = tubesca_core_get_accessories($variables['node']->nid);
			//preprintr($spare_parts);
			if(!empty($spare_parts)){
				$references = commerce_product_load_multiple($spare_parts);
				$wrapped_spares = array();
				//$inner_accordion .= '<table><tr><th>'.$tc_translations['string-area-16'].'</th><th>'.$tc_translations['string-area-17'].'</th>';
				$inner_accordion .= '<table>';
				//if($user->uid > 0){
					//$inner_accordion .= '<th></th>';
				//}
				$inner_accordion .= '</tr>';
				foreach($references as $rid => $reference){
					$ref_entity = entity_metadata_wrapper('commerce_product',$reference);
					$description = $ref_entity->field_description->value();
					$inner_accordion .= '<tr><td>'.$ref_entity->label().'</td><td>'.$description['value'].'</td>';
					//if($user->uid > 0){
					//  $inner_accordion .= '<td>'.tubesca_core_flag_fields_create($ref_entity->getIdentifier(), 'field_qty', 'wishlist').'</td>';
				// }
				 $inner_accordion .= '</tr>';
			
			}
			$inner_accordion .= '</table>';
			}
			if($inner_accordion!='' && $inner_accordion !='<table></table>'){
				$variables['table3'] .='<div class="sub_titles">'.$tc_translations['string-area-182'].'</div>';
			  $variables['table3'] .= $inner_accordion.'';
			}
		}
		//$variables['middle_block'] .= '</div></div></div>';
		}
		
		$temp_var = field_get_items('node', $variables['node'], 'field_arg_1');
		$temp_var = field_view_value('node', $variables['node'], 'field_arg_1', $temp_var[1]);
		if(!empty($temp_var['#markup'])){
		if($variables['arguments'] == '') $variables['arguments'].='<ul>';
		$variables['arguments'] .= '<li>'. $temp_var['#markup'].'</li>';
		}	
		
		$variables['normes'] = '&nbsp;';
	$temp_var = field_get_items('node', $variables['node'], 'field_boolean_setting5');		
	if(!empty($temp_var) && ($temp_var[0]['value'] == 1)){
		$uri = 'public://dicoID192.png';
		if(file_exists(image_style_path('normes',  $uri))){
		  $img_url_rel = str_replace('public://',$tubesca_nav_vars['path_prefix'].'/sites/default/files/',image_style_path('normes',  $uri));
	    $variables['normes'] .= '<img src="'.$img_url_rel.'" >';
		}
	}
	
	$temp_var = field_get_items('node', $variables['node'], 'field_boolean_setting1');		
	if(!empty($temp_var) && ($temp_var[0]['value'] == 1)){
		$uri = 'public://gs_lne_2011.png';
    if(file_exists(image_style_path('normes',  $uri))){
		  $img_url_rel = str_replace('public://',$tubesca_nav_vars['path_prefix'].'/sites/default/files/',image_style_path('normes',  $uri));
	    $variables['normes'] .= '<img src="'.$img_url_rel.'" >';
		}
	}
	
	$temp_var = field_get_items('node', $variables['node'], 'field_boolean_setting2');		
	if(!empty($temp_var) && ($temp_var[0]['value'] == 1)){
		$uri = 'public://dicoID329.png';
	  if(file_exists(image_style_path('normes',  $uri))){
		  $img_url_rel = str_replace('public://',$tubesca_nav_vars['path_prefix'].'/sites/default/files/',image_style_path('normes',  $temp_file->uri));
	    $variables['normes'] .= '<img src="'.$img_url_rel.'" >';
		}
	}
	
	$temp_var = field_get_items('node', $variables['node'], 'field_boolean_setting4');		
	if(!empty($temp_var) && ($temp_var[0]['value'] == 1)){
		$uri = 'public://dicoID331.png';
    $temp_file = file_load_multiple(array(), array('uri' => $uri));
    $temp_file = reset($temp_file);
	  if(file_exists(image_style_path('normes',  $temp_file->uri))){
		$img_url_rel = str_replace('public://',$tubesca_nav_vars['path_prefix'].'/sites/default/files/',image_style_path('normes',  $temp_file->uri));
	  $variables['normes'] .= '<img src="'.$img_url_rel.'" >';
		}
	}
	if($variables['normes']!='&nbsp;'){
		$variables['normes'] = '<span class="normes-logos">'.$variables['normes'].'</span>';
	}
	$variables['qr_code'] = '';
	$temp_var = field_get_items('node', $variables['node'], 'field_qr_code');
	if(!empty($temp_var[0])){
    $variables['qr_code'] =  '<div> Pour en savoir plus:</div><div>'.theme('image', array('path' => $temp_var[0]['uri'])).'</div>';
	}
	
		
		if($variables['normes']!='&nbsp;'){
		$variables['normes'] = '<span>'.$tc_translations['string-area-415'].' :</span>  <span class="normes-logos">'.$variables['normes'].'</span>';
		}
		
		
		//Baseline
		$variables['sub_title'] = '&nbsp;&nbsp;&nbsp;';
		$temp_var = field_get_items('node', $variables['node'], 'field_text_setting3');
		$temp_var = field_view_value('node', $variables['node'], 'field_text_setting3', $temp_var[0]);
		if(!empty($temp_var['#markup'])){
		$variables['sub_title'] = $temp_var['#markup'];
		}
		
		//Logo Enterprise Historique
		$variables['logo_enterprise'] = '';
		$temp_var = field_get_items('node', $variables['node'], 'field_logo_enterprise');
		$temp_var = field_view_value('node', $variables['node'], 'field_logo_enterprise', $temp_var[0]);
		if(!empty($temp_var['#item']['uri'])){
		$variables['logo_enterprise'] = '<div class="logo-enterprise">'.render($temp_var).'</div>';
		}
		
		//Zone de commercialisation France
		$variables['sold_in_france'] = '';
		$temp_var = field_get_items('node', $variables['node'], 'field_boolean_setting3');
		$temp_var2 = field_get_items('node', $variables['node'], 'field_boolean_setting7');
		
		if(!empty($temp_var) && ($temp_var[0]['value'] == 1) && (empty($temp_var2)|| (!empty($temp_var2) && ($temp_var2[0]['value'] == 0)))){
		$variables['sold_in_france'] = '<div><span>'.$tc_translations['string-area-19'].'</span></div>';
		}
		
		//Argumentaire secondaire_1-1, Argumentaire secondaire_2-1, Argumentaire secondaire_3-1
		$variables['arguments'] = '';
		$temp_var = field_get_items('node', $variables['node'], 'field_arg_1');
		$temp_var = field_view_value('node', $variables['node'], 'field_arg_1', $temp_var[1]);
		if(!empty($temp_var['#markup'])){
		if($variables['arguments'] == '') $variables['arguments'].='<ul>';
		$variables['arguments'] .= '<li>'. $temp_var['#markup'].'</li>';
		}
		
		$temp_var = field_get_items('node', $variables['node'], 'field_arg_2');
		$temp_var = field_view_value('node', $variables['node'], 'field_arg_2', $temp_var[1]);
		if(!empty($temp_var['#markup'])){
		if($variables['arguments'] == '') $variables['arguments'].='<ul>';
		$variables['arguments'] .= '<li>'. $temp_var['#markup'].'</li>';
		}
		$temp_var = field_get_items('node', $variables['node'], 'field_arg_3');
		$temp_var = field_view_value('node', $variables['node'], 'field_arg_3', $temp_var[1]);
		if(!empty($temp_var['#markup'])){
		if($variables['arguments'] == '') $variables['arguments'].='<ul>';
		$variables['arguments'] .= '<li>'. $temp_var['#markup'].'</li>';
		}
		if($variables['arguments'] != ''){
		$variables['arguments'].='</ul>';
		$variables['arguments'] = '<p>'.$tc_translations['string-area-20'].':</p>'.$variables['arguments'];
		}
		//Nouveauté
		//$temp_var = field_get_items('node', $variables['node'], 'field_boolean_setting4');
		$temp_var = field_get_items('node', $variables['node'], 'field_button_text');
		$temp_var = field_view_value('node', $variables['node'], 'field_button_text', $temp_var[0]);
		
		$variables['gallery_label'] = '';
		if($temp_var['#markup']!=''){
		$variables['gallery_label'] .= $tc_translations['string-area-21'];
		}
		$temp_var = field_get_items('node', $variables['node'], 'field_boolean_setting6');
		$temp_var = field_view_value('node', $variables['node'], 'field_boolean_setting6', $temp_var[0]);
		
		if($variables['gallery_label'] != '') $variables['gallery_label'] .= ' ';
		if($temp_var['#markup']!=''){
		$variables['gallery_label'] .= 'Promotion';
		}
		if($variables['gallery_label'] != ''){
		$variables['gallery_label'] = '<div class="img_label">'.$variables['gallery_label'].'</div>';
		}
		$mobile_slick_element = array();
		// Add items.
		$items = array();
		$field_image_setting = field_get_items('node', $variables['node'], 'field_image_setting1');
		
		foreach($field_image_setting as $field_image){
		$field_image = field_view_value('node', $variables['node'], 'field_image_setting1', $field_image);
		if(file_exists(image_style_path('product',  $field_image['#item']['uri']))){
		$img_url_rel = str_replace('public://',$tubesca_nav_vars['path_prefix'].'/sites/default/files/',image_style_path('product',  $field_image['#item']['uri']));
		//preprintr($field_image);
		$items[] = array(
			'path' => $field_image['#item']['uri'],
			'slide' =>  '<img src="'.$img_url_rel.'" height="265">',
		);
			}
		}	
		
		$field_image_setting = field_get_items('node', $variables['node'], 'field_visual');
		
		foreach($field_image_setting as $field_image){
			$field_image = field_view_value('node', $variables['node'], 'field_visual', $field_image);
		  if(file_exists(image_style_path('product',  $field_image['#item']['uri']))){
			$img_url_rel = str_replace('public://',$tubesca_nav_vars['path_prefix'].'/sites/default/files/',image_style_path('product',  $field_image['#item']['uri']));
		//preprintr($field_image);
		$items[] = array(
			'path' => $field_image['#item']['uri'],
			'slide' =>  '<img src="'.$img_url_rel.'" height="265">',
		);
			}
		}		
		
		$field_image_setting = field_get_items('node', $variables['node'], 'field_image_setting3');
		
		foreach($field_image_setting as $field_image){
			$field_image = field_view_value('node', $variables['node'], 'field_image_setting3', $field_image);
		 if(file_exists(image_style_path('product',  $field_image['#item']['uri']))){
		    $img_url_rel = str_replace('public://',$tubesca_nav_vars['path_prefix'].'/sites/default/files/',image_style_path('product',  $field_image['#item']['uri']));
		//preprintr($field_image);
		$items[] = array(
			'path' => $field_image['#item']['uri'],
			'slide' =>  '<img src="'.$img_url_rel.'" height="265">',
		);
					}
		}	
		
		$field_image_setting = field_get_items('node', $variables['node'], 'field_image_setting2');
		
		foreach($field_image_setting as $field_image){
			$field_image = field_view_value('node', $variables['node'], 'field_image_setting2', $field_image);
		 if(file_exists(image_style_path('product',  $field_image['#item']['uri']))){
		    $img_url_rel = str_replace('public://',$tubesca_nav_vars['path_prefix'].'/sites/default/files/',image_style_path('product',  $field_image['#item']['uri']));
		//preprintr($field_image);
		$items[] = array(
			'path' => $field_image['#item']['uri'],
			'slide' =>  '<img src="'.$img_url_rel.'" height="265">',
		);
					}
		}
		
		$field_image_setting = field_get_items('node', $variables['node'], 'field_visual_ref');
		
		foreach($field_image_setting as $field_image){
			$field_image = field_view_value('node', $variables['node'], 'field_visual_ref', $field_image);
		 if(file_exists(image_style_path('product',  $field_image['#item']['uri']))){
		    $img_url_rel = str_replace('public://',$tubesca_nav_vars['path_prefix'].'/sites/default/files/',image_style_path('product',  $field_image['#item']['uri']));
		//preprintr($field_image);
		$items[] = array(
			'path' => $field_image['#item']['uri'],
			'slide' =>  '<img src="'.$img_url_rel.'" height="265">',
		);
					}
		}	
		/* Lightbox gallery */
		$base_image = $items[0]['slide'];
		
	
		$thumbnails = '<div class="thumbnails">';
	
		
		foreach($items as $key => $item){
		if($key>0){
			if(file_exists(image_style_path('ref_thumb',  $item['path']))){
			  $img_url_rel = str_replace('public://',$tubesca_nav_vars['path_prefix'].'/sites/default/files/',image_style_path('ref_thumb',  $item['path']));
			  $thumbnails .= '<img src="'.$img_url_rel.'" height="136" width="102">';
			}else{
				$thumbnails .= '';
			}
		}
		if($key==2) break;
			
		}
		if($key==1) $thumbnails .= '';
		$thumbnails .= '</div>';
								
		
		$variables['gallery_base_image'] = $base_image;
		$variables['gallery_thumbnails'] = $thumbnails;
		/*
		$cs = field_get_items('node', $variables['node'], 'field_crselling');
		//preprintr($cs);
		
		$variables['cross_selling'] = '';
		if(!empty($cs)){
		$variables['cross_selling'] .= '<div class="product-related-wrapper"><div class="container"><h2 class="title_related">'.$tc_translations['string-area-22'].'</h2><h3 class="subtitle_related">'.$tc_translations['string-area-23'].'</h3><div class="row">';
		foreach($cs as $delta => $acs){
			
			if(!empty($acs['entity']->field_image_setting2['und'][0]['fid'])){
				if($acs['target_type'] =='node'){
					$cs_title = $acs['entity']->title;
					$cs_url = url('node/'.$acs['entity']->nid);
				}else{
					$cs_title = $acs['entity']->name;
					$cs_url = url('taxonomy/term/'.$acs['entity']->tid);
				}
				$cs_image = file_load($acs['entity']->field_image_setting2['und'][0]['fid']);
				$cs_image = file_create_url($cs_image->uri);
				$variables['cross_selling'] .= '<div class="col col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<div class="field_image"><div class="image_wrapper"><img src="'.$cs_image.'"><a href="'.$cs_url.'"><div class="product_related_image_content"><span class="glyphicon glyphicon-plus"></span></div></a></div></div>
				<div class="field_titile">'.$cs_title.'</div>
			</div>';
		}
		}
		$variables['cross_selling'] .= '</div></div></div>';
		}*/
		}
}