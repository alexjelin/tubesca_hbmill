<?php
/**
 * @file
 * Preprocess : page
 */

include '/var/www/html/tubesca_int/en/sites/default/settings.php';
variable_set('databases_en', $databases);

include '/var/www/html/tubesca_int/es/sites/default/settings.php';
variable_set('databases_es', $databases);

/**
 * Implements template_preprocess_html().
 */
function tubesca_comabi_preprocess_html(&$variables) {
  $node = menu_get_object();
  if (isset($node)) {
    $variables['theme_hook_suggestion'] = 'html__'.$node->type; //
  }
}

/**
 * Implements template_preprocess_page().
 */
function tubesca_comabi_preprocess_page(&$variables) {
  if (isset($variables['node'])) {
    $variables['theme_hook_suggestion'] = 'page__'.$variables['node']->type; //
  }
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $term = menu_get_object('taxonomy_term', 2);

    if ($term) {
      if ($term->vocabulary_machine_name == 'family') {
		   global $language;
        $erp_id_value = field_get_items('taxonomy_term', $term, 'field_erp_id');
		$erp_ip = $erp_id_value[0]['value'];
		 
  $databases_es = variable_get('databases_es');
  $databases_en = variable_get('databases_en');
  $hreflang_results = array();

   $type = 'family';
   $hreflang_results=tubesca_comabi_insert_hreflang($databases_en, $databases_es, $erp_ip, $type) ;
   $variables['hreflang_results'] = $hreflang_results;
   db_set_active(); // set default database to active state! 
		 
		 
      }
    }
  }

  drupal_add_library('system', 'ui.accordion');
}