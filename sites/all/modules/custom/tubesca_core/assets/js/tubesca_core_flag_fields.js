Drupal.ajax.prototype.specifiedResponse = function() {
		var ajax = this;

			// Do not perform another ajax command if one is already in progress.
		if (ajax.ajaxing) {
			return false;
		}

		try {
			jQuery.ajax(ajax.options);
		}
		catch (err) {

			alert('An error occurred while attempting to process ' + ajax.options.url);
			return false;
		}

			return false;
	};

jQuery(document).ready(function ($) {

	$('#auto_text').on('keyup', function(e) {
		if (e.which !== 32) {
			var value = $(this).val();
		   
			if (value.length > 2) {
				var custom_settings = {};
					custom_settings.url = '/'+Drupal.settings.tubesca_core.lang+'/tubesca/auto/'+value;
					Drupal.ajax['custom_ajax_action'] = new Drupal.ajax(null, $(document.body), custom_settings);
					Drupal.ajax['custom_ajax_action'].specifiedResponse();
			} else {
				$( "#ajax_replace_auto" ).empty();
			}
		}
	});




	
	$('.tubesca_core_flag_fields_button').on('click', function(){
	
		selector = '.view-display-id-block_1';
		$(selector).triggerHandler('RefreshView');
		$.ajax({
				
				url: '/'+Drupal.settings.tubesca_core.lang+'/tubesca/flags/'+$(this).attr('data-nid')+'/'+$(this).data('field')+'/1/'+$(this).data('flag'),
				dataType: 'json',
				timeout:0,
				cache: false,								 
				type: "GET",
				complete: function(data){
			     window.location.href = '/'+Drupal.settings.tubesca_core.lang+'/'+Drupal.settings.tubesca_core.tubesca_nav_vars.my_list;
					}
			  });
	});
	$('.tubesca_core_flag_fields').on('click', function(){
			if ($(this).data('type') == 'plus')
		{
			$('#'+$(this).data('id')).val($('#'+$(this).data('id')).val()*1+1*1);
		} else{
			if ($('#'+$(this).data('id')).val() > 0)
			{
				$('#'+$(this).data('id')).val($('#'+$(this).data('id')).val()*1-1*1);
			}
			
		}
		selector = '.view-display-id-block_1';
		$(selector).triggerHandler('RefreshView');
		$.ajax({
				
				url: '/'+Drupal.settings.tubesca_core.lang+'/tubesca/flags/'+$(this).data('nid')+'/'+$(this).data('field')+'/'+$('#'+$(this).data('id')).val()+'/'+$(this).data('flag'),
				dataType: 'json',
				timeout:0,
				cache: false,								 
				type: "GET",
				success: function(data){}
			   
			  });
	});
	$(".remove_all").on('click', function(){
	
			
		var ajax = $.ajax({
				
				url: '/'+Drupal.settings.tubesca_core.lang+'/tubesca/flags_remove/'+$(this).data('nids'),
				dataType: 'json',
				timeout:0,
				cache: false,								 
				type: "GET",
				success: function(data){}
			   
			  });
		ajax.complete(function(jqXHR){
			if(jqXHR.readyState === 4) {
				selector = '.view-display-id-page';
				$(selector).triggerHandler('RefreshView');
				selector = '.view-display-id-block_1';
				$(selector).triggerHandler('RefreshView');
			}
		});
	});
});


(function ($) {

  Drupal.behaviors.ajaxExample = {
    attach: function (context, settings) {
    
      // CSS Selector for the button which will trigger the AJAX call
		
      $('.'+ Drupal.settings.custom.ajaxCssSend, context).click(function () {

          $.ajax({
           url: Drupal.settings.custom.ajaxUrlSend, // This is the AjAX URL set by the custom Module
           method: "GET",
           data: { 'type' : $(this).data('type'), 'name' : $(this).data('name'), 'target' : $(this).data('target') }, // Set the number of Li items requested
           dataType: "html",          // Type of the content we're expecting in the response
           success: function(data) {
            //$('#ajax-result').html(data);  // Place AJAX content inside the ajax wrapper div
           }

         });

      });
     }
  };

}(jQuery));