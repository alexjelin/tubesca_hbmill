CKEDITOR.stylesSet.add('mycustomstyleset',
  [
    	/* Block Styles */

	// These styles are already available in the "Format" combo, so they are
	// not needed here by default. You may enable them to avoid placing the
	// "Format" combo in the toolbar, maintaining the same features.
	
	
	{ name : 'H1'		, element : 'h1' },
	{ name : 'H1 Subtitle'	, element : 'h1', attributes : { 'class' : 'subtitle' } },
	{ name : 'H2'		, element : 'h2' },
	{ name : 'H2 Subtitle'	, element : 'h2', attributes : { 'class' : 'subtitle' } },
	{ name : 'H3'		, element : 'h3' },
	{ name : 'H4'		, element : 'h4' },
	
  { name : 'Body'		, element : 'p' },
	{ name : 'Body bold'		, element : 'p', attributes : { 'class' : 'bold' } },
	{ name : 'Intro'	, element : 'p', attributes : { 'class' : 'intro' } },
	{ name : 'Separator'	, element : 'p', attributes : { 'class' : 'grey_separator' } },
	{ name : 'Bullets'	, element : 'ul', attributes : { 'class' : 'orange_bullets' } },
	{ name : 'Listing'		, element : 'p', attributes : { 'class' : 'listing' } },
	

	/* Inline Styles */

	// These are core styles available as toolbar buttons. You may opt enabling
	// some of them in the Styles combo, removing them from the toolbar.
	/*
	{ name : 'Strong'			, element : 'strong', overrides : 'b' },
	{ name : 'Emphasis'			, element : 'em'	, overrides : 'i' },
	{ name : 'Underline'		, element : 'u' },
	{ name : 'Strikethrough'	, element : 'strike' },
	{ name : 'Subscript'		, element : 'sub' },
	{ name : 'Superscript'		, element : 'sup' },
	*/

	/*{ name : 'Marker: Yellow'	, element : 'span', styles : { 'background-color' : 'Yellow' } },
	{ name : 'Marker: Green'	, element : 'span', styles : { 'background-color' : 'Lime' } },

	{ name : 'Big'				, element : 'big' },
	{ name : 'Small'			, element : 'small' },
	{ name : 'Typewriter'		, element : 'tt' },

	{ name : 'Computer Code'	, element : 'code' },
	{ name : 'Keyboard Phrase'	, element : 'kbd' },
	{ name : 'Sample Text'		, element : 'samp' },
	{ name : 'Variable'			, element : 'var' },

	{ name : 'Deleted Text'		, element : 'del' },
	{ name : 'Inserted Text'	, element : 'ins' },

	{ name : 'Cited Work'		, element : 'cite' },
	{ name : 'Inline Quotation'	, element : 'q' },

	{ name : 'Language: RTL'	, element : 'span', attributes : { 'dir' : 'rtl' } },
	{ name : 'Language: LTR'	, element : 'span', attributes : { 'dir' : 'ltr' } },*/

	/* Object Styles */

	{
		name : 'Image on Left',
		element : 'img',
		attributes :
		{
			'style' : 'padding: 5px; margin-right: 5px',
			'border' : '2',
			'align' : 'left'
		}
	},

	{
		name : 'Image on Right',
		element : 'img',
		attributes :
		{
			'style' : 'padding: 5px; margin-left: 5px',
			'border' : '2',
			'align' : 'right'
		}
	},

  ]);