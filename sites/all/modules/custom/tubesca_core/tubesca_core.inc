<?php

/**
 * Callback for retrieving document resources.
 *
 */
function _document_retrieve($operator, $range_id, $type_id, $lang) {

  $documents = array();
	if(($range_id == 'all')&&($type_id == 'all')){
		  $result1 = db_query("SELECT DISTINCT nid FROM {node} n " .
          "INNER JOIN {field_data_field_spare_parts} sp ON sp.entity_id = n.nid " .
          "WHERE (n.language LIKE :lang) AND (n.status = 1) AND (n.type IN ('media')) AND (sp.bundle IN ('media'))", array(':lang' => $lang));
	}elseif($type_id == 'all'){
		  $result1 = db_query("SELECT DISTINCT nid FROM {node} n " .
          "INNER JOIN {field_data_field_spare_parts} sp ON sp.entity_id = n.nid " .
          "INNER JOIN {field_data_field_family} fam ON fam.entity_id = sp.field_spare_parts_target_id " .
          "WHERE (n.language LIKE :lang) AND (n.status = 1) AND (n.type IN ('media')) AND (sp.bundle IN ('media')) AND (fam.bundle IN ('product_family')) " .
          "AND (fam.field_family_tid = :range_id)", array(':lang' => $lang, ':range_id' => $range_id));
	}elseif($range_id == 'all'){
		  $result1 = db_query("SELECT DISTINCT nid FROM {node} n " .
          "INNER JOIN {field_data_field_media_type} mt ON mt.entity_id = n.nid " .
          "WHERE (n.language LIKE :lang) AND (n.status = 1) AND (n.type IN ('media')) AND (mt.bundle IN ('media')) " .
          "AND (mt.field_media_type_tid = :type_id)", array(':lang' => $lang, ':type_id' => $type_id));
	}else{
		  $result1 = db_query("SELECT DISTINCT nid FROM {node} n " .
          "INNER JOIN {field_data_field_media_type} mt ON mt.entity_id = n.nid " .
          "INNER JOIN {field_data_field_spare_parts} sp ON sp.entity_id = n.nid " .
          "INNER JOIN {field_data_field_family} fam ON fam.entity_id = sp.field_spare_parts_target_id " .
          "WHERE (n.language LIKE :lang) AND (n.status = 1) AND (n.type IN ('media')) AND (sp.bundle IN ('media')) AND (fam.bundle IN ('product_family')) AND (mt.bundle IN ('media')) " .
          "AND (fam.field_family_tid = :range_id) AND (mt.field_media_type_tid = :type_id)", array(':lang' => $lang, ':range_id' => $range_id, ':type_id' => $type_id));
	}

  $nids = array();
  foreach ($result1 as $obj) {
    $nids[] = $obj->nid;
  }
  foreach ($nids as $nid) {
    $current_node = node_load($nid);
    $node_wrapper = entity_metadata_wrapper('node', $current_node);
    $documents[] = _extractMediaData($node_wrapper);
  }

  return (object) ($documents);
}

function _extractMediaData($node_wrapper) {
  $document = array();

  $document['document_id'] = $node_wrapper->getIdentifier();
  $document['document_type_tid'] = $node_wrapper->field_media_type->value()->tid;
  $document['document_type_name'] = $node_wrapper->field_media_type->value()->name;
  $document['description'] = ($node_wrapper->field_description->value())?$node_wrapper->field_description->value():$node_wrapper->field_media_type->value()->description;
	$media = $node_wrapper->field_media->value(); 
  $document['media'] = file_create_url($media[0]['uri']);
  $document['title'] = $node_wrapper->title->value();
  $document['is_secured'] = (file_uri_scheme($media[0]['uri'])=='private')?true:false;
	
  return $document;
}

/**
 * Callback for retrieving mobile catalogue resources.
 *
 */
function _catalogue_retrieve($operation, $condition, $lang) {
  //$tree = taxonomy_get_tree(3);
  $tree = '';
  $arbo = 173;
  $product_catalogue = array();
	$product_catalogue['response'] = array();
  $counter_elements_level2 = array();
  $remote_path = '/var/www/html/tubesca_ftp_rootdir/' . $arbo . '.xml';
  if (!$xml = simplexml_load_file($remote_path, 'SimpleXMLElement', LIBXML_NOCDATA)) {
    die('Unable to open XML tree');
  }else {
    foreach ($xml->page as $pg) {
      $pre_page = array();
      $pre_page['initial_title'] = (string) $pg->name;
      $pre_page['first_levels'] = array();
      foreach ($pg->famille as $pg_el1) {
        $level1 = array();
        $tid = tubesca_core_import_entity_exist((int) trim($pg_el1['idFamille']), 'family', 'fr');
        if($tid<0) continue;
        $title = (string) $pg_el1->name;
        $current_term = taxonomy_term_load($tid);
        $level1['menu_level_1_id'] = $tid;
        $level1['menu_level_1_title'] = $title;
        $level1['menu_level_1_epr_id'] = $current_term->field_erp_id['und'] [0] ['value'];
        $level1['level_2'] = array();
        foreach ($pg_el1->famille as $pg_el2) {
          $level2 = array();
          $tid = tubesca_core_import_entity_exist((int) trim($pg_el2['idFamille']), 'family', 'fr');
          if($tid<0) continue;
          $title = (string) $pg_el2->name;
          $current_term = taxonomy_term_load($tid);
					$original_url = $_SERVER['HTTP_HOST'] . "/taxonomy/term/" . $tid;
          $url_alias = $_SERVER['HTTP_HOST'] . "/" . drupal_get_path_alias("taxonomy/term/" . $tid);
          $level2['parent_family_id'] = $tid;
          $level2['parent_family_title'] = $title;
          $level2['parent_family_epr_id'] = $current_term->field_erp_id['und'] [0] ['value'];
					$level2['parent_admin_url'] = $original_url;
					$level2['parent_family_url'] = $url_alias;
          $level1['level_2'][] = $level2;
          unset($level2);

        }
        $pre_page['first_levels'][] = $level1;
        unset($level1);
      }
      $product_catalogue['response'][] = $pre_page;
      unset($pre_page);
    }
  }
  return (object) $product_catalogue;
}

/**
 * Callback for retrieving mobile simulator resources.
 *
 */
function _simulator_retrieve($operation, $condition, $lang) {
  if (($condition == 'reference') || ($condition == 'arbo') || ($condition == 'write')) {
    $result = tubesca_core_app_import($condition);
    return (object) $result;
  }
}

/**
 * Callback for retrieving my account resources.
 *
 */
function _myaccount_retrieve($operation, $client_id, $lang) {
  $warnings = array();
  $users = entity_load('user', array($client_id));

  foreach ($users as $key => $user) {
    $user_uid = $user->uid;
    $simulator_status = $user->field_simulator_warning['und'][0]['value'];
    $information_status = $user->field_information_warning['und'][0]['value'];
    $warnings['simulator'] = $simulator_status;
    $warnings['information'] = $information_status;
  }

  return (object) $warnings;
}

/**
 * Callback for retrieving my account resources.
 *
 */
function _myaccount_update($operation, $client_id, $lang, $nvalues) {
	 $account = user_load($client_id);
	 $wrapper = entity_metadata_wrapper('user', $account);
	 $wrapper->field_simulator_warning->set($nvalues[0]);
	 $wrapper->field_information_warning->set($nvalues[1]);
   $wrapper->save();
	 $fields = array(
			'field_simulator_warning' => $nvalues[0],
			'field_information_warning' => $nvalues[1],
    );
	 return (object) $fields;
}

/**
 * Callback for retrieving my expert resources.
 *
 */
function _myexpert_retrieve($operation, $client_id, $lang) {

  global $tc_translations;
  $results = array();

  $users = entity_load('user', array($client_id));

  $user_roles = array(
      '11' => 'Lead',
      '12' => 'Distributeurs',
      '13' => 'Loueurs',
      '14' => 'Utilisateurs',
      '15' => 'Probation',
  );


  foreach ($users as $key => $user) {

    //echo "<pre>"; print_r($user); echo "</pre>";

    $token_value = trim($user->field_token['und'][0]['value']);

    $cliet_id_value = $user->uid;

    //$language = $lang;

    if ($client_id == $cliet_id_value) {


      $the_role = 0;

      foreach ($user->roles as $key => $role) {

        $id_role = array_search($role, $user_roles);

        if (( $id_role > 11) && ( $id_role < 15)) {

          $the_role = $id_role;

          break;
        }
      }

      $mon_expert = $user->field_mon_expert['und'][0]['target_id'];

      if (empty($mon_expert)) {
        $contact_term = tubesca_core_get_contact_list($the_role);

        switch ($the_role) {

          case '12': // for Distributeur


            $results['dir_ventes'] = $contact_term[0]->field_text_setting1['und'] [0] ['value']; // Directeur des ventes
            $field_name = 'field_text_setting1';


            $results['responsable_name'] = $contact_term[0]->field_text_setting4['und'] [0] ['value']; //  name

            $results['responsable_tel'] = $contact_term[0]->field_responsable_region_tel['und'] [0] ['value'];

            $results['responsable_mail'] = $contact_term[0]->field_responsable_region_mail['und'] [0] ['email'];



            $results['adv_name'] = $contact_term[0]->field_text_setting5['und'] [0] ['value']; // adv name

            $results['adv_label'] = $tc_translations['string-area-37'];
            $results['responsable_region_label'] = $tc_translations['string-area-36'];
            $results['directeur_des_ventes_label'] = $tc_translations['string-area-35'];
            $results['sav_lourd_label'] = $tc_translations['string-area-38'];
            $results['sav_leger_label'] = $tc_translations['string-area-40'];
            $results['pole_commande_label'] = $tc_translations['string-area-39'];
            $results['label_pove_adv'] = $tc_translations['string-area-70'];
            $results['label_horaires'] = '9h00 - 12h00 | 14h00 - 17h00';

            $results['adv_tel'] = $contact_term[0]->field_adv_tel['und'];

            $results['adv_mail'] = $contact_term[0]->field_adv_mail['und'];



            $results['pole_name'] = $contact_term[0]->field_text_setting6['und'] [0] ['value']; // pole name

            $results['pole_tel'] = $contact_term[0]->field_pole_commande_tel['und'] [0] ['value'];

            $results['pole_mail'] = $contact_term[0]->field_pole_commande_mail['und'] [0] ['email'];



            $results['sav_leger_tel'] = $contact_term[0]->field_sav_leger_tel['und'] [0] ['value'];

            $results['sav_leger_name'] = $contact_term[0]->field_text_setting7['und'] [0] ['value']; // sav  loucontact_termd name

            $sav_loucontact_termd_mail = 'contact@tubesca-comabi.com';



            $results['sav_leger_email'] = $field_sav_legecontact_term_email;



            $results['sav_lourd_tel'] = $contact_term[0]->field_sav_lourd_tel['und'] [0] ['value'];

            $results['sav_lourd_name'] = $contact_term[0]->field_title['und'] [0] ['value']; //sav legecontact_term name

            $field_sav_legecontact_term_email = 'contact@tubesca-comabi.com';



            $results['sav_lourd_email'] = $field_sav_legecontact_term_email;



            break;



          case '13': // for  Loueurs

            $results['adv_label'] = $tc_translations['string-area-37'];
            $results['responsable_region_label'] = $tc_translations['string-area-36'];
            $results['directeur_des_ventes_label'] = $tc_translations['string-area-35'];
            $results['label_pove_adv'] = $tc_translations['string-area-70'];
            $results['label_horaires'] = '9h00 - 12h00 | 14h00 - 17h00';

            $results['dir_ventes'] = $contact_term[0]->field_text_setting1['und'] [0] ['value']; // dicontact_termecteucontact_term de ventes



            $results['responsable_name'] = $contact_term[0]->field_text_setting4['und'] [0] ['value']; //  name

            $results['responsable_tel'] = $contact_term[0]->field_responsable_region_tel['und'] [0] ['value'];

            $results['responsable_mail'] = $contact_term[0]->field_responsable_region_mail['und'] [0] ['email'];



            $results['adv_name'] = $contact_term[0]->field_text_setting5['und'] [0] ['value']; // adv name

            $results['adv_tel'] = $contact_term[0]->field_adv_tel['und'];

            $results['adv_mail'] = $contact_term[0]->field_adv_mail['und'];



            break;





          case '14': // for Utilisateur

            $results['adv_label'] = $tc_translations['string-area-37'];
            $results['sav_lourd_label'] = $tc_translations['string-area-38'];
            $results['sav_leger_label'] = $tc_translations['string-area-40'];

            $results['adv_name'] = $contact_term[0]->field_text_setting5['und'] [0] ['value']; // adv name

            $results['adv_tel'] = $contact_term[0]->field_adv_tel['und'] [0] ['value'];

            $results['adv_mail'] = $contact_term[0]->field_adv_mail['und'];



            $results['sav_leger_tel'] = $contact_term[0]->field_sav_leger_tel['und'] [0] ['value'];

            $results['sav_leger_name'] = $contact_term[0]->field_text_setting7['und'] [0] ['value']; // sav  loucontact_termd name

            $sav_loucontact_termd_mail = 'contact@tubesca-comabi.com';



            $results['sav_leger_email'] = $field_sav_legecontact_term_email;



            $results['sav_lourd_tel'] = $contact_term[0]->field_sav_lourd_tel['und'] [0] ['value'];

            $results['sav_lourd_name'] = $contact_term[0]->field_title['und'] [0] ['value']; //sav legecontact_term name

            $field_sav_legecontact_term_email = 'contact@tubesca-comabi.com';



            $results['sav_lourd_email'] = $field_sav_legecontact_term_email;





            break;
        }
      }
      else {
        $contact_term = taxonomy_term_load($mon_expert);

        $results['dir_ventes'] = $contact_term->field_text_setting1['und'] [0] ['value']; // Directeur des ventes
        $results['responsable_name'] = $contact_term->field_text_setting4['und'] [0] ['value']; //  name
        $results['responsable_tel'] = $contact_term->field_responsable_region_tel['und'] [0] ['value'];
        $results['responsable_mail'] = $contact_term->field_responsable_region_mail['und'] [0] ['email'];
        $results['adv_name'] = $contact_term->field_text_setting5['und'] [0] ['value']; // adv name
        $results['adv_tel'] = $contact_term->field_adv_tel['und'];
        $results['adv_mail'] = $contact_term->field_adv_mail['und'];

        $results['pole_name'] = $contact_term->field_text_setting6['und'] [0] ['value']; // pole name
        $results['pole_tel'] = $contact_term->field_pole_commande_tel['und'] [0] ['value'];
        $results['pole_mail'] = $contact_term->field_pole_commande_mail['und'] [0] ['email'];
        $results['sav_leger_tel'] = $contact_term->field_sav_leger_tel['und'] [0] ['value'];
        $results['sav_leger_name'] = $contact_term->field_text_setting7['und'] [0] ['value']; // sav  loucontact_termd name
		
        $sav_loucontact_termd_mail = 'contact@tubesca-comabi.com';
        $results['sav_leger_email'] = $field_sav_legecontact_term_email;
        $results['sav_lourd_tel'] = $contact_term->field_sav_lourd_tel['und'] [0] ['value'];
        $results['sav_lourd_name'] = $contact_term->field_title['und'] [0] ['value']; //sav legecontact_term name
        $field_sav_legecontact_term_email = 'contact@tubesca-comabi.com';
        $results['sav_lourd_email'] = $field_sav_legecontact_term_email;
      }


      break; // the end of foreach loop
    }
  }



  return (object) $results;
}

/**
 * Access callback for the note resource.
 *
 * @param string $op
 *  The operation that's going to be performed.
 * @param array $args
 *  The arguments that will be passed to the callback.
 * @return bool
 *  Whether access is given or not.
 */
function _tubesca_core_services_access($op) {
  global $user;
  $access = FALSE;

  switch ($op) {
    case 'view':
      //$note = resttubesca_get_note($args[0]);
      //$access = user_access('note resource view any note');
      //$access = $access || $note->uid == $user->uid && user_access('note resource view own notes');
			if($user->uid>0){
			  $access = TRUE;
			}
    break;
		case 'view_catalogue':
		  $access = TRUE;
    break;
		case 'view_document':
		  $access = TRUE;
    break;
  }
  //$access = TRUE;

  return $access;
}