SUMMARY
-------
Simple integration with the Replicate module by using the Replicate UI's
pre-existing replication form.

REQUIREMENTS
------------
This module requires the following modules:
 * Bean (https://drupal.org/project/bean)
 * Replicate (https://www.drupal.org/project/replicate)
 * Replicate UI (https://www.drupal.org/project/replicate_ui)


INSTALLATION
------------
* Install as usual, see
https://www.drupal.org/documentation/install/modules-themes/modules-7 for
further information.
