<?php 
	
	$path= "/var/www/html/tubesca_ftp_rootdir/";

        //$dest="gregory.seves@agencemayflower.com";
        $dest="gregory.seves@agencemayflower.com, stephanie.balay@agencemayflower.com, sebastien.urios@agencemayflower.com";
	$subject="test rapport fichiers tubesca";
	$headers ="MIME-Version: 1.0\r\n"; 
	$headers.='From:"TUBESCA PROD"<no-reply@tubesca-comabi.com >'."\n"; 
	$headers.='content-Type:text/html;charset ="utf-8"'."\n"; 
	$headers.='content-Transfert-Encoding : 8bit';
	$filelist = array("diff_en" => 'utf8-export-web-multilingue-disctinct-diff_en.txt',
					  "full_en" => 'utf8-export-web-multilingue-disctinct_en.txt',
					  "diff_es" => 'utf8-export-web-multilingue-disctinct-diff_es.txt',
					  "full_es" => 'utf8-export-web-multilingue-disctinct_es.txt',
					  "diff_fr" => 'utf8-export-web-multilingue-disctinct-diff_fr.txt',
					  "full_fr" => 'utf8-export-web-multilingue-disctinct_fr.txt',
					  "173.xml" => '173.xml',
					  "175.xml" => '173.xml',
					  "176.xml" => '176.xml',
					  "440.xml" => '440.xml'
				);

	$message = "<html><body>";
	foreach($filelist as $file){
		$message .= "<br><br>".$file." : <br>";
		
		if(file_exists($path.$file) ){ 

			// Si taille > 100Ko
			$message = filesize($path.$file) > 102400 ?  $message."<span style='color:red'>" : $message."<span style='color:green'>"; 
			$message.= filesize($path.$file)." octets</span>     " ;

			// Si date < date -1
			$yesterday = new DateTime('-1 day');
  			 
			$message = filectime($path.$file) < $yesterday->getTimestamp() ? $message." | <span style='color:red'>" : $message." | <span style='color:green'>" ;
			$message.= date('Y-m-d H:i:s', filectime($path.$file))."</span>     ";

			if(!strpos($file,"xml")){
				$contenu_fichier = file($path.$file);
				$message = count($contenu_fichier) > 100 ?	 $message." | <span style='color:red'>" : $message." | <span style='color:green'>";
				$message.= count($contenu_fichier).' lignes</span>     ' ;
			}

		} else {
			$message = "<span style='color:red'> File doesn't exists </span> " ;
		}
	}
    //fopen("utf8-export-web-multilingue-disctinct_en.txt", mode)
	$message = $message."</body></html>";
	echo $message;
	echo '<br><br><br>';
	if( mail($dest,$subject, $message, $headers) ) echo "Mail envoyé";
	else echo "erreur envoie du mail";

 ?>
